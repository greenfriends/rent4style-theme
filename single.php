<?php
use PluginContainer\Packages\R4sBooking\Controller\R4sBooking;

get_header();
$productId = get_queried_object_id();
$postType = get_post($productId)->post_type;
if($postType === 'product') {
$product = wc_get_product($productId);
$productImageThumb = wp_get_attachment_image_src($product->get_image_id(),'thumbnail');
$attachmentIds = $product->get_gallery_image_ids();
$attachmentImages = [];
foreach($attachmentIds as $attachmentId) {
    $attachmentImages[$attachmentId]['thumbnail'] = wp_get_attachment_image($attachmentId, 'medium');
    $attachmentImages[$attachmentId]['img'] = wp_get_attachment_image($attachmentId, 'large');
    $attachmentImages[$attachmentId]['url'] = wp_get_attachment_image_url($attachmentId, 'large');
}

$rating = $product->get_average_rating();
$reviewCount = $product->get_review_count();
$originalPrice = $product->get_meta('originalPrice');
$variations = [];
$attributes = [];
if ($product instanceof WC_Product_Variable) {
    $variations = $product->get_available_variations();
    $attributes = $product->get_variation_attributes();
}
$productVideoUrl = $product->get_meta('productVideoUrl');
$attachmentId = attachment_url_to_postid($productVideoUrl);
$attachment = null;
if($attachmentId) {
    $attachment = get_post($attachmentId);
}
if($attachment) {
    $videoTitle = $attachment->post_title;
}
global $gfContainer;
/** @var R4sBooking $calendarController */
$calendarController = $gfContainer->get(R4sBooking::class);
$reviews = getProductReviewInfo($productId) ?? [];
?>
    <div class="container container--main container--space">
        <div class="details">
            <div class="details--left">
                <div class="details--thumbs">
                    <?php if(count($reviews) > 0):?>
                        <?php
                            if(!$reviews[0]->image) {
                                $reviews[0]->image = THEME_URI . '/assets/images/logoRed.png';
                            }
                            if(!$reviews[0]->thumbnail) {
                                $reviews[0]->thumbnail = THEME_URI . '/assets/images/logoRed.png';
                            }
                        ?>
                        <div class="details--thumbs-customer">
                            <div class="reviewData hiddenElement"
                                 data-index="0"
                                 data-image-full="<?= $reviews[0]->image ?>"
                                 data-weight="<?= $reviews[0]->authorWeight ?>"
                                 data-height="<?= $reviews[0]->authorHeight ?>"
                                 data-size="<?= $reviews[0]->authorSize ?>"
                                 data-content="<?= $reviews[0]->content ?>"
                                 data-age="<?=$reviews[0]->age?>"
                                 data-worn-to="<?=$reviews[0]->wornTo?>"
                                 data-body-type="<?=$reviews[0]->bodyType?>"
                                data-title="<?=$reviews[0]->title?>">
                            </div>
                            <div class="initialRatingHtml">
                                <?=getRatingHtml($reviews[0]->rating)?>
                            </div>
                            <img src="<?=$reviews[0]->thumbnail?>" alt="<?=$reviews[0]->author?>" />
                            <span><?=__('Rent4Style djevojka', 'gf-theme')?></span>
                        </div>
                    <?php endif;?>
                    <div class="swiper details--thumbs-carousel">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <?=$product->get_image('medium')?>
                            </div>
                            <?php foreach ($attachmentImages as $attachmentImage): ?>
                                <div class="swiper-slide">
                                    <?=$attachmentImage['thumbnail']?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php if($productVideoUrl) :?>
                    <div class="details--thumbs-video" data-src="<?=$productVideoUrl?>">
                        <svg class="icon icon--medium">
                            <use href="<?=THEME_URI . '/assets/images/sprite.svg#play'?>" />
                        </svg>
                        <span><?=__('Video','gf-theme')?></span>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="details--gallery">
                    <div class="swiper details--carousel" style="--swiper-navigation-color: #000; --swiper-navigation-size: 20px;  --swiper-pagination-color: #FFF">
                        <ul class="swiper-wrapper lightbox">
                            <li class="swiper-slide">
                                <a href="<?=wp_get_attachment_image_url($product->get_image_id(), 'large')?>">
                                    <?=$product->get_image('large')?>
                                </a>
                            </li>
                            <?php foreach($attachmentImages as $attachmentImage): ?>
                                <li class="swiper-slide">
                                    <a href="<?=$attachmentImage['url']?>" data-lightbox="details">
                                        <?=$attachmentImage['img']?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                    <?php if($productVideoUrl) :?>
                    <div class="details--video">
                        <video controls="controls" muted autoplay src="" title="<?=$videoTitle?>"></video>
                    </div>
                        <div class="details--video-open" data-src="<?=$productVideoUrl?>">
                            <svg class="icon">
                                <use href=<?=THEME_URI . '/assets/images/sprite.svg#play'?> />
                            </svg>
                            <span>Video</span>
                        </div>
                    <?php endif;?>
                    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="pswp__bg"></div>
                        <div class="pswp__scroll-wrap">
                            <div class="pswp__container">
                                <div class="pswp__item"></div>
                                <div class="pswp__item"></div>
                                <div class="pswp__item"></div>
                            </div>
                            <div class="pswp__ui pswp__ui--hidden">
                                <div class="pswp__top-bar">
                                    <div class="pswp__counter"></div>
                                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                                    <button class="pswp__button pswp__button--share" title="Share"></button>
                                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                                    <div class="pswp__preloader">
                                        <div class="pswp__preloader__icn">
                                            <div class="pswp__preloader__cut">
                                                <div class="pswp__preloader__donut"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                    <div class="pswp__share-tooltip"></div>
                                </div>
                                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
                                <div class="pswp__caption">
                                    <div class="pswp__caption__center"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if($productVideoUrl) :?>
                    <div style="font-weight:bold;" class="details--images-open">
                        <span>Galerija</span>
                    </div>
                <?php endif;?>

            </div>
            <div class="details--right">
                <div class="details--info">
                    <div class="details--ratings">
                        <?=getRatingHtml($rating)?>
                        <span>(<?=$reviewCount?>)</span>
                    </div>
                    <h1><?=$product->get_title()?></h1>
                    <p><?=$product->get_short_description()?></p>
                    <?php if ($originalPrice):?>
                    <div class="details--original-price">
                        <?=number_format($originalPrice, 2) . ' ' . get_woocommerce_currency_symbol() . ' ' .  __('Original price', 'gf-theme')?>
                    </div>
                    <?php endif;?>
                    <?php $wishlistForUser = json_encode([]) ?>
                    <?php if(is_user_logged_in()):?>
                        <?php
                        global $gfContainer;
                        $wishlist = $gfContainer->get(\PluginContainer\Packages\Wishlist\Controller\Wishlist::class);
                        $wishlistForUser = json_encode($wishlist->getWishlistProducts());
                        ?>
                    <?php endif;?>

                    <?php if(is_user_logged_in()):?>
                        <?=\PluginContainer\Packages\Wishlist\Controller\Wishlist::getWihslistIconHtml($product)?>
                    <?php endif;?>
                </div>

                <div class="details--option">
                    <div class="details--option-price">
                        <input id="reserve" type="radio" value="reserve" checked />
                        <label for="reserve">
                            <div>
                                <div class="details--option-info">
                                    <h6><?=__('Jednokratni najam','gf-theme')?></h6>
                                    <span class="details--option-days"><?=__('Rent na 4 dana','gf-theme')?></span>
                                    <span class="details--option-cost"><?=$product->get_price() . ' ' . get_woocommerce_currency_symbol()?>
                                        <?php
                                        global $gfContainer;
                                        /**
                                         * @var \PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion $currencyConversion
                                         */
                                        $currencyConversion = $gfContainer->get(\PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion::class);
                                        $rate = $currencyConversion->getExchangeRate();
                                        ?>
                                        <?php if($rate && get_woocommerce_currency() === 'HRK'):?>
                                          <?= '/ &euro;' . number_format($product->get_price() / $rate,2)?>
                                        <?php endif;?>
                                    </span>
                                </div>
                                <img src="<?=$productImageThumb[0]?>" alt="<?=$product->get_title()?>" />
                            </div>
                        </label>
                    </div>
                    <div class="details--option-shiping" style="display: block">
                        <?php if (count($variations)):?>
                            <?php foreach($attributes as $attributeName => $value): ?>
                            <div class="details--select">
                                <div class="full-width"> <!-- add "error" when empty -->
                                    <?php
                                        $fixedAttrName = ucfirst(str_replace('pa_','',$attributeName));
                                        if($fixedAttrName === 'Size') {
                                            $fixedAttrName = 'Veličina'; // @todo fix for every product with the size attribute for now its hardcoded
                                        }
                                        if($fixedAttrName === 'Color') {
                                            $fixedAttrName = 'Boja';
                                        }
                                    ?>
                                    <label for="variationSelect"><?=$fixedAttrName?></label>
                                    <select data-attribute="<?=$attributeName?>" id="variationSelect" class="attributeSelect">
                                        <option value="-1"><?=__('Odaberi','gf-theme')?></option>
                                        <?php foreach ($value as $attr):?>
                                            <option value="<?=$attr?>"><?=ucfirst($attr)?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        <?php endif;?>
                        <div class="details--input"> <!-- add "error" when empty -->
                            <span><?=__('Dostava + dan povrata','gf-theme')?></span>
                            <?php
                            $selectedDates =  WC()->session->get('orderDates');
                            if($selectedDates):?>
                                <input aria-label="<?=__('Pick a date','gf-theme')?>" class="details--option-calendar disableEvents" type="text" readonly value="<?=formatOrderDatesForInputView($selectedDates)?>" />
                                <div class="noticeWarning noticeContainer">
                                    <?=sprintf(__('Za jednu narudžbu možete rezervirati samo jedan datum.
                                     Ako želite promijeniti datum za svoje proizvode kliknite <a href="%s">ovdje</a>','gf-theme'),wc_get_checkout_url())?>
                                </div>
                            <?php else:?>
                                <input aria-label="<?=__('Pick a date','gf-theme')?>" class="details--option-calendar popup-calendar--open disableEvents" readonly type="text" />
                            <?php endif;?>

                        </div>
                        <div class="details--option-button">
                            <button disabled data-id="<?=$productId?>" class="details--add-to-bag disabled"><?=__('DODAJ U KOŠARICU','gf-theme')?></button>
                        </div>
                        <input type="hidden" id="variationId" name="variationId">
                        <div class="noticeError noticeContainer">

                        </div>
                        <div class="noticeSuccess noticeContainer">

                        </div>
                    </div>
                </div>


                <div class="details--filters">
                    <div class="filter">
                        <div class="filter--top">
                            <h6><?=__('O proizvodu', 'gf-theme')?></h6>
                            <span class="filter--toggle">
                                    <svg class="icon minus">
                                        <use href="<?=THEME_URI . '/assets/images/sprite.svg#minus'?>"></use>
                                    </svg>
                                    <svg class="icon plus">
                                        <use href="<?=THEME_URI . '/assets/images/sprite.svg#plus'?>"></use>
                                    </svg>
                                </span>
                        </div>
                        <div class="filter--space">
                            <div class="filter--content">
                                <p><?=wpautop($product->get_description())?></p>
                            </div>
                        </div>
                    </div>
                    <div class="filter">
                        <div class="filter--top">
                            <h6><?=__('Veličine i mjere', 'gf-theme')?></h6>
                            <span class="filter--toggle">
                                    <svg class="icon minus">
                                        <use href="<?=THEME_URI . '/assets/images/sprite.svg#minus'?>"></use>
                                    </svg>
                                    <svg class="icon plus">
                                       <use href="<?=THEME_URI . '/assets/images/sprite.svg#plus'?>"></use>
                                    </svg>
                                </span>
                        </div>
                        <div class="filter--space">
                            <div class="filter--content">
                                <p><?=wpautop($product->get_meta('sizeAndFit')) ?? ''?></p>
                            </div>
                        </div>
                    </div>
                    <div class="filter">
                        <div class="filter--top">
                            <h6><?=__('Savjeti stilista', 'gf-theme')?></h6>
                            <span class="filter--toggle">
                                    <svg class="icon minus">
                                        <use href="<?=THEME_URI . '/assets/images/sprite.svg#minus'?>"></use>
                                    </svg>
                                    <svg class="icon plus">
                                       <use href="<?=THEME_URI . '/assets/images/sprite.svg#plus'?>"></use>
                                    </svg>
                                </span>
                        </div>
                        <div class="filter--space">
                            <div class="filter--content">
                                <p><?=wpautop($product->get_meta('stylistAdvice')) ?? ''?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="share">
                    <div class="a2a_kit a2a_default_style">
                        <a class="a2a_button_facebook facebook">
                            <svg class="icon">
                                <use href="<?=THEME_URI . '/assets/images/sprite.svg#facebook-f'?>"></use>
                            </svg>
                        </a>
                        <a class="a2a_button_whatsapp whatsapp">
                            <svg class="icon">
                                <use href="<?=THEME_URI . '/assets/images/sprite.svg#whatsapp'?>"></use>
                            </svg>
                        </a>
                        <a class="a2a_button_email email">
                            <svg class="icon">
                                <use href="<?=THEME_URI . '/assets/images/sprite.svg#envelope'?>"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="popup-calendar popup-hidden">
        <p>Izaberi datum</p>
        <div class="popup-scroll">
            <div class="calendar">
                <div class="calendarContainer">
                    <div class="calendarHeader">
                        <button id="prevMonth">
                            <svg class="icon icon--medium">
                                <use href="<?=THEME_URI . '/assets/images/sprite.svg#angle-left-solid'?>"></use>
                            </svg>
                        </button>
                        <div>
                            <span class="js-calendar-month"></span>
                            <span class="calendarYear"></span>
                        </div>
                        <button id="nextMonth">
                            <svg class="icon icon--medium">
                                <use href="<?=THEME_URI . '/assets/images/sprite.svg#angle-right-solid'?>"></use>
                            </svg>
                        </button>
                    </div>
                    <?php
                    $calendarController->printBookingCalendar();
                    ?>
                </div>
            </div>
<!--            <div class="calendar--success">Uspešno poslat zahtev. Očekujte odgovor na vaš email.</div>-->
            <div class="calendar--options">
                <div class="calendar--option-date">Ako su ti potrebni termini koji nisu ponuđeni, <span class="popup-calendar-dates--open">KLIKOM</span> izaberi drugi termin.</div>
            </div>
            <div id="calendarNoticeContainer">

            </div>
        </div>
        <div class="popup-buttons">
            <button class="popup-button popup-button--secondary">Ugasi</button>
            <button class="popup-button popup-button--primary">Potvrdi</button>
        </div>
    </div>
    <?php include THEME_DIR . '/templates/single/reviews.php' ?>
    <div class="more">
        <div class="container container--main">
            <?php
                $productSliderController = $gfContainer->get(\PluginContainer\Packages\ProductSlider\Controller\ProductSlider::class);
                echo $productSliderController->getRelatedSliderByProduct($product);
            ?>
        </div>
    </div>
    <template id="contactFormTemplate">
        <div class="popup-calendar-dates popup-hidden" style="overflow:auto;">
            <?=do_shortcode('[contact-form-7 id="267" title="Contact form 1"]')?>
        </div>
    </template>
    <script src="https://cdn.jsdelivr.net/npm/calendar-base@1.0.0/dist/calendarbase.umd.production.min.js"></script>
<?php
} else { ?>
    <div class="staticContainer">
    <?php
    while ( have_posts() ) :
        echo '<h1>' . get_the_title() . '</h1>';
        the_post();
        the_content();
    endwhile;// End of the loop.?>
    </div>
<?php
}
?>
    <script>
        let wishlistForUser = JSON.parse('<?=$wishlistForUser?>');
        let size = Object.keys(wishlistForUser).length;
        if(size > 0) {
            let addToWishlistItems = document.querySelectorAll('.addToWishlist');
            addToWishlistItems.forEach((item) => {
                let dataId = item.getAttribute('data-id');
                if (Object.values(wishlistForUser).includes(dataId)) {
                    item.classList.add('delete');
                    item.innerHTML = `<svg viewBox="0 0 512 512" id="heart-full" fill="#E52C42"
            xmlns="http://www.w3.org/2000/svg"><path d="M472.1 270.5L279 470.2c-12.64 13.07-33.27 13.08-45.91.01l-193.2-199.7C-16.21 212.5-13.1 116.7 49.04 62.86 103.3 15.88 186.4 24.42 236.3 75.98L256 96.25l19.7-20.27c49.95-51.56 132.1-60.1 187.3-13.12 62.1 53.74 65.2 149.64 9.1 207.64z"/></svg>`;
                }
            });
        }
    </script>
<?php
get_footer();
