<?php
/**
 * Template Name: Custom Shop Page
 */
get_header();
$cols = get_option('woocommerce_catalog_columns', 4);
$rows = get_option('woocommerce_catalog_rows', 4);
$perPage = $cols * $rows;
$currentPage = get_query_var('paged') ?: 1;
$categories = get_terms(['taxonomy' => 'product_cat', 'hide_empty' => true]);
$tags = get_terms(['taxonomy' => 'product_tag', 'hide_empty' => true]);
global $gfContainer;
/** @var \PluginContainer\Core\WpDbUtils\WpDbUtils $wpDbUtils */
$wpDbUtils = $gfContainer->get(\PluginContainer\Core\WpDbUtils\WpDbUtils::class);
$attributes = $wpDbUtils->getAvailableAttributes();
$query = $wpDbUtils->getProducts($perPage, $currentPage);
$products = $query['products'];
$maxNumPages = $query['maxNumPages'];
include THEME_DIR . '/templates/shop/shopPageContent.php';
get_footer();
