<?php
/**
 * Template Name: Contact
 */
get_header();
?>
<article class="about">
    <h1>Kontakt</h1>
    <img src="/wp-content/uploads/2022/04/m10.jpeg" alt="<?=__('About image','r4s')?>" />
    <p>Javite nam se na e-mail: <a href="mailto:info@rent4style.com">info@rent4style.com</a>,
    na <a href="https://www.instagram.com/rent4style/">instagram</a>,
    <a href="https://www.facebook.com/IaMatea/" target="_blank">Facebook</a>,
    ili WhatsApp <a href="tel:+385976691110">+385 97 669 1110</a></p>
</article>
<section class="about--me">
    <div class="about--me-img">
        <div>
            <img src="/wp-content/uploads/2022/04/m10.jpeg" alt="<?=__('About image','r4s')?>">
            <img src="/wp-content/uploads/2022/04/m11.jpeg" alt="<?=__('About image','r4s')?>">
            <img src="/wp-content/uploads/2022/04/m12.jpeg" alt="<?=__('About image','r4s')?>">
        </div>
        <a href="https://www.instagram.com/matea.miljan/" target="_blank">
            <svg class="icon">
                <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#instagram" />
            </svg>
            Follow me
        </a>
    </div>
    <div class="about--me-info">
        <h1><span>Bok,</span> ja sam Matea</h1>
        <p>Kreativna i prije svega uporna osoba koja je oživjela svoj mali san, Rent4Style. Davno novinarka, danas možda više i ne tako mlada 😛 poduzetnica.</p>
        <a href="/about">
            Learn more
            <svg class="icon">
                <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#arrow-right-long" />
            </svg>
        </a>
    </div>
</section>
    <div class="about--menu">
        <ul>
            <li><a href="/about" title="about"><?=__('O nama','r4s')?></a></li>
            <li><a href="/faqs" title="faq"><?=__('FAQ','r4s')?></a></li>
            <li class="active"><a href="/contact" title="contact"><?=__('Kontakt','r4s')?></a></li>
        </ul>
    </div>


<?php
get_footer();
