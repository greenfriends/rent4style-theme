<?php
get_header();
if ( have_posts() ):?>
    <div class="container container--main container--space">
        <div class="columns">
            <aside class="column--left">
                <div class="filters--scroll">
                    <div class="links">
                        <h6><?=__('Kategorije', 'gf-theme')?></h6>
                        <ul>
                            <?php $categories = get_terms(['taxonomy' => 'product_cat', 'hide_empty' => true]); ?>
                            <?php foreach ($categories as $category): ?>
                                <?php
                                $catLink = get_term_link($category);
                                $class = '';
                                $actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                if($actualLink === $catLink) {
                                    $class = 'active';
                                }
                                ?>
                                <?php if($category->name === 'Uncategorized') {continue;} ?>
                                <li><a class="<?=$class?>" href="<?=$catLink?>"><?=$category->name?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
            </aside>
            <div class="column--right">
                <div class="products">
                    <?php
                    while ( have_posts() ) : the_post(); ?>
                    <?php
                        $product = wc_get_product(get_the_ID());
                    ?>
                    <div class="product">
                        <figure class="product--image">
                            <a href="<?=$product->get_permalink()?>">
                                <?=$product->get_image('large')?>
                            </a>
                        </figure>
                        <div class="product--info">
                            <h2 class="product--made"><a target="_blank"
                                                         href="<?=$product->get_meta('productMadeUrl')?>"><?=$product->get_meta('productMadeTitle')?></a>
                            </h2>
                            <span class="product--name"><?=$product->get_title()?></span>
                            <?php
                            global $gfContainer;
                            /**
                             * @var \PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion $currencyConversion
                             */
                            $currencyConversion = $gfContainer->get(\PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion::class);
                            $rate = $currencyConversion->getExchangeRate();
                            ?>
                            <span class="product--price"><?=$product->get_price_html()?> <?=$rate && get_woocommerce_currency() === 'HRK' ?  '<span class="ratePrice">/ &euro; ' . number_format($product->get_price() / $rate,2) . '</span>' : ''?></span>
                        </div>
                    </div>
                    <?php
                    endwhile;
                    ?>
                </div>
                <div class="pagination">
                    <?php
                    $currentPage = get_query_var('paged') ?: 1;
                    $maxNumPages = $wp_query->max_num_pages;
                    ?>
                    <a href="<?=get_pagenum_link($currentPage - 1)?>"
                       class="pagination--arrow <?=$currentPage <= 1 ? 'paginationDisabled' : ''?>">
                        <svg class="icon">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#angle-left"/>
                        </svg>
                    </a>
                    <span class="pagination--page"><?=$currentPage . ' / ' . $maxNumPages?></span>
                    <a href="<?=get_pagenum_link($currentPage + 1)?>"
                       class="pagination--arrow <?=$currentPage >= $maxNumPages ? 'paginationDisabled' : ''?>">
                        <svg class="icon">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#angle-right"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>

<?php
else:
    echo '<p style="padding:3rem;">' . __( 'Žao nam je, niti jedna objava se ne podudara s tvojim kriterijima', 'gfShopTheme' ) . '</p>';
endif;
get_footer(); ?>