<?php
get_header();
?>

<div class="staticContainer">
    <p>
        <?=__('UPS, izgleda da ne možemo pronaći stranicu koju tražite....','gf-theme')?>
    </p>
    <p>
        <?=__('Stranica koju tražite ili ne postoji ili je došlo do nekakve greške, nastavite na našu')?>
        <a href="<?=get_home_url()?>" title="home"><?=__('naslovnicu','gf-theme')?>.</a>
    </p>
</div>


<?php
get_footer();
