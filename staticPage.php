<?php
/**
 * Template Name: StaticPage
 */
get_header();
?>
<div class="staticContainer">
    <?php
    /* Start the Loop */
    while ( have_posts() ) :
        echo '<h1>' . get_the_title() . '</h1>';
        the_post();
        the_content();

        // If comments are open or there is at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) {
            comments_template();
        }
    endwhile;// End of the loop.
    ?>
</div>
<?php
get_footer();