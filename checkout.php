<?php
//Template name: Custom Checkout
get_header();
do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
    echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
    return;
}
$checkout->get_checkout_fields();
$customer = WC()->customer;
$firstName = $customer->get_first_name();
$lastName = $customer->get_last_name();
$fullName = $customer->get_first_name() . ' ' . $customer->get_last_name();
$address = $customer->get_billing_address_1();
$city = $customer->get_billing_city();
$countries = WC()->countries->get_countries();
$countryCode = $customer->get_billing_country();
$countryName = $countries[$countryCode];
$postCode = $customer->get_billing_postcode();
$phone = $customer->get_billing_phone();
$email = $customer->get_billing_email();
$street = $customer->get_meta('billing_street');
$streetNumber = $customer->get_meta('billing_street_number');
$apartmentNumber = $customer->get_meta('billing_apartment_number');
$cart = WC()->cart;

?>
    <div class="container container--main container--space">
        <div class="logged">
            <div class="logged--sidebar">
                <form name="checkout" method="post" action="<?=esc_url(wc_get_checkout_url())?>"
                      enctype="multipart/form-data">
                    <section class="checkout">
                        <div class="checkout--top">
                            <h5><?=__('Billing info', 'gf-theme')?></h5>
                        </div>
                        <div class="checkout--billing">
                            <div class="details--input"> <!-- add "error" when empty -->
                                <span>First name</span>
                                <input type="text">
                            </div>
                            <div class="details--input"> <!-- add "error" when empty -->
                                <span>Last name</span>
                                <input type="text">
                            </div>
                            <div class="details--input"> <!-- add "error" when empty -->
                                <span>Address</span>
                                <input type="text"/>
                            </div>
                            <div class="details--input"> <!-- add "error" when empty -->
                                <span>Zip code</span>
                                <input type="text"/>
                            </div>
                            <div class="details--input"> <!-- add "error" when empty -->
                                <span>City</span>
                                <input type="text"/>
                            </div>
                            <button>Save</button>
                        </div>
                    </section>
                    <section class="checkout">
                        <div class="checkout--top">
                            <h5>Summary</h5>
                        </div>
                        <div class="checkout--promo">
                            <input type="text" placeholder="Promo code"/>
                            <button>Apply</button>
                        </div>
                        <div class="checkout--summary">
                            <ul>
                                <li>
                                    <span>Subtotal</span>
                                    <span>€ 61,00</span>
                                </li>
                                <li>
                                    <span>Rental coverage</span>
                                    <span>€ 4,99</span>
                                </li>
                                <li>
                                    <span>Shipping</span>
                                    <span>€ 9,99</span>
                                </li>
                                <li>
                                    <span>Return via carrier</span>
                                    <span>FREE</span>
                                </li>
                                <li>
                                    <span>Tax</span>
                                    <span>FREE</span>
                                </li>
                                <li>
                                    <span>Order total</span>
                                    <span>€ 76,00</span>
                                </li>
                            </ul>
                            <button disabled>Place order</button>
                            <small>By placing this order, you agree to the <a href="text.html">Terms of Service</a> and
                                <a href="text.html">Privacy Policy</a>.</small>
                        </div>
                    </section>
                </form>
            </div>
            <div class="logged--content">
                <h2>Shop</h2>
                <h6>Make your purchase</h6>
                <div class="order--list">
                    <div class="order">
                        <div class="order--top">
                            <h5>Pending order</h5>
                            <span class="order--edit">Save and order</span>
                        </div>
                        <div class="order--info">
                            <div class="order--image">
                                <a href="details.html">
                                    <img src="static/img/m6.jpg" alt=""/>
                                </a>
                            </div>
                            <div class="order--product">
                                <strong>Badgley Mischka</strong>
                                <span>V Neck Thin Hanging Night Dress</span>
                                <span>Size:
                                        <select>
                                            <option>XL</option>
                                            <option>M</option>
                                            <option>S</option>
                                            <option>L</option>
                                            <option>XXL</option>
                                        </select>
                                    </span>
                                <strong>€ 73,00</strong>
                            </div>
                            <div class="order--remove">
                                <svg class="icon">
                                    <use href="static/build/sprite.svg#xmark"/>
                                </svg>
                            </div>
                            <div class="order--dialog">
                                <div class="order--dialog-close">
                                    <svg class="icon">
                                        <use href="static/build/sprite.svg#xmark"/>
                                    </svg>
                                </div>
                                <p>Remove your order</p>
                                <div class="order--dialog-buttons">
                                    <button class="order--dialog-button-no">Nemoj ukloniti</button>
                                    <button class="order--dialog-button-yes">Ukloni!</button>
                                </div>
                            </div>
                        </div>
                        <ul class="order--date-address">
                            <li>
                                <span>Date:</span>
                                <span><input class="popup-calendar--open" type="text"
                                             value="Thu, Apr 21 - Sun, Apr 24"/></span>
                            </li>
                            <li>
                                <span>Name:</span>
                                <span><input type="text" value="Matea Miljan"/></span>
                            </li>
                            <li><!-- class "error" when needed -->
                                <span>Address:</span>
                                <span>
                                        <small>Dostava:</small>
                                        <input type="text" value="" placeholder="Unesite adresu"/>
                                    </span>
                            </li>
                            <li>
                                <span></span>
                                <span>
                                        <small>Prikup:</small>
                                        <input type="text" value="" placeholder="Opciona adresa"/>
                                    </span>
                            </li>
                        </ul>
                    </div>
                    <div class="order">
                        <div class="order--top">
                            <h5>Pending order</h5>
                            <span class="order--edit">Edit</span>
                        </div>
                        <div class="order--info">
                            <div class="order--image">
                                <a href="details.html">
                                    <img src="static/img/m6.jpg" alt=""/>
                                </a>
                            </div>
                            <div class="order--product">
                                <strong>Badgley Mischka</strong>
                                <span>V Neck Thin Hanging Night Dress</span>
                                <span>Size:
                                        <select disabled>
                                            <option>XL</option>
                                            <option>M</option>
                                            <option>S</option>
                                            <option>L</option>
                                            <option>XXL</option>
                                        </select>
                                    </span>
                                <strong>€ 73,00</strong>
                            </div>
                            <div class="order--remove">
                                <svg class="icon">
                                    <use href="static/build/sprite.svg#xmark"/>
                                </svg>
                            </div>
                            <div class="order--dialog">
                                <div class="order--dialog-close">
                                    <svg class="icon">
                                        <use href="static/build/sprite.svg#xmark"/>
                                    </svg>
                                </div>
                                <p>Remove your order</p>
                                <div class="order--dialog-buttons">
                                    <button class="order--dialog-button-no">Nemoj ukloniti</button>
                                    <button class="order--dialog-button-yes">Ukloni</button>
                                </div>
                            </div>
                        </div>
                        <ul class="order--date-address">
                            <li>
                                <span>Date:</span>
                                <span><input class="popup-calendar--open" type="text" value="Thu, Apr 21 - Sun, Apr 24"
                                             disabled/></span>
                            </li>
                            <li>
                                <span>Name:</span>
                                <span><input type="text" value="Matea Miljan" disabled/></span>
                            </li>
                            <li>
                                <span>Address:</span>
                                <span>
                                        <small>Dostava:</small>
                                        <input type="text" value="Patačičkina ul. 29, Zagreb" disabled/>
                                    </span>
                            </li>
                            <li>
                                <span></span>
                                <span>
                                        <small>Prikup:</small>
                                        <input type="text" value="Nikole Tesle ul. 16, stan 12, Zagreb" disabled/>
                                    </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="to-top">
        <span>Top</span>
        <svg class="icon">
            <use href="static/build/sprite.svg#angle-up"/>
        </svg>
    </div>
<?php
get_footer(); ?>