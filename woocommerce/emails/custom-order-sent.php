<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="hr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>?</title>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tbody><tr>
        <td align="center" valign="top">
            <div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#ffffff;border:1px solid #dedede;border-radius:3px">
                <tbody><tr>
                    <td align="center" valign="top">

                        <table border="0" cellpadding="0" cellspacing="0" width="100%"  style="background-color:#96588a;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;border-radius:3px 3px 0 0">
                            <tbody><tr>
                                <td  style="padding:36px 48px;display:block">
                                    <h1 style="font-size:24px;font-weight:300;line-height:150%;margin:0;text-align:left;color:#ffffff;background-color:inherit">Rent4Style proizvod je krenuo prema tebi</h1>
                                </td>
                            </tr>
                            </tbody></table>

                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <p>Draga <?=$userName?>,</p>
                        <p>udahni duboko i vikni yesssss jer je tvoj #rent4style proizvod krenuo prema tebi.</p>

                        <p>Veselimo se što ćeš uskoro nositi našu #rent4style haljinu i što si postala naša #rent4stylegirls. Ne
                            zaboravi da haljinicu trebaš na vrijeme predati nazad GLS-u u ponedjeljak.</p>

                        <p>Javi nam se ako imaš bilo kakvo pitanje.</p>

                        <p>Tvoj Rent4Style tim</p>
                    </td>
                </tr>
                </tbody></table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">

            <table border="0" cellpadding="10" cellspacing="0" width="600">
                <tbody><tr>
                    <td valign="top" style="padding:0;border-radius:6px">
                        <table border="0" cellpadding="10" cellspacing="0" width="100%">
                            <tbody><tr>
                                <td colspan="2" valign="middle"  style="border-radius:6px;border:0;color:#8a8a8a;font-size:12px;line-height:150%;text-align:center;padding:24px 0">
                                    <p style="margin:0 0 16px"><a href="<?=get_home_url()?>" target="_blank"><?=str_replace('https://','',get_bloginfo('url'))?></a></p>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
                </tbody></table>

        </td>
    </tr>
    </tbody></table>
</body>
</html>