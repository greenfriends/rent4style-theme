<?php
use PluginContainer\Packages\R4sBooking\Controller\R4sBooking;

global $gfContainer;
/** @var R4sBooking $calendarController */
$calendarController = $gfContainer->get(R4sBooking::class);
//Template name: Custom Checkout
get_header();
wc_clear_notices();
do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
    echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message',
        __('You must be logged in to checkout.', 'woocommerce')));
    return;
}
$checkout->get_checkout_fields();
$customer = WC()->customer;
$firstName = $customer->get_first_name();
$lastName = $customer->get_last_name();
$fullName = $customer->get_first_name() . ' ' . $customer->get_last_name();
$address = $customer->get_billing_address_1();
$city = $customer->get_billing_city();
$countryCode = 'HR';
$countryName = __('Croatia','gf-theme');
$postCode = $customer->get_billing_postcode();
$phone = $customer->get_billing_phone();
$email = $customer->get_billing_email();
$cart = WC()->cart;
$dates = WC()->session->get('orderDates');

global $gfContainer;
/**
 * @var \PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion $currencyConversion
 */
$currencyConversion = $gfContainer->get(\PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion::class);
$rate = $currencyConversion->getExchangeRate();
?>
    <div class="container container--main container--space">
        <?php if($rate && get_woocommerce_currency() === 'HRK'):?>
        <div style="display:none;" id="currencyConversion"><?=$rate?></div>
        <?php endif;?>
        <div class="logged">
            <div class="logged--sidebar">
                <form id="checkout" name="checkout" method="post" action="<?=esc_url(wc_get_checkout_url())?>"
                      enctype="multipart/form-data">
                    <section class="checkout">
                        <div class="checkout--top">
                            <h5><?=__('Naplata', 'gf-theme')?></h5>
                        </div>
                        <input type="hidden" id="billing_country" name="billing_country" value="<?=$countryCode?>">
                        <div class="checkout--billing">
                            <div class="details--input">
                                <label for="billing_first_name"><?=__('Ime', 'gf-theme')?></label>
                                <input type="text" name="billing_first_name" id="billing_first_name"
                                       value="<?=$firstName?>">
                            </div>
                            <div class="details--input">
                                <label for="billing_last_name"><?=__('Prezime', 'gf-theme')?></label>
                                <input type="text" name="billing_last_name" id="billing_last_name"
                                       value="<?=$lastName?>">
                            </div>
                            <div class="details--input">
                                <label for="billing_email"><?=__('Email ', 'gf-theme')?></label>
                                <input type="text" name="billing_email" id="billing_email" value="<?=$email?>">
                            </div>
                            <div class="details--input">
                                <label for="billing_address_1"><?=__('Adresa', 'gf-theme')?></label>
                                <input type="text" name="billing_address_1" id="billing_address_1"
                                       value="<?=$address?>">
                            </div>
                            <div class="details--input">
                                <label for="billing_phone"><?=__('Broj mobitela', 'gf-theme')?></label>
                                <input type="text" name="billing_phone" id="billing_phone"
                                       value="<?=$phone?>">
                            </div>
                            <div class="details--input">
                                <label for="billing_postcode"><?=__('Poštanski broj', 'gf-theme')?></label>
                                <input type="text" name="billing_postcode" id="billing_postcode" value="<?=$postCode?>">
                            </div>
                            <div class="details--input">
                                <label for="billing_city"><?=__('Grad ', 'gf-theme')?></label>
                                <input type="text" name="billing_city" id="billing_city" value="<?=$city?>">
                            </div>
                        </div>
                    </section>
                    <?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
                    <?php if ($cart->needs_payment()) : ?>
                        <ul class="wc_payment_methods payment_methods methods">
                            <?php
                            $gateways = WC()->payment_gateways->get_available_payment_gateways();
                            if (!empty($gateways)) {
                                foreach ($gateways as $fullName => $gateway) {
//                                            if ($fullName !== 'payway') {
//                                                continue;
//                                            }
                                    ?>
                                    <input id="payment_method_<?php echo esc_attr($gateway->id); ?>" type="radio" class="input-radio"
                                           name="payment_method"
                                           value="<?php echo esc_attr($gateway->id); ?>" <?php checked($gateway->chosen, true); ?>
                                           data-order_button_text="<?php echo esc_attr($gateway->order_button_text); ?>"/>
                                    <?php
                                }
                            } else {
                                echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters('woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__('Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce') : esc_html__('Please fill in your details above to see available payment methods.', 'woocommerce')) . '</li>'; // @codingStandardsIgnoreLine
                            }
                            ?>
                        </ul>
                    <?php endif; ?>
                </form>
                <?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
                    <section class="checkout">
                        <div class="checkout--top">
                            <h5><?=__('Sažetak', 'gf-theme')?></h5>
                        </div>
                        <div class="checkout--promo">
                            <?=do_shortcode('[coupon_field]')?>
                        </div>
                        <div class="checkout--summary">
                            <ul id="checkoutInfo">
                                <li>
                                    <span><?=__('Ukupno', 'gf-theme')?></span>
                                    <span id="subtotal"><?=number_format($cart->get_subtotal(),
                                            2) . ' ' . get_woocommerce_currency_symbol() . ($rate && get_woocommerce_currency() === 'HRK' ? ' / &euro; ' .  number_format($cart->get_subtotal() / $rate,2) : '')?></span>
                                </li>
                                <li>
                                    <span><?=__('Dostava', 'gf-theme')?></span>
                                    <span id="shipping"><?=$cart->get_shipping_total() ? number_format($cart->get_shipping_total(),
                                            2) . ' ' . get_woocommerce_currency_symbol() . ($rate && get_woocommerce_currency() === 'HRK' ? ' / &euro; ' .  number_format($cart->get_shipping_total() / $rate,2) : '') : __('Besplatna', 'gf-theme')?></span>
                                </li>
                                <li>
                                    <span><?=__('Ukupan iznos', 'gf-theme')?></span>
                                    <span id="total"><?=$cart->get_total() . ($rate && get_woocommerce_currency() === 'HRK' ? ' / &euro; ' .  number_format($cart->total / $rate,2) : '')?></span>
                                </li>
                            </ul>

                            <button style="margin-bottom:1rem;" id="placeOrder" form="checkout" name="woocommerce_checkout_place_order" type="submit"><?=__('IDI NA PLAĆANJE','gf-theme')?></button>
                            <?php
                            $termsLinkHtml = '<a title="Terms link" href="' . get_permalink(wc_terms_and_conditions_page_id()) . '">' . __('Opće uvjete poslovanja',
                                    'gf-theme') . '</a>';
                            $privacyPolicyLinkHtml = '<a title="Privacy policy link" href="' . get_permalink(wc_privacy_policy_page_id()) . '">' . __('Izjavu o privatnosti',
                                    'gf-theme') . '</a>';
                            ?>
                            <?=sprintf('Plaćanjem prihvaćate %s kao i  %s.',
                                $termsLinkHtml, $privacyPolicyLinkHtml)?>
                        </div>
                    </section>
            </div>
            <div class="logged--content">

                <h2><?=__('SHOP', 'gf-theme')?></h2>
                <h6><?=__('Unajmi proizvod', 'gf-theme')?></h6>
                <div class="order--list">
                    <div class="order">
                        <div class="order--top">
                            <h5><?=__('Na čekanju', 'gf-theme')?></h5>
                        </div>
                        <?php
                        foreach ($cart->get_cart_contents() as $itemData):
                            /** @var WC_Product $product */
                            $product = $itemData['data'];
                            $productImageUrl = wp_get_attachment_image_url($product->get_image_id(), 'full');
                            $creatorName = $product->get_meta('productMadeTitle');
                            ?>
                            <div class="orderInfoContainer">
                                <div class="order--info" data-variation-id="<?=$itemData['variation_id']?>" data-post-id="<?=$itemData['product_id']?>">
                                    <div class="order--image">
                                        <a href="<?=get_permalink($product->get_id())?>" title="<?=$product->get_title()?>">
                                            <img src="<?=$productImageUrl?>" alt="<?=$product->get_title()?>"/>
                                        </a>
                                    </div>
                                    <div class="order--product">
                                        <strong><?=$creatorName?></strong>
                                        <span><?=$product->get_title()?></span>
                                        <?php
                                        $variations = [];
                                        $attributes = [];
                                        if ($product instanceof WC_Product_Variation) {
                                            /** @var WC_Product_Variable $parentProduct */
                                            $parentProduct = wc_get_product($product->get_parent_id());
                                            if ($parentProduct instanceof WC_Product_Variable) {
                                                $variations = $parentProduct->get_available_variations();
                                                $attributes = $parentProduct->get_variation_attributes();
                                                $variationAttributes = $product->get_variation_attributes();
                                            }
                                        }
                                        ?>
                                        <?php
                                        if (count($variations)): ?>
                                            <div class="attributeWrapper">
                                                <?php
                                                foreach ($attributes as $attributeName => $value):
                                                    $formattedAttributeName = 'attribute_' . $attributeName;
                                                    $fixedAttrName = ucfirst(str_replace('pa_', '', $attributeName));
                                                    if($fixedAttrName === 'Size') {
                                                        $fixedAttrName = 'Veličina'; // @todo fix for every product with size, hardcoded for now
                                                    }
                                                    if($fixedAttrName === 'Color') {
                                                        $fixedAttrName = 'Boja';
                                                    }
                                                    ?>
                                                    <span>
                                                <?=$fixedAttrName?>
                                                <select data-attribute="<?=$attributeName?>" id="variationSelect"
                                                        class="attributeSelect">
                                                    <option value="-1">Odaberi</option>
                                                    <?php
                                                    foreach ($value as $attr):
                                                        $selected = '';
                                                        if (isset($variationAttributes[$formattedAttributeName]) &&
                                                            $variationAttributes[$formattedAttributeName] === $attr) {
                                                            $selected = 'selected';
                                                        }
                                                        ?>
                                                        <option <?=$selected?> value="<?=$attr?>"><?=ucfirst($attr)?></option>
                                                    <?php
                                                    endforeach; ?>
                                                </select>
                                            </span>
                                                <?php
                                                endforeach; ?>
                                            </div>
                                        <?php
                                        endif; ?>
                                        <div class="priceQtyContainer">
                                            <?php
                                            $secondCurrency = '';
                                            if(get_woocommerce_currency() === 'HRK') {
                                                $secondCurrency = '/' .  ($rate ? '&euro; ' . number_format($product->get_price() / $rate,2) : '');
                                            }
                                            ?>
                                            <strong class="priceContainer"><?=number_format($product->get_price(),
                                                                         2) . ' ' . get_woocommerce_currency_symbol() . $secondCurrency?></strong>
                                            <span><?=__('Količina','gf-theme')?>
                                              <input min="0" aria-label="<?=__('Quantity','gf-theme')?>" type="number" name="qty" class="productQty" value="<?=$itemData['quantity']?>">
                                            </span>
                                            <div class="noticeContainer noticeError">

                                            </div>
                                            <div class="noticeContainer noticeSuccess">

                                            </div>
                                        </div>

                                    </div>
                                    <div class="order--remove">
                                        <svg class="icon">
                                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#xmark"/>
                                        </svg>
                                    </div>
                                    <div class="order--dialog">
                                        <div class="order--dialog-close">
                                            <svg class="icon">
                                                <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#xmark"/>
                                            </svg>
                                        </div>
                                        <p><?=__('Ukloni proizvod', 'gf-theme')?></p>
                                        <div class="order--dialog-buttons">
                                            <button class="order--dialog-button-no"><?=__('Nemoj ukloniti',
                                                    'gf-theme')?></button>
                                            <button class="order--dialog-button-yes"><?=__('Ukloni',
                                                    'gf-theme')?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endforeach; ?>
                        <ul class="order--date-address">
                            <li>
                                <span><?=__('Datum:', 'gf-theme')?></span>
                                <span><input form="checkout" class="details--option-calendar popup-calendar--open" type="text"
                                             value="<?= $dates ? formatOrderDatesForInputView($dates) : ''?>"/></span>
                                <?php if(isset($_GET['dateError']) && $_GET['dateError'] === 'true'):?>
                                    <span class="noticeContainer noticeError show"><?=__('Odaberi željeni datum','gf-theme')?></span>
                                <?php endif; ?>
                            </li>
                            <li>
                                <span><?=__('Ime:', 'gf-theme')?></span>
                                <span>
                                    <input form="checkout" aria-label="shipping first name" name="shipping_first_name" type="text"
                                           value="" placeholder="<?=__('Ime', 'gf-theme')?>"/>
                                </span>
                            </li>
                            <li>
                                <span><?=__('Prezime:', 'gf-theme')?></span>
                                <span>
                                    <input form="checkout" aria-label="shipping first name" name="shipping_last_name" type="text"
                                           placeholder="<?=__('Prezime', 'gf-theme')?>"/>
                                </span>
                            </li>
                            <li>
                                <span><?=__('Broj mobitela:', 'gf-theme')?></span>
                                <span>
                                    <input form="checkout" aria-label="shipping phone" name="shipping_phone" type="text"
                                           placeholder="<?=__('Broj mobitela', 'gf-theme')?>"/>
                                </span>
                            </li>
                            <li><!-- class "error" when needed -->
                                <span><?=__('Adresa:', 'gf-theme')?></span>
                                <span>
                                        <small><?=__('Adresa za dostavu:', 'gf-theme')?></small>
                                        <input form="checkout" name="shipping_address_1" aria-label="<?=__('Adresa za dostavu')?>"
                                               type="text" placeholder="<?=__('Adresa za dostavu', 'gf-theme')?>"/>
                                </span>
                            </li>
                            <li>
                                <span></span>
                                <span>
                                        <small><?=__('Adresa za prikup', 'gf-theme')?></small>
                                        <input form="checkout" name="order_comments" aria-label="<?=__('Adresa za prikup', 'gf-theme')?>"
                                               type="text"
                                               placeholder="<?=__('Adresa za prikup (ostavi prazno ako je ista kao adresa za dostavu.)',
                                                   'gf-theme')?>"/>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="popup-calendar popup-hidden">
        <p>Izaberite datum</p>
        <div class="popup-scroll">
            <div class="calendar">
                <div class="calendarContainer">
                    <div class="calendarHeader">
                        <button id="prevMonth">
                            <svg class="icon icon--medium">
                                <use href="<?=THEME_URI . '/assets/images/sprite.svg#angle-left-solid'?>"></use>
                            </svg>
                        </button>
                        <div>
                            <span class="js-calendar-month"></span>
                            <span class="calendarYear"></span>
                        </div>
                        <button id="nextMonth">
                            <svg class="icon icon--medium">
                                <use href="<?=THEME_URI . '/assets/images/sprite.svg#angle-right-solid'?>"></use>
                            </svg>
                        </button>
                    </div>
                    <?php
                    $calendarController->printBookingCalendar();
                    ?>
                </div>
            </div>
            <!--            <div class="calendar--success">Uspešno poslat zahtev. Očekujte odgovor na vaš email.</div>-->
            <div class="calendar--options">
                <div class="calendar--option-date">Ako su vam potrebni termini koji ovdje nisu ponuđeni <span class="popup-calendar-dates--open">izaberi produženi datum</span></div>
            </div>
        </div>
        <div class="popup-buttons">
            <button class="popup-button popup-button--secondary">Zatvori</button>
            <button class="popup-button popup-button--primary">Primijeni</button>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/calendar-base@1.0.0/dist/calendarbase.umd.production.min.js"></script>
<?php
get_footer(); ?>