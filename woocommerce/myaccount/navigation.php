<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
$user = wp_get_current_user();
$userFirstName = $user->get('first_name');
$userLastName = $user->get('last_name');
$logoutUrl = wp_logout_url(home_url());
?>
<div class="logged--user">
    <h4><?="$userFirstName $userLastName"?></h4>
    <a href="<?=$logoutUrl?>" class="logged--user-logout"><?=__('Odjavi se', 'gf-theme')?></a>
    <ul>
        <?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
        <?php if($label === 'Profile') {
                $label = 'Profil';
            }
        ?>
            <li>
                <a href="<?php echo esc_url( wc_get_account_endpoint_url($endpoint)); ?>"><?php echo esc_html($label); ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php do_action('woocommerce_after_account_navigation' );?>
