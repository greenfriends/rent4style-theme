<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;
do_action( 'woocommerce_before_edit_account_form' );
$notices = wc_get_notices();
wc_clear_notices();
?>

<div class="logged--content">
    <h2><?=__('Profil', 'gf-theme')?></h2>
    <h6><?=__('Pregledaj i uredi svoj profil', 'gf-theme')?></h6>
        <?php if (isset($notices['success'])) :?>
            <div class="logged--success"><?=__('Izmjene su sačuvane.', 'gf-theme')?></div>
        <?php endif;
        if (isset($notices['error'])) :?>
            <?php foreach ($notices['error'] as $errors): ?>
                <?php foreach ($errors as $error):?>
                    <?php if (!is_array($error)) :?>
                        <div class="logged--error"><?=$error?></div>
                    <?php endif; ?>
                <?php endforeach;?>
            <?php endforeach;?>
        <?php endif; ?>
    <form class="logged--form woocommerce-EditAccountForm edit-account" action="" method="post">
        <?php do_action('woocommerce_edit_account_form_start'); ?>
        <div>
            <label for="account_first_name"><?php esc_html_e('First name', 'woocommerce'); ?></label>
            <input type="text"  name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr($user->first_name); ?>" />
        </div>
        <div>
            <label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?></label>
            <input type="text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr($user->last_name); ?>" />
        </div>
        <div>
            <label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?></label>
            <input type="email" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr($user->user_email); ?>" />
        </div>
        <div>
            <label for="password_current"><?php esc_html_e('Trenutna lozinka', 'gf-theme' ); ?></label>
            <input type="password" name="password_current" id="password_current" autocomplete="off" placeholder="<?=__('Ostavite prazno ako nema promjena','gf-theme')?>" />
        </div>
        <div>
            <label for="password_1"><?php esc_html_e('Nova lozinka', 'gf-theme' ); ?></label>
            <input type="password" name="password_1" id="password_1" autocomplete="off" placeholder="<?=__('Ostavite prazno ako nema promjena','gf-theme')?>" />
        </div>
        <div>
            <label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
            <input type="password" name="password_2" id="password_2" autocomplete="off" />
        </div>
        <div>
            <label for="dateOfBirth"><?=__('Datum rođenja', 'gf-theme')?></label>
            <input type="date" id="dateOfBirth" name="dateOfBirth" value="<?=$user->get('dateOfBirth')?>"  />
        </div>
        <div>
            <label for="dateOfBirth"><?=__('Instagram profil', 'gf-theme')?></label>
            <input type="text" id="instagram" name="instagram" value="<?=$user->get('instagram')?>"  />
        </div>
        <div>
            <strong><?=__('Tvoja veličina:', 'gf-theme')?></strong>
        </div>
        <div>
            <label for="height"><?=__('Visina:', 'gf-theme')?></label>
            <input type="text" id="height" name="height" value="<?=$user->get('height')?>"/>
        </div>
        <div>
            <label for="weight"><?=__('Težina', 'gf-theme')?></label>
            <input type="text" id="weight" name="weight" value="<?=$user->get('weight')?>"/>
        </div>
        <div>
            <label for="size"><?=__('Veličina:', 'gf-theme')?></label>
            <input type="text" id="size" name="size" value="<?=$user->get('size')?>"/>
        </div>
        <?php do_action('woocommerce_edit_account_form'); ?>
        <div class="logged--button">
            <?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
            <input name="save_account_details" type="submit" value="<?=__('Spremi', 'gf-theme')?>">
            <input type="hidden" name="action" value="save_account_details" />
        </div>
        <?php do_action( 'woocommerce_edit_account_form_end' ); ?>
    </form>
    <?php do_action( 'woocommerce_after_edit_account_form' ); ?>
</div>