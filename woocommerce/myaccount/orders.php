<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_account_orders', $has_orders);

global $wpdb;
$userId = get_current_user_id();
function hasUserCommented($db,$postId,$userId) {
    $table = $db->prefix . 'comments';
    $sql = "SELECT COUNT(*) as count FROM $table WHERE comment_post_ID = $postId AND user_id = $userId";
    return (int)$db->get_results($sql)[0]->count;
}

function hasSevenDaysPassedSinceOrderWasSent($order) {
    $orderDate = $order->get_date_completed();
    if($orderDate) {
        $orderTimestamp = $orderDate->getTimestamp();
        return $orderTimestamp + 60 * 60 * 24 * 7 <= time();
    }
    return false;
}

if ($has_orders) : ?>

    <div class="logged--content">
        <h2><?=__('Narudžba', 'gf-theme')?></h2>
        <h6><?=__('Pogledaj svoju narudžbu', 'gf-theme')?></h6>

        <div class="order--list">
            <?php
            /** @var WC_Order $order */
            foreach ($customer_orders->orders as $order ) :
                $orderClass = 'order order--renting';
                $orderIsCompleted = false;
                if ($order->get_status() === 'completed') {
                    $orderClass = 'order';
                    $orderIsCompleted = true;
                }
                ?>
            <div class="<?=$orderClass?>" data-order-id="<?=$order->get_id()?>">
                <div class="doesntFit">
                    <div class="closeFit">
                        <svg data-num="3" class="icon star">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#xmark"></use>
                        </svg>
                    </div>
                    <p>
                        <?=__('Veličina mi ne odgovara', 'gf-theme')?>
                    </p>
                    <textarea aria-label="<?=__('Poruka', 'gf-theme')?>" placeholder="<?=__('Poruka', 'gf-theme')?>" name="orderNote" class="orderNote"  cols="30" rows="10"></textarea>
                    <div class="actions">
                        <div class="cancel"><?=__('Odustani', 'gf-theme')?></div>
                        <div class="accept"><?=__('Potvrdi', 'gf-theme')?></div>
                    </div>
                </div>
                <div class="order--top myAccount">
                    <h5><?=sprintf(__('Naručeno #%s', 'gf-theme'), $order->get_order_number())?></h5>
                    <h5><?=translateStatus(wc_get_order_status_name($order->get_status()))?></h5>
                    <?php if($order->get_status() !== 'item-does-not-fit'):?>
                        <span class="order--remove myAccount"><?=__('Veličina mi ne odgovara', 'gf-theme')?></span>
                    <?php else :?>
                        <span class="order--remove myAccount"></span>
                    <?php endif; ?>
                </div>
                <div class="order--info myAccount">
                    <?php
                    /** @var WC_Order_Item_Product $item */
                    foreach ($order->get_items() as $item):
                        $parentProduct = wc_get_product($item->get_data()['product_id']);
                        $product = $parentProduct;
                        $attributes = [];
                        $userAlreadyLeftAComment = false;
                        if ($parentProduct->get_type() === 'variable') {
                            $product = wc_get_product($item->get_data()['variation_id']);
                            if(hasUserCommented($wpdb,$parentProduct->get_id(),get_current_user_id()) > 0) {
                                $userAlreadyLeftAComment = true;
                            }
                            foreach ($product->get_attributes() as $name => $value) {
                                $attributeName = str_replace('pa_', '', $name);
                                $attributes[] = ucfirst($attributeName) . ': ' . ucfirst($value);
                            }
                        }
                    ?>
                    <?php //var_dump($item->get_data())?>
                        <div class="order--product myAccount">
                            <div class="order--image">
                                <a href="<?=$product->get_permalink()?>">
                                    <?=$product->get_image('thumbnail')?>
                                </a>
                            </div>
                            <div>
                                <strong><?=$parentProduct->get_meta('productMadeTitle')?></strong>
                                <span><?=$product->get_title()?></span>
                                <?php if ($attributes !== []): ?>
                                    <?php foreach ($attributes as $attribute): ?>
                                        <?php
                                        $attribute = str_replace(['Size', 'Color'], ['Veličina', 'Boja'], $attribute);
                                        ?>
                                        <span><?=$attribute?></span>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <strong><?=__('Cijena: ', 'gf-theme'). $item->get_total() / $item->get_quantity() . ' ' . get_woocommerce_currency_symbol()?></strong>
                                <strong><?=__('Količina: ', 'gf-theme').$item->get_quantity()?></strong>
                                <strong><?=__('Dostava: ', 'gf-theme')?> <?= $order->get_shipping_total() ? number_format($order->get_shipping_total(),
                                            2) . ' ' . get_woocommerce_currency_symbol() : __('Besplatna', 'gf-theme')?></strong>
                                <strong><?=__('Ukupno: ', 'gf-theme') . $item->get_total() . ' '. get_woocommerce_currency_symbol()?></strong>
                            </div>
                            <?php if($orderIsCompleted && !$userAlreadyLeftAComment && !hasSevenDaysPassedSinceOrderWasSent($order)):?>
                                <div class="reviews--item-right reviewHidden">
                                    <div class="reviews--item-desc">
                                        <div class="reviews--item-rate-date">
                                            <div class="details--ratings">
                                                <svg data-num="1" class="icon star">
                                                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#star"></use>
                                                </svg>
                                                <svg data-num="2" class="icon star">
                                                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#star"></use>
                                                </svg>
                                                <svg data-num="3" class="icon star">
                                                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#star"></use>
                                                </svg>
                                                <svg data-num="4" class="icon star">
                                                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#star"></use>
                                                </svg>
                                                <svg data-num="5" class="icon star">
                                                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#star"></use>
                                                </svg>
                                            </div>
                                            <div class="reviews--item-date">  <?=__('Ocijeni','gf-theme')?></div>
                                        </div>
                                        <div class="reviews--item-text orderDesc">
                                            <input aria-label="<?=__('Title', 'gf-theme')?>" placeholder="<?=__('Naslov', 'gf-theme')?>" type="text" name="reviewTitle" class="reviewTitle">
                                            <textarea aria-label="<?=__('Recenzija','gf-theme')?>" class="" placeholder="<?=__('Recenzija','gf-theme')?>" rows="4"></textarea>
                                        </div>
                                    </div>
                                    <div class="reviews--item-image">
                                        <span class="add-image"><?=__('Dodaj fotografiju','gf-theme')?><br>(+)</span>
                                        <input accept="image/*" type="file" class="add-image--input">
                                    </div>
                                </div>
                                <div class="order--button addReview" data-send-text="<?=__('Pošalji recenziju','gf-theme')?>"  data-product-id="<?=$product->get_id()?>">
                                    <button><?=__('Dodaj recenziju','gf-theme')?></button>
                                    <small><?=__('Rok za ostavljane recenzije je 7 dana','gf-theme')?></small>
                                </div>
                            <?php endif;?>
                        </div>
                    <?php endforeach;?>
                    <div class="order--dialog order--dont-fit">
                        <div class="order--dialog-close">
                            <svg class="icon">
                                <use href="static/build/sprite.svg#xmark" />
                            </svg>
                        </div>
                        <p>Remove your order</p>
                        <div class="order--dialog-buttons">
                            <button class="order--dialog-button-no">Nemoj ukloniti</button>
                            <button class="order--dialog-button-yes">Ukloni</button>
                        </div>
                    </div>
                </div>
                <div class="orderTotal">
                    <strong><?=__('Ukupan iznos: ', 'gf-theme') . $order->get_total() . ' '. get_woocommerce_currency_symbol()?></strong>
                </div>
                <ul class="order--date-address">
                    <li>
                        <span><?=__('Datum: ', 'gf-theme')?></span>
                        <span>
                            <input aria-label="order date" type="text" value="<?=$order->get_meta('orderDateView')?>" disabled />
                        </span>
                    </li>
                    <li>
                        <span><?=__('Ime: ', 'gf-theme')?></span>
                        <span>
                            <input aria-label="customer name" type="text"
                                   value="<?=$order->get_shipping_first_name() .' '. $order->get_shipping_last_name() ?>" disabled />
                        </span>
                    </li>

                    <li>
                        <span><?=__('Adresa: ', 'gf-theme')?></span>
                        <span>
                                        <small><?=__('Adresa za dostavu: ', 'gf-theme')?></small>
                                        <input aria-label="shipping address" type="text" value="<?=$order->get_shipping_address_1()?>" disabled />
                                    </span>
                    </li>
                    <li>
                        <span></span>
                        <span>
                                        <small><?=__('Adresa za prikup: ' , 'gf-theme')?></small>
                                        <input aria-label="pickup address" type="text" value="<?=$order->get_customer_note()?>" disabled />
                                    </span>
                    </li>
                </ul>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="pagination">
                        <span class="pagination--arrow">
                             <a href="<?=esc_url( wc_get_endpoint_url( 'orders', $current_page - 1 ) )?>"
                                class="pagination--arrow <?=$current_page <= 1 ? 'paginationDisabled' : ''?>">
                                <svg class="icon">
                                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#angle-left"/>
                                </svg>
                            </a>
                        </span>
            <span class="pagination--page"><?=$current_page . ' / ' . $customer_orders->max_num_pages?></span>
            <a href="<?=esc_url( wc_get_endpoint_url( 'orders', $current_page + 1 ) )?>"
               class="pagination--arrow <?=$current_page >= $customer_orders->max_num_pages ? 'paginationDisabled' : ''?>">
                <svg class="icon">
                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#angle-right"/>
                </svg>
            </a>
        </div>
    </div>
    <div id="hiddenIconStarSolid">
        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#star-solid">
        </use>
    </div>
    <div id="hiddenIconStarHollow">
        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#star">
        </use>
    </div>
<?php endif; ?>
