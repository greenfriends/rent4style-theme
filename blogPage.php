<?php
/**
 * Template Name: Blog Page
 */
get_header();
 $catPost = get_posts(get_cat_ID("Blog post")); //change this
?>
<div class="staticContainer">
    <div class="blogContainer">
<?php
foreach ($catPost as $key => $post):?>
    <?php
        if($key === 0) {
            echo '<h1>' . $post->post_title . '</h1>';
           echo $post->post_content;
        } else {
            $url = $post->guid;
            $title = $post->post_title;
            $date = $post->post_date;
            ?>
            <div class="blogItem">
                <h2>
                    <a href="<?=$url?>" title="<?=$title?>">
                        <?=$title?>
                    </a>
                </h2>
            </div>
            <?php
        }
    ?>
<?php endforeach; ?>
    </div>
</div>
<?php
get_footer();

