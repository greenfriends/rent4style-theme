<?php
/**
 * Template Name: How it works
 */
get_header();
?>

<div class="works">
                <div class="works--top">
                    <div>
                        <h1>Kako unajmiti haljinu?</h1>
                        <p>Ljepotice, prije svega dobro nam došla. Ovdje ćeš pronaći sve informacije kako da <b>#rent4style</b> haljina
                            stigne u tvoje ruke.</p>
                        <p>Iz tjedna u tjedan širimo ponudu kako haljina tako i ostalih komada odjeće. Budi
                            u toku i na našem Instagram profilu <a href="https://www.instagram.com/rent4style/" title="instagram">@rent4style</a>. Među prvima doznaj što sve stiže u ponudu.</p>
                    </div>
                </div>
                <div class="container container--main">
                    <div class="works--center">
                        <h2>Izaberi proizvod</h2>
                        <p>U našem webshopu čekaju te dostupni proizvodi prikazani kroz fotografiju i video
                            kako bi dobila što točniji uvid u ono što posuđuješ.</p>
                    </div>
                    <section class="works--items">
                        <picture>
                            <img src="<?=get_home_url()?>/wp-content/uploads/2022/06/Naslovnica-2-768x512.jpg" alt="Haljine za leto" />
                        </picture>
                        <div>
                            <h2>Izaberi veličinu i datum</h2>
                            <p>Izaberi veličinu (škicni koje je veličine i model kako bi ti bilo lakše usporediti,
                                a pogledaj i recenzije <b>#rent4style</b> djevojaka).</p>
                            <p>Ako i dalje imaš dvojbi pitaj nas na našim društvenim
                                mrežama ili nam pošalji poruku na WhatsApp. Sada kada znaš veličinu bukiraj željeni datum. Ako je
                                haljina zauzeta, klikni u kalendaru na opciju da ti stigne obavijest ako se oslobodi ili ako nabavimo
                                dodatne zalihe proizvoda.</p>
                        </div>
                    </section>
                    <section class="works--items works--items-right">
                        <div>
                            <h2>Nakon naručivanja</h2>
                            <p>Nakon što si naručila haljinu dostavna služba je dostavlja u pravilu u četvrtak na adresu koju si navela
                                i u ponedjeljak dolazi po našoj narudžbi po nju na navedenu adresu (stavi adresu na kojoj ćeš biti u
                                ponedjeljak jer ako haljina nije predana na vrijeme nažalost stižu penali jer ne želimo da kasni idućoj
                                <b>#rent4style</b> djevojci).</p>
                        </div>
                        <picture>
                            <source srcset="<?=get_home_url()?>/wp-content/uploads/2022/06/Naslovnica_1_2-600x400.jpg" media="(max-width: 480px)" />
                            <img src="<?=get_home_url()?>/wp-content/uploads/2022/06/Naslovnica_1_2-600x400.jpg" alt="Haljine za leto" />
                        </picture>
                    </section>
                    <section class="works--items">
                        <picture>
                            <source srcset="<?=get_home_url()?>/wp-content/uploads/2022/06/Naslovnica_1_1-600x400.jpg" media="(max-width: 480px)" />
                            <img src="<?=get_home_url()?>/wp-content/uploads/2022/06/Naslovnica_1_1-600x400.jpg" alt="Haljine za leto" />
                        </picture>
                        <div>
                            <h2>Unajmi, nosi, vrati, ponovi</h2>
                            <p>Haljina stiže čista, vraćaš je nošenu. Ubaciš je u ambalažu u kojoj je stigla
                                s trakicom i ceduljicom i ubaciš u torbu u kojoj je dostavljena. Na poleđini okreneš papir kako bi
                                dostavna služba znala gdje staviti novu ceduljicu. Ne brini, oni će sve isprintati i staviti. U slučaju
                                oštećenja haljinice bit će naplaćena šteta. Haljinu možeš najkasnije naručiti u utorak do 17h ako je
                                dostupna i želiš je za vikend koji stiže. Ako je hitno, a utorak je već prošao javi nam se na neku od
                                društvenih mreža i vidjet ćemo što možemo (a najčešće možemo puno toga). &#10084;&#65039;</p>
                        </div>
                    </section>
                    <div class="works--list space" style="margin-top:2rem;">
                    <?php dynamic_sidebar('belowHomepageBanner') ?>
                    </div>
                </div>
                <div class="container container--main">
                    <section class="works--list questions" style="margin-top:0;">
                        <h2>Još uvijek imaš pitanje?</h2>
                        <ul>
                            <li>
                                <svg class="icon icon--large">
                                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#instagram" />
                                </svg>
                                <div>
                                    <h3>Pronađi nas na društvenim mrežama</h3>
                                    <p>Pošalji nam poruku na naš Instagram (<a target="_blank" href="https://www.instagram.com/rent4style/" title="instagram">@rent4style</a>) ili
                                        Facebook <a target="_blank" href="https://www.facebook.com/IaMatea" title="facebook">profil</a>.</p>
                                </div>
                            </li>
                            <li class="sendEmail">
                                <svg class="icon icon--large">
                                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#envelope" />
                                </svg>
                                <div>
                                    <h3>Pošalji nam mail</h3>
                                    <p>Pošalji nam upit mailom na <a title="mail" href="mailto:info@rent4style.com">info@rent4style.com</a></p>
                                </div>
                            </li>
                            <li class="center">
                                <svg class="icon icon--large">
                                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#phone-volume" />
                                </svg>
                                <div>
                                    <h3>Pošalji nam poruku na WhatsApp</h3>
                                    <p>Uvijek nam se možeš javiti i porukom na naš WhatsApp na broj
                                        <a href="tel:++385976691110">++385976691110</a></p>
                                </div>
                            </li>
                        </ul>
<!--                        <div class="works&#45;&#45;button"><a href="contact.html">Go to Contact page</a></div>-->
                    </section>
                </div>
            </div>
<?php
get_footer();
