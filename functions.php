<?php
use PluginContainer\Packages\R4sBooking\Service\BookedProduct as Service;
define('THEME_DIR', __DIR__);
define('THEME_URI', get_template_directory_uri());
add_action('wp_enqueue_scripts', function ()
{
    wp_enqueue_style('rent4styleCss', get_template_directory_uri() . '/style.css?version=' . time());
});
add_theme_support('custom-logo');
add_theme_support('menus');
add_theme_support('widgets');
add_theme_support('timeline');
add_theme_support('post-thumbnails');

add_action('after_setup_theme', 'registerThemeMenus');
function registerThemeMenus()
{
    //@todo move from here to a config ?
    $nav = [
        'Primary Menu',
        'Footer One',
        'Footer Two',
        'Footer Three',
        'Footer Four',
        'Aside Menu'
    ];
    foreach ($nav as $menuName) {
        if (!wp_get_nav_menu_object($menuName)) {
            $menuId = wp_create_nav_menu($menuName);
            $locations = get_theme_mod('nav_menu_locations');
            $locations[$menuId] = $menuId;
            set_theme_mod('nav_menu_locations', $locations);
        }
    }
}

// scripts @todo move


add_action('wp_enqueue_scripts', function ()
{
    wp_enqueue_script('r4sMainJs', get_template_directory_uri() . '/assets/js/main.js', [], time(), true);
});

add_action('add_meta_boxes', 'create_custom_meta_box');
function create_custom_meta_box()
{
    add_meta_box(
        'additionalProductInfo',
        __('Additional Product Information', 'gf-theme'),
        'additionalProductInfoContent',
        'product',
        'normal'
    );

     add_meta_box(
        'userImageReview',
        __('User Submitted Data', 'gf-theme'),
        'imageReviewMetaBox',
        'comment',
        'normal'
    );

     add_meta_box(
         'userDimensions',
         __('User Dimensions', 'gf-theme'),
         'userDimensionsMetaBox',
         'comment',
         'normal'
     );

     add_meta_box(
       'productVideo',
       __('Product Video', 'gf-theme'),
       'productVideoMetaBox',
       'product',
       'side',
       'low'
     );
}

function productVideoMetaBox($post) { ?>
    <?php
        $productVideoFileName = get_post_meta($post->ID, 'productVideoName',true) ?? '';
        $productVideoUrl = get_post_meta($post->ID, 'productVideoUrl',true) ?? '';
     ?>
    <button style="margin-bottom:1rem;" id="setProductVideo" class="button button-primary button-large"><?=__('Choose a video for the product', 'r4ss')?></button>
    <input type="hidden" id="productVideoUrl" value="<?=$productVideoUrl?>" name="productVideoUrl">
    <input type="hidden" id="productVideoFileName" value="<?=$productVideoFileName?>" name="productVideoFileName">
    <input aria-label="<?=__('File Name','gf-theme')?>" type="text" id="productVideoFileNameDisplay" readonly name="productVideoFileNameDisplay" value="<?=$productVideoFileName?>">

    <button id="removeVideo" style="margin-bottom:1rem;margin-top:1rem;" class="button button-primary button-large"><?=__('Remove video', 'r4ss')?></button>
    <script>
    let productVideoUrlInput = jQuery('#productVideoUrl');
    let productVideoFileName = jQuery('#productVideoFileNameDisplay');
    let productVideoFileInput = jQuery('#productVideoFileName');

    document.getElementById('removeVideo').addEventListener('click', (e) => {
        e.preventDefault();
       productVideoFileInput.val('');
       productVideoFileName.val('');
       productVideoUrlInput.val('');
    });

    jQuery('#setProductVideo').click(function(e) {
    e.preventDefault();
    let video = wp.media({
        title: '<?=__('Add Video')?>',
        multiple: false,
        library: {type: 'video/MP4'},
    }).open()
        .on('select', function(e){
            let uploadedVideo = video.state().get('selection').first().toJSON();
            productVideoUrlInput.val(uploadedVideo.url);
            productVideoFileName.val(uploadedVideo.filename);
            productVideoFileInput.val(uploadedVideo.filename);
        });
    });
    </script>
<?php }

add_action('save_post', 'saveProductVideo', 10, 2);

function saveProductVideo($postId, $post)
{
    if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || $post->post_type !== 'product') {
        return $postId;
    }
    update_post_meta($postId, 'productVideoName', wp_kses_post($_POST['productVideoFileName'] ?? ''));
    update_post_meta($postId, 'productVideoUrl', wp_kses_post($_POST['productVideoUrl'] ?? ''));
    return $postId;
}

function imageReviewMetaBox($post) {
    $imageReviewUrl = get_comment_meta($post->comment_ID,'user_image_url');
    $age = get_comment_meta($post->comment_ID, 'age', true);
    $wornTo = get_comment_meta($post->comment_ID, 'worn_to', true);
    $bodyType = get_comment_meta($post->comment_ID, 'body_type', true);
    $title = get_comment_meta($post->comment_ID, 'title', true);
    if(isset($imageReviewUrl[0])) {
        echo '<img style="width:auto;max-height:500px;object-fit:contain;" src="' . $imageReviewUrl[0] .'" alt="Image Review">';
        echo '<div><h1>' . $title .'</h1></div>';
        echo '<div><span>Age: ' . $age .'</span></div>';
        echo '<div><span>Worn To: ' . $wornTo .'</span></div>';
        echo '<div><span>Body Type: ' . $bodyType .'</span></div>';
    }
}

function userDimensionsMetaBox($post) {
    $size = get_comment_meta($post->comment_ID, 'user_size', true);
    $weight = get_comment_meta($post->comment_ID, 'user_weight', true);
    $height = get_comment_meta($post->comment_ID, 'user_height', true);
    echo <<<HTML
        <label>
            Size
            <input type="text" value="$size" name="userSize">
        </label>
         <label>
            Weight
            <input type="text" value="$weight" name="userWeight">
        </label>
         <label>
            Height
            <input type="text" value="$height" name="userHeight">
        </label>
    HTML;
}

add_action('edit_comment', function($commentId, $data) {
    if(isset($_POST['userSize'])) {
         update_comment_meta($commentId, 'user_size', $_POST['userSize']);
    }
    if(isset($_POST['userWeight'])) {
        update_comment_meta($commentId, 'user_weight', $_POST['userWeight']);
    }
    if(isset($_POST['userHeight'])) {
        update_comment_meta($commentId, 'user_height', $_POST['userHeight']);
    }
},10,2);
/**
 * @param $post
 * @return void
 * @used-by create_custom_meta_box()
 */
function additionalProductInfoContent($post)
{
    $sizeAndFit = get_post_meta($post->ID, 'sizeAndFit', true) ?? '';
    $stylistAdvice = get_post_meta($post->ID, 'stylistAdvice', true) ?? '';
    $args['textarea_rows'] = 6;
    echo '<p>' . __('Size and Fit', 'gf-theme') . '</p>';
    wp_editor($sizeAndFit, 'sizeAndFit', $args);
    echo '<p>' . __('Stylist Advice', 'gf-theme') . '</p>';
    wp_editor($stylistAdvice, 'stylistAdvice', $args);
}

add_action('save_post', 'saveAdditionalProductInfo', 10, 2);

function saveAdditionalProductInfo($postId, $post)
{
    if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || $post->post_type !== 'product') {
        return $postId;
    }
    update_post_meta($postId, 'sizeAndFit', wp_kses_post($_POST['sizeAndFit'] ?? ''));
    update_post_meta($postId, 'stylistAdvice', wp_kses_post($_POST['stylistAdvice'] ?? ''));
    return $postId;
}

add_action('woocommerce_product_options_inventory_product_data', 'originalPriceProductField');
/**
 * Displays the custom text field input field in the WooCommerce product data meta box
 */
function originalPriceProductField()
{
    $args = [
        'id' => 'originalPrice',
        'label' => __('Original Price', 'gf-theme')
    ];
    woocommerce_wp_text_input($args);
}

add_action('woocommerce_process_product_meta', 'saveOriginalPriceProductField');

function saveOriginalPriceProductField($postId)
{
    $product = wc_get_product($postId);
    $field = $_POST['originalPrice'] ?? '';
    $product->update_meta_data('originalPrice', sanitize_text_field($field));
    $product->save();
}

register_sidebar([
    'name' => 'homepageSlider_1',
    'id' => 'homepageSlider_1',
    'before_sidebar' => '<div>',
    'after_sidebar' => '</div>',
    'before_widget' => '<div class="container container--main">',
    'after_widget' => '</div>'
                 ]);
register_sidebar([
    'name' => 'homepageSlider_2',
    'id' => 'homepageSlider_2',
    'before_sidebar' => '<div>',
    'after_sidebar' => '</div>',
    'before_widget' => '<div class="container container--main">',
    'after_widget' => '</div>'
                 ]);
register_sidebar([
    'name' => 'homepageShowCase_1',
    'id' => 'homepageShowCase_1',
    'before_sidebar' => '<div>',
    'after_sidebar' => '</div>',
    'before_widget' => '<div>',
    'after_widget' => '</div>'
                 ]);
register_sidebar([
    'name' => 'homepageReviews',
    'id' => 'homepageReviews',
    'before_sidebar' => '<div>',
    'after_sidebar' => '</div>',
    'before_widget' => '<div>',
    'after_widget' => '</div>'
                 ]);
register_sidebar([
    'name' => 'belowHomepageReviews',
    'id' => 'belowHomepageReviews',
    'before_sidebar' => '<div>',
    'after_sidebar' => '</div>',
    'before_widget' => '<div>',
    'after_widget' => '</div>'
                 ]);
register_sidebar([
    'name' => 'belowHomepageBanner',
    'id' => 'belowHomepageBanner',
    'before_sidebar' => '<div>',
    'after_sidebar' => '</div>',
    'before_widget' => '<div>',
    'after_widget' => '</div>'
                 ]);
register_sidebar([
    'name' => 'aboveFooter',
    'id' => 'aboveFooter',
    'before_sidebar' => '<div>',
    'after_sidebar' => '</div>',
    'before_widget' => '<div>',
    'after_widget' => '</div>'
                 ]);

// Slider override

add_filter('sliderHeader',function($slider) {
    if($slider) {
    ?>
    <div class="view--more-top center">
        <h6><?=$slider->getTitle()?></h6>
        <span><a title="slider header link <?=$slider->getTitle()?>" href="<?=$slider->getLinkTo()?>"><?=__('Pogledaj sve','gf-theme')?></a></span>
    </div>
    <?php }
},9);

add_filter('sliderBeforeLoopItem', function ($product)
{
    if($product) {
        ?>
        <div class="product">
        <?=\PluginContainer\Packages\Wishlist\Controller\Wishlist::getWihslistIconHtml($product)?>
        <?php
    }
},9);

add_filter('sliderAfterLoopItem', function ($product)
{
    if($product) { ?>
        </div> <!-- this closes the product div from sliderBeforeLoopItem -->
        </div>  <!-- this closes the product--info div from sliderBeforeItemTitle  -->
        <?php
     }
},9);

add_filter('sliderBeforeItemTitle', function ($product)
{
    if($product) { ?>
        <figure class="product--image">
            <a href="<?=$product->get_permalink()?>" title="<?=$product->get_title()?>">
                <?= woocommerce_get_product_thumbnail('large') ?>
            </a>
        </figure>
        <div class="product--info">
    <?php }
},9);

add_filter('sliderItemTitle', function ($product)
{
    global $gfContainer;
    /**
     * @var \PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion $currencyConversion
     */
    $currencyConversion = $gfContainer->get(\PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion::class);
    $rate = $currencyConversion->getExchangeRate();
    $currency = get_woocommerce_currency();
    $secondCurrency = '';
    if($currency === 'HRK') {
        $secondCurrency = $rate ? '<span class="ratePrice">/ &euro; ' . number_format($product->get_price() / $rate,2) . '</span>' : '';
    }

    if($product) {
        //Remove action for product title because we are not in loop and wc can't get correct title
        remove_action('woocommerce_template_loop_product_title', 'woocommerce_template_loop_product_title', 10); ?>
        <h2 class="product--made">
            <a href="<?=$product->get_meta('productMadeUrl')?>" title="<?=$product->get_meta('productMadeTitle')?>"><?=$product->get_meta('productMadeTitle')?></a>
        </h2>
        <span class="product--name"><?=$product->get_title()?></span>
                            <span class="product--price"><?=$product->get_price_html()?> <?=$secondCurrency?></span>

    <?php }
},9);

 add_filter('sliderAfterItemTitle', function ($product) {},9); // we need it empty, cuz wc adds the price html here



// variations check on add to cart
add_action('wp_ajax_gfAddToCart', 'gfAddToCart' ); // executed when logged in
add_action('wp_ajax_nopriv_gfAddToCarty', 'gfAddToCart' ); // executed when logged out


function gfAddToCart() {
    $postId = $_POST['postId'] ?? null;
    $variationId = $_POST['variationId'] ?? null;
    $dates = $_POST['dates'] !== 'undefined' ? $_POST['dates'] : null;
    if(!$dates) {
        $datesFromSession = WC()->session->get('orderDates');
        if($datesFromSession) {
            $dates = $datesFromSession;
        }
    } else {
        $dates = json_decode(stripslashes($dates));
    }
    if(!$postId || !$variationId) {
        wp_send_json(json_encode(['success' => false,'message' => __('A problem occurred while adding the product, please refresh the page and try again','gf-theme')]));
    }
    if(!$dates) {
        wp_send_json(json_encode(['success' => false,'message' => __('Odaberi željeni datum','gf-theme')]));
    }
   try {
    global $gfContainer;
    /**
    * @var \PluginContainer\Packages\R4sBooking\Controller\R4sBooking $booking
    */
    $booking = $gfContainer->get(\PluginContainer\Packages\R4sBooking\Controller\R4sBooking::class);
    $available = $booking->checkIfProductIsAvailableForBooking($variationId, serialize($dates), 1,false,true);
    if($available['success'] === false) {
        wp_send_json(json_encode(['success' => false,'message' => $available['message']]));
    }
    } catch(Exception $e) {
        $logger = $gfContainer->get(\PluginContainer\Core\Logger\Logger::class);
        $logger->error('Error in adding product to cart', ['error' => $e->getMessage(),'trace' => $e->getTraceAsString(), 'productId' => $postId]);
             wp_send_json(json_encode(['success' => false,'message' => __('A problem occurred while adding the product, please refresh the page and try again','gf-theme')]));
    }
    \WC()->cart->add_to_cart($postId, 1, $variationId);
    $notices = wc_get_notices();
    if(isset($notices['error'])) {
        foreach($notices['error'] as $notice) {
            if(isset($notice['notice']) && $notice['notice'] !== '') {
                switch($notice['notice']) {
                    case strpos($notice['notice'],'You cannot add that amount to the cart') > 0:
                        wc_clear_notices();
                        wp_send_json(json_encode(['success' => false,'message' => __('Maximum quantity for the selected product has been reached','gf-theme')]));
                        break;
                }
            }
        }
    }
    WC()->session->set( 'orderDates', $dates);
    wp_send_json(json_encode(['success' => true,'message' => __('Proizvod je dodan u košaricu','gf-theme')]));
}
add_action('wp_ajax_checkVariationAvailability', 'checkVariationAvailability' ); // executed when logged in
add_action('wp_ajax_nopriv_checkVariationAvailability', 'checkVariationAvailability' ); // executed when logged out
function checkVariationAvailability() {
     $postId = $_POST['postId'];
     $data = null;
     if($_POST['data']) {
       $data = json_decode(stripslashes($_POST['data']));
     }
     $dataLength = count($data);
     if($postId && $data) {
         $product = wc_get_product($postId);
         if ($product instanceof WC_Product_Variable) {
             $variations = $product->get_available_variations();
             foreach($variations as $variation) {
                 $attributes = $variation['attributes'];
                 $count = 0;
                foreach($data as $dataObj) {
                    // if qty is important add && $variation['max_qty'] !== ''
                    if(array_key_exists('attribute_' . $dataObj->attribute,$attributes) &&
                        $attributes['attribute_' . $dataObj->attribute] === $dataObj->value) {
                        $count++;
                    }
                }
                if($count === $dataLength) {
                    wp_send_json(json_encode(['success' => true,'variationId' => $variation['variation_id']]));
                }
             }
         }
     }
     wp_send_json(json_encode(['success' => false,'message' => __('This product is not available','gf-theme')]));
}

add_action('wp_ajax_changeVariation', 'changeVariation' ); // executed when logged in
add_action('wp_ajax_nopriv_changeVariation', 'changeVariation' ); // executed when logged out
function changeVariation() {
    $postId = $_POST['postId'];
    $quantity = $_POST['quantity'];
    $oldVariationId = $_POST['oldVariationId'];
     $data = null;
     if($_POST['data']) {
       $data = json_decode(stripslashes($_POST['data']));
     }
     $dataLength = count($data);
     if($postId && $data && $quantity && $oldVariationId) {
         $product = wc_get_product($postId);
         if ($product instanceof WC_Product_Variable) {
             $variations = $product->get_available_variations();
             foreach($variations as $variation) {
                 $attributes = $variation['attributes'];
                 $count = 0;
                foreach($data as $dataObj) {
                    if(array_key_exists('attribute_' . $dataObj->attribute,$attributes) &&
                        $attributes['attribute_' . $dataObj->attribute] === $dataObj->value) {
                        $count++;
                    }
                }
                if($count === $dataLength) {
                    removeOldVariationFromCart($oldVariationId);
                    WC()->cart->add_to_cart($postId, $quantity, $variation['variation_id']);
                    wp_send_json(json_encode(['success' => true,'variationId' => $variation['variation_id'],'price' => number_format($variation['display_price'],2) . ' ' .  get_woocommerce_currency_symbol()]));
                }
             }
         }
     }
     wp_send_json(json_encode(['success' => false,'message' => __('This product is not available','gf-theme'),'available' => 'no']));
}
function removeOldVariationFromCart($oldVariationId) { // this is the same as removeVariationFromCart, but the ajax one bugs if parameters are passed
     foreach (WC()->cart->get_cart() as $item_key => $item ) {
            if ($item['variation_id'] === (int)$oldVariationId) {
                WC()->cart->remove_cart_item($item_key);
                return true;
            }
        }
     return false;
}

add_action('wp_ajax_removeVariationFromCart', 'removeVariationFromCart'); // executed when logged in
add_action('wp_ajax_nopriv_removeVariationFromCart', 'removeVariationFromCart'); // executed when logged out
function removeVariationFromCart() {
    $variationId = $_POST['variationId'];
    if($variationId) {
        foreach (WC()->cart->get_cart() as $item_key => $item ) {
            if ($item['variation_id'] === (int)$variationId) {
                WC()->cart->remove_cart_item($item_key);
                wp_send_json(json_encode(['success' => true,'message' => __('Product has been successfully removed','gf-theme')]));
            }
        }
    }
    wp_send_json(json_encode(['success' => false,'message' => __('A problem occurred while removing the product, please refresh the page and try again','gf-theme')]));
}
add_action('wp_ajax_removeDateFromSession', 'removeDateFromSession' ); // executed when logged in
add_action('wp_ajax_nopriv_removeDateFromSession', 'removeDateFromSession' ); // executed when logged out
function removeDateFromSession() {
    WC()->session->set('orderDates',null);
     wp_send_json(json_encode(['success' => true]));
}

add_action('wp_ajax_regenerateCart', 'regenerateCart' ); // executed when logged in
add_action('wp_ajax_nopriv_regenerateCart', 'regenerateCart' ); // executed when logged out
function regenerateCart() {
    global $gfContainer;
    /**
     * @var \PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion $currencyConversion
     */
    $currencyConversion = $gfContainer->get(\PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion::class);
    $rate = $currencyConversion->getExchangeRate();

    $cart = WC()->cart;
    $subTotal = number_format($cart->get_subtotal(),2) . ' ' . get_woocommerce_currency() .
    ($rate && get_woocommerce_currency() === 'HRK' ? ' / &euro; ' .  number_format($cart->get_subtotal() / $rate,2) : '');
    $totalProducts = $cart->get_total();
    $total = $cart->get_total() . ($rate && get_woocommerce_currency() === 'HRK' ? ' / &euro; ' .  number_format($cart->total / $rate,2) : '');
    wp_send_json(json_encode(['success' => true,
    'data' => [
            'subtotal' => $subTotal,
            'totalProducts' => $totalProducts,
            'total' => $total
            ]
    ]));
}
add_action('wp_ajax_updateDatesInSession', 'updateDatesInSession' ); // executed when logged in
add_action('wp_ajax_nopriv_updateDatesInSession', 'updateDatesInSession' ); // executed when logged out
function updateDatesInSession() {
    $dates = $_POST['dates'] !== 'undefined' ? $_POST['dates'] : null;
    if($dates) {
        $dates = json_decode(stripslashes($dates));
        WC()->session->set( 'orderDates', $dates);
        wp_send_json(json_encode(['success' => true]));
     }
}
add_action('wp_ajax_changeQuantity', 'changeQuantity' ); // executed when logged in
add_action('wp_ajax_nopriv_changeQuantity', 'changeQuantity' ); // executed when logged out
function changeQuantity() {
    $cart = WC()->cart;
    $variationId = $_POST['variationId'] ?? null;
    $quantity = $_POST['quantity'] ?? null;
    if($variationId && $quantity) {
        foreach($cart->get_cart_contents() as $cartItemKey => $item) {
            if($item['variation_id'] === (int)$variationId) {
                $maxQuantity = $item['data']->get_meta('numberOfItems');
                if($maxQuantity < (int)$quantity) {
                    wp_send_json(json_encode(['success' => false, 'message' => __('The maximum quantity for this item is ') . $maxQuantity]));
                }
                $cart->set_quantity($cartItemKey, $quantity);
            }
        }
        wp_send_json(json_encode(['success' => true, 'quantity' => $quantity]));
    }
}
function formatOrderDatesForInputView($dates) {
    $lastIndex = count($dates) - 1;
    return sprintf(
        '%s-%s-%s - %s-%s-%s',
        formatDay($dates[0]->day),
        formatMonth($dates[0]->month),
        $dates[0]->year,
        formatDay($dates[$lastIndex]->day),
        formatMonth($dates[$lastIndex]->month),
        $dates[$lastIndex]->year
    );
}
function formatMonth($month) {
        if ($month < 10) {
            return '0' . $month;
        }
        return $month;
    }
function formatDay($day) {
    if ($day < 10) {
        return '0' . $day;
    }
    return $day;
}

add_shortcode( 'coupon_field', 'promoCouponField' );
function promoCouponField() {
    $message = '';
    $coupon = '';
    if(isset($_GET['coupon'])){
        if($coupon = esc_attr($_GET['coupon'])) {
            $applied = WC()->cart->apply_coupon($coupon);
        } else {
            $applied = false;
        }
        $success = sprintf( __('Kupon "%s" uspješno primijenjen.', 'gf-theme'), $coupon );
        $error   = __("Kupon nije dostupan", 'gf-theme');
        $message = isset($applied) && $applied ? $success : $error;
    }

    $output  = '<form id="coupon-redeem">
    <input value="' . $coupon .'" type="text" name="coupon" id="coupon" placeholder="'.__('Promo kod', 'gf-theme').'"/>
    <button type="submit">'.__('Primijeni', 'gf-theme').'</button>';

    $output .= isset($coupon) ? '<p class="result">'.$message.'</p>' : '';

    return $output . '</form>';
}

remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);


// unset from checkout
add_filter('woocommerce_checkout_fields', 'removeFieldsAddress');
function removeFieldsAddress($fields) {
    $fields['shipping']['shipping_address_2']['required'] = true;
     $fields['billing']['billing_state']['required'] = false;
     return $fields;
}

add_action( 'woocommerce_checkout_update_order_meta', 'customCheckoutFieldHandle' );
function customCheckoutFieldHandle( $order_id ) {
    update_post_meta( $order_id, '_shipping_address_1', sanitize_text_field( $_POST['shipping_address_1'] ) );
}



//add_action('init',function() {
//    wp_enqueue_script( 'mainModule', THEME_URI . '/assets/js/mainModule.js', [], '1.0.0');
//});
//add_filter('script_loader_tag', 'addModuleTypeToMainModuleScript' , 10, 3);
//function addModuleTypeToMainModuleScript($tag, $handle, $src) {
//     if ('mainModule' !== $handle ) {
//        return $tag;
//    }
//    return '<script type="module" src="' . esc_url( $src ) . '"></script>';
//}

//add_action('init',function() {
//    wp_enqueue_script( 'mainModule2', THEME_URI . '/assets/js/mainModule2.js', [], '1.0.0');
//});
//add_filter('script_loader_tag', 'addModuleTypeToMainModule2Script' , 10, 3);
//function addModuleTypeToMainModule2Script($tag, $handle, $src) {
//     if ('mainModule2' !== $handle ) {
//        return $tag;
//    }
//    return '<script type="module" src="' . esc_url( $src ) . '"></script>';
//}
//add_action('init',function() {
//    wp_enqueue_script( 'mainModule3', THEME_URI . '/assets/js/mainModule3.js', [], '1.0.1');
//});
//add_filter('script_loader_tag', 'addModuleTypeToMainModule3Script' , 10, 3);
//function addModuleTypeToMainModule3Script($tag, $handle, $src) {
//     if ('mainModule3' !== $handle ) {
//        return $tag;
//    }
//    return '<script type="module" src="' . esc_url( $src ) . '"></script>';
//}

//add_action('init',function() {
//    wp_enqueue_script( 'mainModule4', THEME_URI . '/assets/js/mainModule4.js', [], '1.0.1');
//});
//add_filter('script_loader_tag', 'addModuleTypeToMainModule4Script' , 10, 3);
//function addModuleTypeToMainModule4Script($tag, $handle, $src) {
//     if ('mainModule4' !== $handle ) {
//        return $tag;
//    }
//    return '<script type="module" src="' . esc_url( $src ) . '"></script>';
//}
add_action('init',function() {
    wp_enqueue_script( 'mainModule5', THEME_URI . '/assets/js/mainModule5.js', [], '1.0.1');
});
add_filter('script_loader_tag', 'addModuleTypeToMainModule5Script' , 10, 3);
function addModuleTypeToMainModule5Script($tag, $handle, $src) {
     if ('mainModule5' !== $handle ) {
        return $tag;
    }
    return '<script type="module" src="' . esc_url( $src ) . '"></script>';
}
//My Account
add_filter('woocommerce_account_menu_items', 'removeAccountTabs', 99);

function removeAccountTabs($items) {
    unset($items['downloads'], $items['edit-address'], $items['dashboard']);
    $items['edit-account'] = __('Profile', 'gf-theme');
    return $items;
}

function removePasswordStrengthMeter($strength):int {
    return 0;
}
add_filter( 'woocommerce_min_password_strength', 'removePasswordStrengthMeter' );

add_filter('woocommerce_save_account_details_required_fields', 'myAccountRequiredFields');
function myAccountRequiredFields($required_fields){
    unset($required_fields['account_display_name']);
    return $required_fields;
}

//Redirect my-account to my-account/edit-account
function customRedirectDashboard() {
    if(function_exists('WC') && is_account_page() && is_user_logged_in() && !is_wc_endpoint_url()) {
        wp_redirect(wc_get_account_endpoint_url('edit-account'));
        exit;
    }
}
add_action('wp', 'customRedirectDashboard');

add_action('woocommerce_save_account_details', 'saveCustomFields', 12, 1);
function saveCustomFields($userId) {
    $dateOfBirth = $_POST['dateOfBirth'] ?? '';
    $height = $_POST['height'] ?? '';
    $weight = $_POST['weight'] ?? '';
    $size = $_POST['size'] ?? '';
    $instagram = $_POST['instagram'] ?? '';

    update_user_meta($userId, 'dateOfBirth', $dateOfBirth);
    update_user_meta($userId, 'height', $height);
    update_user_meta($userId, 'weight', $weight);
    update_user_meta($userId, 'size', $size);
    update_user_meta($userId, 'instagram', $instagram);
}

add_filter( 'woocommerce_my_account_my_orders_query', 'setLimitForMyAccountOrders', 10, 1 );
function setLimitForMyAccountOrders($args) {
    // Set the post per page
    $args['limit'] = 3;
    return $args;
}

add_action('wp_ajax_addReview', 'addReview' ); // executed when logged in
add_action('wp_ajax_nopriv_addReview', 'addReview' ); // executed when logged out

require_once(ABSPATH . 'wp-admin/includes/image.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/media.php');
function addReview() {
    $productId = $_POST['productId'] ?? null;
    $userId = get_current_user_id();
    $user = get_user_by('id', $userId);
    $name = null;
    $email = null;
    $height = null;
    $weight = null;
    $size = null;
    if($productId && $userId) {
        $product = wc_get_product($productId);
        if($product instanceof WC_Product_Variation) {
            $variationId = (int)$productId;
            $productId = $product->get_parent_id();
        }
    $email = $user->get('user_email');
    $name = $user->get('first_name') . ' ' . $user->get('last_name');
    $height = $user->get('height');
    $weight = $user->get('weight');
    $size = $user->get('size');
    }
    $description = $_POST['description'] ?? null;
    $imageData = $_FILES['imageData'] ?? null;
    $wornTo = $_POST['wornTo'];
    $bodyType = $_POST['bodyType'];
    $age = $_POST['age'];
    $title = $_POST['title'];
    $attachmentId = null;
    $attachmentUrl = null;
    if($imageData) {
          $attachmentId = media_handle_upload('imageData',$productId);
          $attachmentUrlFull = wp_get_attachment_image_src($attachmentId, 'full')[0];
          $attachmentUrlThumbnail = wp_get_attachment_image_src($attachmentId, 'medium')[0];
    }

    if(!$productId || !$description || !$email || !$name || !$user) {
        wp_send_json(json_encode(['success' => false, 'message' => __('An error has occurred. Please try again.','gf-theme')]));
    }

      $data = [
        'comment_post_ID' => $productId,
        'comment_author' => $name,
        'comment_author_email' => $email,
        'comment_author_url' => '',
        'comment_content' => $description,
        'comment_type' => '',
        'comment_parent' => 0,
        'user_id' => get_current_user_id(),
        'comment_approved' => 0,
    ];
    $commentId = wp_insert_comment($data);  //returns the commentId
    if($commentId) {
        update_comment_meta($commentId, 'rating', (int)$_POST['rating']);
        update_comment_meta($commentId, 'user_height', $height);
        update_comment_meta($commentId, 'user_weight', $weight);
        update_comment_meta($commentId, 'user_size', $size);
        update_comment_meta($commentId, 'user_image_url', $attachmentUrlFull);
        update_comment_meta($commentId, 'user_image_url_thumbnail', $attachmentUrlThumbnail);
        update_comment_meta($commentId, 'variation_id', $variationId ?? null);
        update_comment_meta($commentId, 'worn_to', $wornTo);
        update_comment_meta($commentId, 'age', $age);
        update_comment_meta($commentId, 'body_type', $bodyType);
        update_comment_meta($commentId, 'title', $title);
         wp_send_json(json_encode(['success' => true, 'message' => __('Recenzija je poslana i čeka odobrenje.','gf-theme')]));
    } else {
        wp_send_json(json_encode(['success' => false, 'message' => __('An error has occurred. Please try again.','gf-theme')]));
    }
}
add_action('template_redirect', 'skipCart');
function skipCart(){
    // Redirect to check out (when cart is not empty)
    if (is_cart() && ! WC()->cart->is_empty()) {
        wp_safe_redirect(wc_get_checkout_url());
        exit();
    }
    // Redirect to shop if cart is empty
    if ( is_cart() && WC()->cart->is_empty() ) {
        wp_safe_redirect(wc_get_page_permalink('shop'));
        exit();
    }
}

add_shortcode('gf_reviews', static function (){
    /** @var WP_Comment[] $comments */
$comments = get_comments(['post_type' => 'product', 'number' => 3, 'status' => 'approve']);
    if (count($comments) === 0) {
        return ;
    }?>
    <div class="container container--main">
            <div class="hp--title"><?=__('#rent4style girls', 'gf-theme')?></div>
            <div class="hp--girls">
            <?php foreach ($comments as $comment):?>
                <div>
                    <img src="<?=get_comment_meta($comment->comment_ID, 'user_image_url', true)?>" alt="<?=__('review image', 'gf-theme')?>" />
                    <p><svg class="icon icon--large">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#single-quote" />
                        </svg><?=$comment->comment_content?></p>
                </div>
            <?php endforeach;?>
            </div>
        </div>
        <?php
});

add_action('wp_ajax_getProductReviewInfo', 'getProductReviewInfo' ); // executed when logged in
add_action('wp_ajax_nopriv_getProductReviewInfo', 'getProductReviewInfo' ); // executed when logged out
function getProductReviewInfo($productId, $number = 1, $offset = 0, $ajax = false, $sort = false, $filters = false) {
    if(isset($_POST['ajax']) && $_POST['ajax'] === '1') {
        $ajax = true;
    }
    if($ajax) {
        $productId = $_POST['productId'];
        $offset = (int)$_POST['offset'];
    }

     $args = [
    'number'      => $number,
    'status'      => 'approve',
    'post_status' => 'publish',
    'post_type'   => 'product',
    'post_id' => $productId,
    'offset' => $offset,
    ];
    $data = [];
    $comments = get_comments($args);

    if(count($comments) === 0) {
        if($ajax) {
            wp_send_json(json_encode(['message' => __('Nema više recenzija','gf-theme')]));
        } else {
            return;
        }
    }
    foreach($comments as $comment) {
        $commentMeta = get_comment_meta($comment->comment_ID);
        $commentObject = new stdClass();
        $commentObject->author = $comment->comment_author ?? '';
        $commentObject->authorWeight = $commentMeta['user_weight'][0] ?? '';
        $commentObject->authorHeight = $commentMeta['user_height'][0] ?? '';
        $commentObject->authorSize = $commentMeta['user_size'][0] ?? '';
        $commentObject->date = $comment->comment_date ?? '';
        $commentObject->content = $comment->comment_content ?? '';
        $commentObject->image = $commentMeta['user_image_url'][0] ?? '';
        $commentObject->thumbnail = $commentMeta['user_image_url_thumbnail'][0] ?? '';
        $commentObject->ratingHtml = getRatingHtml($commentMeta['rating'][0]);
        $commentObject->rating = $commentMeta['rating'][0];
        $commentObject->age = $commentMeta['age'][0];
        $commentObject->wornTo = $commentMeta['worn_to'][0];
        $commentObject->bodyType = $commentMeta['body_type'][0];
        $commentObject->title = $commentMeta['title'][0];
        $data[] = $commentObject;
    }
    if($ajax) {
      wp_send_json(json_encode($data));
    }

    return $data;
}

function getRatingHtml($rating) {
    $rating = (float)$rating;
    $halfStar = false;
    $hs = 0;
    if (fmod($rating, 1) !== 0.00) {
       $halfStar = true;
       $hs = 1; // used in counter for stars
       $rating = floor($rating);
    }
    $html = '';
    for ($i= 1; $i <= $rating; $i++) {
            $html .= '<svg class="icon star-solid">';
            $html .= '<use href="' . THEME_URI . '/assets/images/sprite.svg#star-solid' . '" />';
            $html .= '</svg>';
    }
    if ($halfStar) {
     $html .= '<svg class="icon star-half">';
     $html .= '<use href="' . THEME_URI . '/assets/images/sprite.svg#star-half-stroke' . '" />';
     $html .= '</svg>';
    }
    if ($rating < 5) {
        for ($i = $rating + 1 + $hs; $i <= 5; $i++) {
        $html .= '<svg class="icon star">';
        $html .= '<use href="' . THEME_URI . '/assets/images/sprite.svg#star' . '" />';
        $html .= '</svg>';
        }
    }
    return $html;
}

add_action( 'woocommerce_checkout_order_processed', 'addDatesMetaToOrder',1,1);
function addDatesMetaToOrder($orderId) {
        $dates = WC()->session->get('orderDates');
        $order = wc_get_order($orderId);
        if($dates && $order) {
            $order->update_meta_data('orderDates',serialize($dates));
            $order->save();
           global $gfContainer;
            try {
                $bookingController = $gfContainer->get(\PluginContainer\Packages\R4sBooking\Controller\R4sBooking::class);
                if($bookingController) {
                    $bookingController->creatBookingEntryForOrderProducts($orderId,serialize($dates));
                }
            } catch(Exception $e) {
                $logger = $gfContainer->get(\PluginContainer\Core\Logger\Logger::class);
                $logger->error($e->getMessage(),['orderId' => $orderId]);
                 wp_redirect(wc_get_checkout_url());
                exit();
            }
            WC()->session->set('orderDates',null);
        } else {
            wp_redirect(wc_get_checkout_url() . '?dateError=true');
            exit();
        }
}
add_image_size( 'thumbnail', 150, 150, false);
add_image_size('singleRent', 550, 825, false);

add_action('wp_ajax_itemFit', static function () {
    $orderId = $_POST['orderId'];
    $note = $_POST['orderNote'];
    if ($orderId === '') {
        echo json_encode(['success' => false]);
        die();
    }
    $order = wc_get_order($orderId);
    $order->update_status('wc-item-does-not-fit', $note);
    $order->save();
    wp_mail('info@rent4style.com','Item doesn\'t fit', 'Item from order #'. $orderId . ' does not fit');
    echo json_encode(['success' => true]);
    die();
});


add_action('wp_ajax_addUserToNotifyList', 'addUserToNotifyList' ); // executed when logged in
add_action('wp_ajax_nopriv_addUserToNotifyList', 'addUserToNotifyList' ); // executed when logged out

function addUserToNotifyList() {
    $productId = null;
    $userId = null;
    try {
        global $gfContainer;
        /**
        * @var \PluginContainer\Packages\R4sBooking\Controller\R4sBooking $booking
        */
        $booking = $gfContainer->get(\PluginContainer\Packages\R4sBooking\Controller\R4sBooking::class);
        if(isset($_POST['dates']) && isset($_POST['productId'])) {
             $dates = json_decode(stripslashes($_POST['dates']));
             $productId = json_decode($_POST['productId']);
             $userId = get_current_user_id();
             if($booking->checkIfUserIsAlreadyInNotifyListForProductForDate($productId, $userId, serialize($dates))[0]['count'] > 0) {
                  wp_send_json(json_encode(['success' => false,'message' => 'Već ste na listi čekanja za ovu haljinu.']));
             } else {
                 $booking->createNotifyIfAvailableEntry($dates, $userId, $productId);
                 wp_send_json(json_encode(['success' => true,'message' => 'Na tvoju mail adresu stiže obavijest ako se proizvod oslobodi.']));
             }
        }
    } catch(Exception $e) {
        $logger = $gfContainer->get(\PluginContainer\Core\Logger\Logger::class);
        $logger->error('Error in adding user to the notify me list', ['error' => $e->getMessage(),'trace' => $e->getTraceAsString(), 'productId' => $productId, 'userId' => $userId]);
         wp_send_json(json_encode(['success' => false,'message' => __('A problem occurred while processing this request, please try again.','gf-theme')]));
    }
}

function translateStatus($status) { // @todo remove when loco is fixed, not translating atm
    switch($status) {
        case 'Cancelled':
            return 'Otkazana narudžba';
        case 'Delivered':
            return 'Isporučena';
        case 'Item does not fit':
            return 'Veličina ne odgovara';
        case 'Sent':
            return 'Poslat';
        default:
            return $status;
    }
}


function getHexForColorName($color) {
    switch($color) {
        case 'cream':
            return '#FFFDD0';
        case 'flowers':
            return '#B5E6BE';
        case 'light-blue':
            return '#add8e6';
        case 'light-green':
            return '#90EE90';
        case 'light-pink':
            return '#ffb6c1';
        case 'light-purple':
            return '#CBC3E3';
        case 'pink-and-orange':
            return 'linear-gradient(160deg, rgba(255,5,237,1) 0%, rgba(255,5,237,1) 0%, rgba(255,157,71,1) 58%);border:1px solid pink';
        default:
            return $color;
    }
}
add_action('wp_ajax_getCommentsWithFilters', 'getCommentsWithFilters' ); // executed when logged in
add_action('wp_ajax_nopriv_getCommentsWithFilters', 'getCommentsWithFilters' ); // executed when logged out
function getCommentsWithFilters() {
    $data = [];
    if($_POST['productId']) {
        $productId = $_POST['productId'];
    }
    if($_POST['offset']) {
        $offset = $_POST['offset'];
    }
    if($_POST['weight']) {
        $weight = $_POST['weight'];
    }
    if($_POST['height']) {
        $height = $_POST['height'];
    }
    if($_POST['size']) {
        $size = $_POST['size'];
    }
    $perPage = 1;

    $args = [
        'post_id' => $productId,
        'post_status' => 'publish',
        'number' => $perPage,
        'offset' => $offset,

        'meta_query' => [
                'relation' => 'AND'
        ]
    ];
     if($_POST['sort'] && $_POST['sort'] === 'popular') {
        $args['meta_key'] = 'rating';
        $args['orderby'] = ['meta_value_num' => 'DESC'];
    }
    if($weight && $weight !== '-1') {
        $args['meta_query'][] = [
             [
                    'key' => 'user_weight',
                    'value' => $weight,
                    'compare' => '<=',
                    'type' => 'NUMERIC'
            ],
                       [
                'key' => 'user_weight',
                'value' => '/',
                'compare' => '!='
            ],
        ];
    }
    if($height && $height !== '-1') {
        $args['meta_query'][] = [
             [
                    'key' => 'user_height',
                    'value' => $height,
                    'compare' => '<=',
                    'type' => 'NUMERIC'
            ],
                       [
                'key' => 'user_height',
                'value' => '/',
                'compare' => '!='
            ],
        ];
    }
    if($size && $size !== '-1') {
         $args['meta_query'][] = [
             [
                    'key' => 'user_size',
                    'value' => $size,
                    'compare' => '='
            ],
                       [
                'key' => 'user_size',
                'value' => '/',
                'compare' => '!='
            ],
        ];
    }
    $filterQuery = new WP_Comment_Query($args);
    $comments = $filterQuery->comments;
    if(count($comments) > 0 ) {
        $comment = $comments[0];
        $commentMeta = get_comment_meta($comment->comment_ID);
        $commentObject = new stdClass();
        $commentObject->author = $comment->comment_author ?? '';
        $commentObject->authorWeight = $commentMeta['user_weight'][0] ?? '';
        $commentObject->authorHeight = $commentMeta['user_height'][0] ?? '';
        $commentObject->authorSize = $commentMeta['user_size'][0] ?? '';
        $commentObject->date = $comment->comment_date ?? '';
        $commentObject->content = $comment->comment_content ?? '';
        $commentObject->image = $commentMeta['user_image_url'][0] ?? '';
        $commentObject->thumbnail = $commentMeta['user_image_url_thumbnail'][0] ?? '';
        $commentObject->ratingHtml = getRatingHtml($commentMeta['rating'][0]);
        $commentObject->rating = $commentMeta['rating'][0];
        $commentObject->age = $commentMeta['age'][0];
        $commentObject->wornTo = $commentMeta['worn_to'][0];
        $commentObject->bodyType = $commentMeta['body_type'][0];
        $commentObject->title = $commentMeta['title'][0];
        $data[] = $commentObject;
        wp_send_json(json_encode($data));
    }
   wp_send_json(json_encode(['message' => __('Nema više recenzija','gf-theme')]));
}


add_action('add_meta_boxes', function ()
        {
         add_meta_box(
            'userInstagramBox',
            'User Instagram',
            'renderUserInstagramMetaBox',
            'shop_order',
            'side',
            'high'
        );
    });

function renderUserInstagramMetaBox() {
    global $post;
    $userId = wc_get_order($post->ID)->get_user_id() ?? null;
    if($userId) {
        echo get_user_meta($userId,'instagram',true) ?? '';
    }
}

add_filter( 'woocommerce_email_order_meta_fields', 'custom_woocommerce_email_order_meta_fields', 10, 3 );

function custom_woocommerce_email_order_meta_fields( $fields, $sent_to_admin, $order ) {
    $metaFieldValue = get_post_meta($order->get_id(), 'orderDates', true) ?? '';
        if($metaFieldValue !== '') {
        $prettyDates = Service::formatOrderDatesForInputView(unserialize($metaFieldValue,['allowed_classes' => true])) ?? '';
        $fields['rent_dates'] = [
            'label' => __( 'Datum rezervacije' ),
            'value' => $prettyDates,
        ];
    }
    return $fields;
}


add_action("woocommerce_order_status_changed", "sentStatusMail");

function sentStatusMail($order_id, $checkout = null) {
    global $woocommerce;
   $order = new WC_Order( $order_id );
   if($order->status === 'sent' ) {
       $userName = $order->get_billing_first_name();
        ob_start();
        include THEME_DIR . '/woocommerce/emails/custom-order-sent.php';
        $message = ob_get_clean();
        wp_mail($order->get_billing_email(), __('Rent4Style proizvod je krenuo prema tebi','plugin-container'), $message, 'Content-type: text/html');
   }
}

function wp_maintenance_mode() {
    if(get_current_user_id() === 9) {
        return;
    }
    if (!current_user_can('edit_themes') || !is_user_logged_in()) {
        echo '<h1 style="text-align:center;">USKORO</h1><br /><p style="text-align:center; font-size:22px;">14.9.2022.</p>';
        exit();
    }
}
//add_action('get_header', 'wp_maintenance_mode');

function myCronSchedule($schedules){
    if(!isset($schedules['1min'])){
        $schedules['1min'] = array(
            'interval' => 60,
            'display' => __('Once every 1 minute'));
    }
    return $schedules;
}
add_filter('cron_schedules','myCronSchedule');

add_action( 'init', 'scheduleCancelPendingOrders');
add_action( 'gf_cancel_pending_orders', 'cancelPendingOrders' );
function scheduleCancelPendingOrders() {
    if ( ! wp_next_scheduled ( 'gf_cancel_pending_orders' ) ) {
        wp_schedule_event( time(), '1min', 'gf_cancel_pending_orders' );
    }
}

function cancelPendingOrders() {
    $newTime = strtotime('-30 minutes');
    $tenMinutesBeforeNow = date('Y-m-d H:i:s', $newTime);
    $orders = wc_get_orders(['status' => 'pending', 'limit' => -1, 'date_created'	=>  '<' . strtotime(get_gmt_from_date($tenMinutesBeforeNow))]);
    foreach($orders as $order) {
        $order->update_status('cancelled', 'Status changed to cancelled after 30 minutes of pending payment');
    }
}

add_action('init', function() {
   if (defined('WP_CLI') && WP_CLI) {
        ini_set('max_execution_time', 1200);
        ini_set('display_errors', 1);
        \WP_CLI::add_command('changeProductPrices', 'changeProductPrices');
    }
});


function changeProductPrices()
{
    update_option('woocommerce_currency', 'EUR', true);
    //get all variations
    $args = [
        'post_type' => 'product_variation',
        'posts_per_page' => -1,
        'post_status' => 'publish',
    ];
    $variations = get_posts($args);
    //loop through variations and update price
    foreach ($variations as $variation) {
        $product = wc_get_product($variation->ID);
        $price = (int)$product->get_price();
        $regularPrice = (int)$product->get_regular_price();
        $salePrice = (int)$product->get_sale_price();
        $price = (int)number_format($price / 7.5345,2);
        $regularPrice = number_format($regularPrice / 7.5345,2);
         if($salePrice) {
             $sale_price = number_format($salePrice / 7.5345,2);
              $product->set_sale_price($sale_price);
         }
         $product->set_price($price);
         $product->set_regular_price($regularPrice);
         $product->save();
    }
    wp_mail(['petariv9595@gmail.com', 'v.jankovic992@gmail.com','djavolak@mail.ru']
            ,'Rent4Style prices updated',
            'Srecna nova godina, happy debugging');

}

function addGtagToHead() { ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MQSPTBT9');</script>
    <!-- End Google Tag Manager -->
<?php }
add_action('wp_head', 'addGtagToHead');


function addGtagToBody() { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQSPTBT9"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php }
add_action('wp_body_open', 'addGtagToBody');
