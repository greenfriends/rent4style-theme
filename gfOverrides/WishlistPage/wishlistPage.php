<?php
use PluginContainer\Packages\Wishlist\Controller\Wishlist;

if(!is_user_logged_in()):?>
    <h2 class="wishlistWarningTitle"><?=__(
            sprintf('Morate biti ulogirani da biste ušli u svoju listu želja. Prijavite se <a style="text-decoration: underline" href="%s">OVDJE</a>.',
                    get_permalink( get_option('woocommerce_myaccount_page_id'))), 'plugin-container')?></h2>
<?php else:
global $gfContainer;
$wishlistController = $gfContainer->get(Wishlist::class);
$wishlistProducts = $wishlistController->getWishlistProducts();
$countProducts = count($wishlistProducts);
?>
<main class="favorites">
    <div class="page--title">
        <h1><?=__('Tvoji omiljeni proizvodi','gf-theme')?>
            <svg class="icon heart-full">
                <use href="<?=THEME_URI . '/assets/images/'?>/sprite.svg#heart-full" />
            </svg>
        </h1>
    </div>
    <div class="container container--main container--space">
        <div class="columns">
            <div class="column--right">
                <div class="products">
                    <?php
                    foreach($wishlistProducts as $id) {
                        $product = wc_get_product($id);
                        if($product) {
                            $thumbnail = wp_get_attachment_image_src(
                                get_post_thumbnail_id($product->get_id()),
                                'large'
                            );
                            $title = $product->get_title();
                            $price = $product->get_price_html();
                            $url = $product->get_permalink();
                            $rating = wc_get_rating_html($product->get_average_rating());
                            $addToCartUrl = "?add-to-cart={$product->get_id()}";
                            include THEME_DIR . '/gfOverrides/WishlistPage/templates/wishlistProductItem.php';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="to-top">
        <span><?=__('Top','gf-theme')?></span>
        <svg class="icon">
            <use href="<?= THEME_URI . '/assets/images/'?>sprite.svg#angle-up" />
        </svg>
    </div>
    <div class="favorites--hearts"></div>
</main>
<?php endif;?>