<div class="product">
    <figure class="product--image">
        <a href="<?=$url?>" title="<?=$title?>">
            <img src="<?=$thumbnail[0]?>" alt="<?=$title?>" />
        </a>
    </figure>
    <div class="deleteFromWishlist" data-id="<?=$product->get_id()?>" data-method="deleteFromWishlist"
         title="<?=__('Remove from wishlist', 'plugin-container')?>">
            <span class="product--like liked">
                    <svg class="icon heart-full">
                        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#heart-full" />
                    </svg>
            </span>
    </div>
    <div class="product--info">
        <h2 class="product--made"><a href="<?=$product->get_meta('productMadeByUrl')?>"><?=$product->get_meta('productMadeTitle')?></a></h2>
        <span class="product--name"><?=$title?></span>
        <?php
        global $gfContainer;
        /**
         * @var \PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion $currencyConversion
         */
        $currencyConversion = $gfContainer->get(\PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion::class);
        $rate = $currencyConversion->getExchangeRate();
        ?>
        <span class="product--price"><?=$product->get_price_html()?> <?=$rate && get_woocommerce_currency() === 'HRK' ? '<span class="ratePrice"> &euro; ' . number_format($product->get_price() / $rate,2) . '</span>' : ''?></span>
    </div>
    <div class="product--button">
        <a href="<?=$url?>"><?=__('Dodaj u košaricu','gf-theme')?></a>
    </div>
</div>