<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <title><?=get_bloginfo('description')?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php if(is_single()):?>
    <?php
        $productId = get_queried_object_id();
        $product = wc_get_product($productId);
        $productImage = null;
        $productUrl = null;

        if($product) {
            $productImage = wp_get_attachment_image_src($product->get_image_id(), 'full');
            $productUrl = $product->get_permalink();
            $productTitle = $product->get_title();
            $productDescription = $product->get_short_description();
        }
    ?>
    <?php if($productImage && $productUrl && $productTitle && $productDescription):?>
            <meta property="og:image" content="<?=$productImage[0]?>" />
            <meta property="og:title" content="<?=$productTitle?>" />
            <meta property="og:url" content="<?=$productUrl?>" />
            <meta property="og:description" content="<?=$productDescription?>" />
            <meta property="og:type" content="product">
    <?php endif;?>
    <?php endif;?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<?php include(THEME_DIR . '/templates/header/header.php') ?>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '642163890869666');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=642163890869666&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<main class="<?= is_user_logged_in() ? 'loggedIn' : 'loggedOut'?>">




