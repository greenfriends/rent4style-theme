<?php
/**
 * Template Name: Homepage
 */
?>
<?php get_header() ?>
<?php $wishlistForUser = json_encode([]) ?>
<?php if(is_user_logged_in()):?>
    <?php
    global $gfContainer;
    $wishlist = $gfContainer->get(\PluginContainer\Packages\Wishlist\Controller\Wishlist::class);
    $wishlistForUser = json_encode($wishlist->getWishlistProducts());
    ?>
<?php endif;?>
<main class="homepage">
        <?= do_shortcode('[gf_banner id="62bdd1ff7e394"]'); ?>
        <div class="hp--line"></div>
        <div class="container container--main">
            <?php dynamic_sidebar('belowHomepageBanner') ?>
        </div>
        <div class="hp--line"></div>
        <?php dynamic_sidebar('homepageSlider_1')?>
        <div class="hp--line"></div>
        <div class="container container--main">
            <div class="featured featured--center">
                <div class="featured--img">
                    <picture>
                        <source srcset="<?=get_home_url()?>/wp-content/uploads/2022/06/Naslovnica_4-683x1024.jpg" media="(max-width: 480px)" />
                        <img src="<?=get_home_url()?>/wp-content/uploads/2022/06/Naslovnica-2-scaled.jpg" alt="Haljine za leto" />
                    </picture>
                </div>
                <section class="featured--content">
                    <h1><?=__('IDEALNE HALJINE ZA LJETO', 'r4s')?></h1>
                    <a href="<?=get_permalink(wc_get_page_id( 'shop' ))?>"><?=__('Sejvaj sada, unajmi kasnije', 'r4s')?></a>
                </section>
            </div>
        </div>
        <div class="hp--line"></div>
        <?php dynamic_sidebar('homepageSlider_2')?>
        <div class="hp--line"></div>
<!--        --><?php //dynamic_sidebar('homepageShowCase_1')?>
        <!--  Showcase Start  -->
        <?php dynamic_sidebar('homepageShowCase_1')?>
        <!--  Showcase End  -->
        <div class="hp--line"></div>
        <div class="container container--main">
            <section class="about--me">
                <div class="about--me-img">
                    <div>
                        <img src="wp-content/uploads/2022/04/m11.jpeg" alt="" >
                        <img src="/wp-content/uploads/2022/04/m12.jpeg" alt="" >
                        <img src="/wp-content/uploads/2022/03/m13.jpg" alt="" >
                    </div>
                    <a href="https://www.instagram.com/matea.miljan/" target="_blank">
                        <svg class="icon">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#instagram" />
                        </svg>
                    Follow me
                    </a>
                </div>
                <div class="about--me-info">
                    <h1><span>Bok,</span> ja sam Matea</h1>
                    <p>Kreativna i prije svega uporna osoba koja je oživjela svoj mali san, Rent4Style. Davno novinarka,
                        danas možda više i ne tako mlada &#128539 poduzetnica.
                    <p>Uz agenciju za promidžbu na svoj 33. rođendan
                        pokrenula sam Rent4Style, platformu za najam odjeće.</p>
                    <a href="<?=get_home_url() . '/about'?>">
                        <?=__('Saznaj više', 'r4s')?>
                    <svg class="icon">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#arrow-right-long" />
                        </svg>
                    </a>
                </div>
            </section>
        </div>
     <?php dynamic_sidebar('homepageReviews')?>


        <div class="container container--main">
            <div class="hp--title">Rent4style Instagram</div>
            <p class="hp--desc">
                Ako nas pratiš na Instagramu ekskluzivno ćeš među prvima doznati što je novo u ponudi, kako nešto
                stoji, kako se nosi, kako izgleda na modelu i kada će biti dostupno u našem webshopu.
                <a href="https://www.instagram.com/rent4style/" target="_blank">
                    <svg class="icon">
                        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#instagram"></use>
                    </svg>
                    Zaprati Rent4Style
                </a>
            </p>
            <?php dynamic_sidebar('belowHomepageReviews')?>
        </div>
    </main>
    <script>
        let wishlistForUser = JSON.parse('<?=$wishlistForUser?>');
        let size = Object.keys(wishlistForUser).length;
        if(size > 0) {
            let addToWishlistItems = document.querySelectorAll('.addToWishlist');
            addToWishlistItems.forEach((item) => {
                let dataId = item.getAttribute('data-id');
                if (Object.values(wishlistForUser).includes(dataId)) {
                    item.classList.add('delete');
                    item.innerHTML = `<svg viewBox="0 0 512 512" id="heart-full" fill="#E52C42"
            xmlns="http://www.w3.org/2000/svg"><path d="M472.1 270.5L279 470.2c-12.64 13.07-33.27 13.08-45.91.01l-193.2-199.7C-16.21 212.5-13.1 116.7 49.04 62.86 103.3 15.88 186.4 24.42 236.3 75.98L256 96.25l19.7-20.27c49.95-51.56 132.1-60.1 187.3-13.12 62.1 53.74 65.2 149.64 9.1 207.64z"/></svg>`;
                }
            });
        }
    </script>
<?php get_footer() ?>