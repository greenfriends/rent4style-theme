<?php
/**
 * Template Name: About
 */
get_header();
?>
    <div class="container container--main container--space">
        <article class="about--page">
            <div class="about--page-left">
                <h1><span>Bok,</span> ja sam Matea</h1>
                <p>Kreativna i prije svega uporna osoba koja je oživjela svoj mali san, Rent4Style. Davno novinarka, danas možda više i ne tako mlada &#128539 poduzetnica. </p>
                <img src="/wp-content/uploads/2022/04/m11.jpeg" alt="<?=__('About Image','r4s')?>" />
            </div>
            <div class="about--page-right">
                <img src="/wp-content/uploads/2022/04/m12.jpeg" alt="<?=__('About Image','r4s')?>" />
                <blockquote>
                        <span>
                            <svg class="icon">
                                <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#quote-left" />
                            </svg>
                        </span>
                    „Nosi ono što te najbolje opisuje“
                </blockquote>
            </div>
            <div class="about--page-center">
                <img src="/wp-content/uploads/2022/03/m13.jpg" alt="<?=__('About Image','r4s')?>" />
                <div>
                    <p>Uz agenciju za promidžbu na svoj 33. rođendan pokrenula sam Rent4Style, platformu za najam odjeće.</p>
                    <p>Influencerica sam s malom vojskom pratitelja koja vjerno već nosi #rent4style haljine. Volim snimati, montirati, raditi, volim prirodu, putovanja i naravno modu. Oduvijek sam imala previše haljina samo za sebe, a sada ih imam i za vas. Uz sebe imam i obitelj i prijatelje i Lu, razmaženu „maltezericu“.</p>
                    <a href="/kontakt">
                        <?= __('STUPI S NAMA U KONTAKT', 'r4s') ?>
                        <svg class="icon">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#arrow-right-long" />
                        </svg>
                    </a>
                </div>
                <div>
                    <a target="_blank" href="https://www.instagram.com/matea.miljan/" title="instagram">
                        <svg class="icon">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#instagram" />
                        </svg>
                        Follow me on Insta
                    </a>
                </div>
            </div>
        </article>
    </div>
    <div class="about--menu">
        <ul>
            <li class="active"><a href="/about" title="about"><?=__('O nama','r4s')?></a></li>
            <li><a href="/faqs" title="faq"><?=__('FAQ','r4s')?></a></li>
            <li><a href="/kontakt" title="contact"><?=__('Kontakt','r4s')?></a></li>
        </ul>
    </div>
<?php
get_footer();
