import Purchase from "./v5/Purchase.js";
import Filters from "./v5/Filters.js";
import Reviews from "./v5/Reviews.js";
import SingleReviewList from "./v5/SingleReviewList.js";

let purchase = new Purchase();
purchase.init();

let filters = new Filters();
let form = document.getElementById('filtersForm');
let formFilters = [];
if(form) {
    let data = new FormData(form);
    [...data.entries()].forEach((entry) => {
        if(entry[0] !== 'pa_color') {
            formFilters.push({selector:entry[0]});
        }
    });
    filters.registerCheckboxFilters(formFilters);
    filters.addCheckboxListeners();
// colors are custom so we separate it
    filters.initColorFilters();
// listener for clearing all filter inputs
    filters.addClearFiltersListener();
}


let reviews = new Reviews();
reviews.init();


let singleReview = new SingleReviewList();
if(singleReview.checkIfSingle()) {
    singleReview.init();
}