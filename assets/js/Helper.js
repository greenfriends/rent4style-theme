export default class Helper {

    ajaxUrl = MYOBJECT.ajaxUrl;
    myObject = MYOBJECT;

    startLoader(target, center = false) {
        if(!document.getElementById('loader')) {
            let loader = document.createElement('span');
            loader.id = 'loader';
            loader.classList.add('gfLoader');
            if(center) {
                loader.classList.add('checkoutLoader');
            } else {
                loader.classList.add('singleLoader');
            }
            if (target) {
                target.appendChild(loader);
            }
        }
    }
    removeLoader() {
        if(document.getElementById('loader')) {
            document.getElementById('loader').remove();
        }
    }

    disableButton(button) {
        button.disabled = true;
        button.classList.add('disabled');
    }

    enableButton(button) {
        button.disabled = false;
        button.classList.remove('disabled');
    }

    parseJsonResponse(json) {
        let res = JSON.parse(json);
        let resObj = {};
        if(res.success) {
            resObj.success = res.success;
        }
        if(res.message) {
            resObj.message = res.message;
        }
        if(res.variationId) {
            resObj.variationId = res.variationId;
        }
        if(res.quantity) {
            resObj.quantity = res.quantity;
        }
        if(res.available) {
            resObj.available = res.available;
        }
        if(res.price) {
            resObj.price = res.price;
        }
        return resObj;
    }

     printMessage(successElem,errorElem,action,message) {
        this.removeMessages(successElem,errorElem);
        switch(action) {
            case 'error':
                errorElem.classList.add('show');
                errorElem.innerText = message;
                break;
            case 'success':
                successElem.classList.add('show');
                successElem.innerText = message;
                break;
            default:
                break;
        }
    }

    printNoticeInContainer(container, message, action) {
        let messageElemExisting = document.getElementById('messageElementNotice');
        if(messageElemExisting) {
            messageElemExisting.remove();
        }
        let messageElem = document.createElement('p');
        messageElem.id = 'messageElementNotice';
        messageElem.style.color = 'white';
        messageElem.style.display = 'block';
        messageElem.style.fontSize = '16px';
        messageElem.style.padding = '12px 20px';

        messageElem.innerText = message;
        switch(action) {
            case 'error':
                messageElem.style.background = '#eb5840'
                break;
            case 'success':
                messageElem.style.background = '#78c775'
                break;
            default:
                break;
        }
        container.appendChild(messageElem);
    }

    removeMessages(successElem,errorElem) {
        successElem.classList.remove('show');
        errorElem.classList.remove('show');
    }

    incrementCartContents(cartElement,cartLink) {
        if(!cartElement.classList.contains('filled')) {
            cartElement.classList.add('filled');
            cartElement.innerHTML = '1';
            if(cartLink) {
                cartLink.setAttribute('href',MYOBJECT.checkoutUrl)
            }
            return;
        }
        let innerHtmlCart = parseInt(cartElement.innerHTML,10);
        cartElement.innerHTML = innerHtmlCart + 1;
    }
}