import Helper from "./Helper.js";

export default class SingleReviewList extends Helper {
    checkIfSingle() {
        return document.querySelector('.single-product') ?? false;
    }
    init() {
        this.customerReviewThumbs = document.querySelector('.details--thumbs-customer');
        this.initialCustomerReviewThumb = document.querySelector('.reviewData');
        this.initialRatingElement = document.querySelector('.initialRatingHtml');
        if(this.initialRatingElement) {
            this.initialRatingHtml = this.initialRatingElement.innerHTML;
        }
        this.nextReview = document.querySelector('.nextReview');
        this.previousReview = document.querySelector('.previousReview');
        this.currentUserThumbIndex = 0;
        this.nextReviewAllow = true;
        this.loadMoreReviewsAction = 'getProductReviewInfo';

        this.sort = document.getElementById('sortReviews');
        this.weightFilter = document.getElementById('reviewsWeight');
        this.sizeFilter = document.getElementById('reviewsSize');
        this.heightFilter = document.getElementById('reviewsHeight');
        this.submitFilters = document.getElementById('submitFilters');

        this.filterAndSortListeners();

        this.loadMoreButton = document.getElementById('loadMoreReviews') ?? null;
        if(this.loadMoreButton) {
            this.loadMoreButtonInitalText = this.loadMoreButton.innerText;
            this.container = this.loadMoreButton.parentElement.parentElement.querySelector('.container');
            this.reviewImages = document.querySelectorAll('.reviews--item-image');
            this.offset = this.loadMoreButton.getAttribute('data-offset');
            this.fetch = true;
            this.overlay = document.querySelector('.overlay');
            this.html = document.querySelector("html");
            this.detailsCustomerPhotos = document.querySelector('.details--customer');
            this.detailsCustomerImage = this.detailsCustomerPhotos.querySelector('.details--customer-image img');
            this.detailsCustomerImagesContainer = this.detailsCustomerPhotos.querySelector('.customerImagesContainer');
            this.detailsCustomerHeight = this.detailsCustomerPhotos.querySelector('.height');
            this.detailsCustomerWeight = this.detailsCustomerPhotos.querySelector('.weight');
            this.detailsCustomerTitle = this.detailsCustomerPhotos.querySelector('.title');
            this.detailsCustomerSize = this.detailsCustomerPhotos.querySelector('.size');
            this.detailsCustomerContent = this.detailsCustomerPhotos.querySelector('.content');
            // this.detailsCustomerBodyType = this.detailsCustomerPhotos.querySelector('.bodyType');
            // this.detailsCustomerAge = this.detailsCustomerPhotos.querySelector('.age');
            // this.detailsCustomerWornTo = this.detailsCustomerPhotos.querySelector('.wornTo');
            this.detailsCustomerClose = document.querySelector('.details--customer-close');
            this.detailsCustomerRating = this.detailsCustomerPhotos.querySelector('.details--customer-ratings');
            this.addLoadMoreListener();
            this.openFullReviewListener();
            this.closeCustomerPhotosListener();
            this.closeCustomerPhotosOnEscapeListener();
        }
        if(this.customerReviewThumbs && this.initialCustomerReviewThumb) {
            this.openCustomerPhotosListener();
            this.nextCustomerReviewThumbListener();
            this.previousCustomerReviewThumbListener();
        }
    }


    filterAndSortListeners() {
        if(this.submitFilters) {
            this.submitFilters.addEventListener('click', () => {
                this.loadMoreReviewsAction = 'getCommentsWithFilters';
                this.fetch = true;
                this.loadMoreButton.innerText = this.loadMoreButtonInitalText;
                this.removeExistingReviews();
                this.getMoreReviews(0,true,true).then((res) => {
                    if(res) {
                        if (res.message) {
                            this.printNoMore(res.message);
                            this.removeLoader();
                            return;
                        }
                        if (res.length > 0) {
                            this.printReviews(res);
                        }
                        this.removeLoader();
                    }
                });
            });
        }
    }


    removeExistingReviews() {
        this.offset = 0;
        document.querySelectorAll('.reviews--item').forEach((reviewItem) => {
            reviewItem.remove();
        });
    }


    openCustomerPhotosListener() {
        this.customerReviewThumbs.addEventListener('click', () => {
            this.currentUserThumbIndex = 0;
            this.openCustomerPhotos();
        });
    }

    openCustomerPhotos() {
        this.openFullReview(this.initialCustomerReviewThumb,true, this.initialRatingHtml);
    }

    nextCustomerReviewThumbListener() {
        this.nextReview.addEventListener('click', async () => {
            let pseudoNextElement = this.customerReviewThumbs.querySelector(`[data-index="${this.currentUserThumbIndex+1}"]`);
            if(pseudoNextElement) {
                this.populateDetailsCustomerPhoto(pseudoNextElement, pseudoNextElement.querySelector('div').innerHTML);
                this.currentUserThumbIndex++;
                return;
            }
            this.disableNavigationAndAddLoader();
            if(this.nextReviewAllow) {
                let response = await fetch(this.ajaxUrl, {
                    method: 'POST',
                    body: this.getFormData(++this.currentUserThumbIndex)
                })
                let res = await response.json();
                let data = JSON.parse(res);
                if(data.message) {
                    this.nextReviewAllow = false;
                    this.currentUserThumbIndex--;
                    this.enableNavigationAndRemoveLoader();
                    return;
                }
                let review = this.generateAndAppendCustomerReviewThumbHtml(data[0]);
                this.populateDetailsCustomerPhoto(review, data[0].ratingHtml);
            }
            this.enableNavigationAndRemoveLoader();
        });
    }

    generateAndAppendCustomerReviewThumbHtml(data) {
        let elem = document.createElement('div');
        let ratingHtml = document.createElement('div');
        ratingHtml.innerHTML = data.ratingHtml;
        elem.appendChild(ratingHtml);
        elem.classList.add('hiddenElement');
        elem.classList.add('reviewData');
        elem.setAttribute('data-index', this.currentUserThumbIndex);
        elem.setAttribute('data-image-full',data.image);
        elem.setAttribute('data-height',data.authorHeight);
        elem.setAttribute('data-weight',data.authorWeight);
        elem.setAttribute('data-size',data.authorSize);
        elem.setAttribute('data-title', data.title);
        elem.setAttribute('data-content',data.content);
        elem.setAttribute('data-age', data.age);
        elem.setAttribute('data-worn-to', data.wornTo);
        elem.setAttribute('data-body-type', data.bodyType);
        this.customerReviewThumbs.appendChild(elem);
        return elem;
    }


    disableNavigationAndAddLoader() {
        this.disableNavigation();
        this.detailsCustomerImagesContainer.style.opacity = '0.5';
        this.startLoader(this.detailsCustomerPhotos.querySelector('.details--customer-carousel'),true);
    }

    enableNavigationAndRemoveLoader() {
        this.enableNavigation();
        this.detailsCustomerImagesContainer.style.opacity = '1';
        this.removeLoader();
    }

    previousCustomerReviewThumbListener() {
        this.previousReview.addEventListener('click', () => {
            if(this.currentUserThumbIndex === 0) {
                return;
            }
            let previousElem = this.customerReviewThumbs.querySelector(`[data-index="${this.currentUserThumbIndex-1}"]`);
            if(previousElem) {
                let ratingHtml = '';
                if(this.currentUserThumbIndex === 1) { // if the previous one will be 0, get the initial rating HTML
                    ratingHtml = this.initialRatingHtml;
                } else {
                    ratingHtml = previousElem.querySelector('div').innerHTML;
                }
                this.populateDetailsCustomerPhoto(previousElem, ratingHtml);
                this.currentUserThumbIndex--;
            }
        })
    }

    closeCustomerPhotosListener() {
        this.detailsCustomerClose.addEventListener('click', () => {
            this.closeCustomerPhotos();
        })
    }
    closeCustomerPhotosOnEscapeListener() {
        document.addEventListener('keyup', (e) => {
            if (e.code === 'Escape') {
                e.preventDefault();
                this.closeCustomerPhotos();
            }
        })
    }

    openFullReviewListener() {
        this.reviewImages.forEach((reviewImage) => {
            reviewImage.addEventListener('click', () => {
                console.log('a');
               this.openFullReview(reviewImage);
            });
        })
    }

    closeCustomerPhotos()  {
        this.html.classList.remove('no-scroll');
        setTimeout(() => {
            this.overlay.classList.remove('open');
        }, 200);
        this.detailsCustomerPhotos.classList.remove('open');
    }

    showNavigation() {
        this.nextReview.style.display = 'block';
        this.previousReview.style.display ='block';
    }

    hideNavigation() {
        this.nextReview.style.display = 'none';
        this.previousReview.style.display ='none';
    }

    disableNavigation() {
        this.nextReview.classList.add('disabledPointer');
        this.previousReview.classList.add('disabledPointer');
    }

    enableNavigation() {
        this.nextReview.classList.remove('disabledPointer');
        this.previousReview.classList.remove('disabledPointer');
    }

    openFullReview(reviewImage, navigation = false, ratingHTML = false) {
        let ratingHtml = false;
        if(navigation) {
            this.showNavigation();
        } else {
            this.hideNavigation();
        }
        if(!ratingHTML) {
            ratingHtml = reviewImage.parentElement.querySelector('.details--ratings').innerHTML;
        } else {
            ratingHtml = ratingHTML;
        }
        this.html.classList.add('no-scroll');
        this.overlay.classList.add('open');
        setTimeout(() => {
            this.populateDetailsCustomerPhoto(reviewImage,ratingHtml);
            this.detailsCustomerPhotos.classList.add('open');
        }, 100);
    }

    populateDetailsCustomerPhoto(reviewImage, ratingHtml) {
        this.detailsCustomerImage.src = reviewImage.getAttribute('data-image-full');
        this.detailsCustomerHeight.innerText  = reviewImage.getAttribute('data-height') + ' cm';
        this.detailsCustomerWeight.innerText  = reviewImage.getAttribute('data-weight') + ' kg';
        this.detailsCustomerSize.innerText  = reviewImage.getAttribute('data-size');
        this.detailsCustomerContent.innerText = reviewImage.getAttribute('data-content');
        // this.detailsCustomerWornTo.innerText = reviewImage.getAttribute('data-worn-to');
        // this.detailsCustomerBodyType.innerText = reviewImage.getAttribute('data-body-type');
        // this.detailsCustomerAge.innerText = reviewImage.getAttribute('data-age');
        this.detailsCustomerTitle.innerText = reviewImage.getAttribute('data-title');
        this.detailsCustomerRating.innerHTML = ratingHtml;
    }

    addLoadMoreListener() {
        if(this.loadMoreButton) {
            this.loadMoreButton.addEventListener('click', () => {
                if(this.fetch) {
                    this.startLoader(this.loadMoreButton.parentElement);
                }
               let offset = false;
                let filters = false;
                let sort = false;
                if(this.loadMoreReviewsAction === 'getCommentsWithFilters') {
                     offset = false;
                     filters = true;
                     sort = true;
                }
                this.getMoreReviews(offset, filters, sort).then((res) => {
                    if(res) {
                        if (res.message) {
                            this.printNoMore(res.message);
                            this.removeLoader();
                            return;
                        }
                        if (res.length > 0) {
                            this.printReviews(res);
                        }
                        this.removeLoader();
                    }
                });
            });
        }
    }

    printNoMore(message) {
        this.loadMoreButton.innerText = message;
        this.fetch = false;
    }

    async getMoreReviews(offset, filters, sort) {
        if(this.fetch) {
            let response = await fetch(this.ajaxUrl, {
                method: 'POST',
                body: this.getFormData(offset, filters, sort)
            })
            let res = await response.json();
            return JSON.parse(res);
        }
    }

    getFormData(offset = false, filters = false, sort = false) {
        const data = new FormData();
        data.append('action',this.loadMoreReviewsAction);
        data.append('productId', this.loadMoreButton.getAttribute('data-product-id'));
        if(offset === false) {
            data.append('offset', this.offset);
        } else {
            data.append('offset', offset);
        }
        data.append('ajax','1')
        if(filters) {
            data.append('height', this.heightFilter.value);
            data.append('weight', this.weightFilter.value);
            data.append('size', this.sizeFilter.value);
        }
        if(sort) {
            data.append('sort', this.sort.value);
        }
        return data;
    }

    printReviews(reviewInfo) {
        let template = document.getElementById('reviewTemplate');
        if(template && 'content' in document.createElement('template')) {
            reviewInfo.forEach((review) => {
                let clone = template.content.cloneNode(true);
                this.container.appendChild(this.populateAndGetReview(review, clone));
            });
            this.offset++;
        }
    }

    populateAndGetReview(review,clone) {
        let viewPhoto = clone.querySelector('.reviews--item-image');
        viewPhoto.setAttribute('data-image-full',review.image);
        viewPhoto.setAttribute('data-weight',review.authorWeight);
        viewPhoto.setAttribute('data-height',review.authorHeight);
        viewPhoto.setAttribute('data-size',review.authorSize);
        viewPhoto.setAttribute('data-content',review.content);
        viewPhoto.setAttribute('data-age', review.age);
        viewPhoto.setAttribute('data-worn-to', review.wornTo);
        viewPhoto.setAttribute('data-body-type', review.bodyType);
        viewPhoto.setAttribute('data-title', review.title);
        viewPhoto.addEventListener('click', () => {
            this.openFullReview(viewPhoto);
        })
        clone.querySelector('.reviews--item-name').innerText = review.author;
        clone.querySelector('.reviews--item-headline').innerText = review.title;
        clone.querySelector('.size').innerText = review.authorSize;
        clone.querySelector('.weight').innerText = review.authorWeight + ' kg';
        clone.querySelector('.height').innerText = review.authorHeight + ' cm';

        let ageElem = clone.querySelector('.age');
        if(ageElem) {
            ageElem.innerText = review.age;
        }

        let wornToElem = clone.querySelector('.wornTo');
        if(wornToElem) {
            wornToElem.innerText = review.wornTo;
        }

        let bodyTypeElem = clone.querySelector('.bodyType');
        if(bodyTypeElem) {
            clone.querySelector('.bodyType').innerText = review.bodyType;
        }

        let titleElem = clone.querySelector('.title');
        if(titleElem) {
            titleElem.innerText = review.title;
        }

        clone.querySelector('.reviews--item-text').innerText = review.content;
        clone.querySelector('.reviews--item-name-2').innerText = review.author;
        clone.querySelector('.reviews--item-date').innerText = review.date;
        clone.querySelector('.reviews--item-text-2').innerText = review.content;
        if(review.image !== '') {
            clone.querySelector('.reviews--item-image img').src = review.image;
        } else {
            clone.querySelector('.reviews--item-image img').remove();
        }
        clone.querySelector('.details--ratings').innerHTML = review.ratingHtml;
        return clone;
    }
}