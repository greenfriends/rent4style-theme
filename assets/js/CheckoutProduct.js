import Helper from "./Helper.js";

export default class CheckoutProduct extends Helper {
    constructor(orderContainer, productContainer) {
        super();
        this.deleteAction = 'removeVariationFromCart';
        this.changeQuantityAction = 'changeQuantity';
        this.changeVariationAction = 'changeVariation';
        this.orderContainer = orderContainer;
        this.productContainer = productContainer;
        this.variationId = productContainer.getAttribute('data-variation-id');
        this.quantityInput = productContainer.querySelector('.productQty');
        this.quantityInputValue = this.quantityInput.value;
        this.removeProductPrompt = productContainer.querySelector('.order--remove');
        this.removeProduct = productContainer.querySelector('.order--dialog-button-yes');
        this.errorContainer = productContainer.querySelector('.noticeContainer.noticeError');
        this.successContainer = productContainer.querySelector('.noticeContainer.noticeSuccess');
        this.deleteEvent =  new CustomEvent('productDeleted', {detail: this});
        this.quantityChangeEvent = new CustomEvent('quantityChanged', {detail: this});
        this.priceElement = productContainer.querySelector('.priceContainer');
        this.currencyConversion = document.getElementById('currencyConversion') ?? null;
        this.attributeSelects = productContainer.querySelectorAll('.attributeSelect');
    }
    init() {
        this.addRemoveProductListener();
        this.addChangeQuantityListener();
        this.addVariationChangeListener();
    }

    addVariationChangeListener() {
        this.attributeSelects.forEach((attributeSelect) => {
            attributeSelect.addEventListener('change', async () => {
                this.removeMessages(this.successContainer,this.errorContainer)
                if(this.hasDuplicateVariationSelected(attributeSelect)) {
                    this.quantityInput.setAttribute('readonly', false);
                    this.printMessage(this.successContainer,this.errorContainer,'error',MYOBJECT.alreadyInBagText);
                    this.orderContainer.dispatchEvent(new CustomEvent('variationChanged', {detail: false}));
                    return;
                }
                if(this.isEveryAttributeSelected(this.attributeSelectsSingle)) {
                    this.startLoader(this.productContainer.parentElement,true);
                    let response = await fetch(this.ajaxUrl,{
                        method: 'POST',
                        body: this.getFormDataVariation()
                    })
                    let res = await response.json();
                    let responseParsed = this.parseJsonResponse(res);
                    if(responseParsed.success) {
                        this.orderContainer.dispatchEvent(new CustomEvent('variationChanged', {detail: true}));
                        let secondCurrency = '';
                        if(this.currencyConversion) {
                            secondCurrency = (parseInt(responseParsed.price) / parseFloat(this.currencyConversion.innerHTML)).toFixed(2);
                        }
                        this.priceElement.innerHTML = (responseParsed.price + ' / &euro; ' + secondCurrency);
                        this.quantityInput.removeAttribute('readonly');
                        //
                    } else {
                        this.quantityInput.value = 1;
                        this.quantityInputValue = 1;
                        this.printMessage(this.successContainer,this.errorContainer,'error',responseParsed.message);

                        if(responseParsed.available && responseParsed.available === 'no') {
                            this.quantityInput.setAttribute('readonly', true);
                            this.orderContainer.dispatchEvent(new CustomEvent('variationChanged', {detail: false}));
                        } else {
                            this.orderContainer.dispatchEvent(new CustomEvent('variationChanged', {detail: true}));
                            let secondCurrency = '';
                            if(this.currencyConversion) {
                                secondCurrency = (parseInt(responseParsed.price) / parseFloat(this.currencyConversion.innerHTML)).toFixed(2);
                            }
                            this.priceElement.innerHTML = responseParsed.price + ' / &euro; ' + secondCurrency;
                        }
                    }
                    if(responseParsed.variationId) {
                        this.productContainer.setAttribute('data-variation-id', responseParsed.variationId);
                        this.variationId = responseParsed.variationId;
                    }
                } else {
                    this.orderContainer.dispatchEvent(new CustomEvent('variationChanged', {detail: false}));
                    this.quantityInput.setAttribute('readonly',true);
                }
                this.removeLoader();
            });
        });
    }

    hasDuplicateVariationSelected(select) {
        let hasDuplicate = false;
        let parentContainer = select.parentElement.parentElement;
        let data = [];
        parentContainer.querySelectorAll('select').forEach((selectElement) => {
            data.push({attribute:selectElement.getAttribute('data-attribute'),value:selectElement.value});
        });
        this.orderContainer.querySelectorAll('.attributeWrapper').forEach((wrapper) => {
            if(!hasDuplicate) {
                let innerSelects = wrapper.querySelectorAll('select');
                if (wrapper !== parentContainer && innerSelects.length === data.length) {
                    let matches = 0;
                    for (let i = 0; i < data.length; i++) {
                        if (wrapper.querySelector(`[data-attribute='${data[i].attribute}']`) &&
                            wrapper.querySelector(`[data-attribute='${data[i].attribute}']`).value === data[i].value) {
                            matches++;
                        }
                    }
                    if (matches === data.length) {
                        hasDuplicate = true;
                    }
                }
            }
        });
        return hasDuplicate;
    }

    isEveryAttributeSelected() {
        let isValid = true;
        this.attributeSelects.forEach((select) => {
            if(select.value === '-1') {
                isValid = false;
            }
        })
        return isValid;
    }

    getFormDataVariation() {
        const data = new FormData();
        data.append('action',this.changeVariationAction);
        data.append('postId',this.productContainer.getAttribute('data-post-id'));
        data.append('data',this.getVariationSelected());
        data.append('quantity',this.quantityInput.value);
        data.append('oldVariationId',this.variationId);
        return data;
    }

    getVariationSelected() {
        let obj = [];
        this.attributeSelects.forEach((select) => {
            obj.push({
                attribute: select.getAttribute('data-attribute'),
                value: select.value
            });
        });
        return JSON.stringify(obj);
    }

    addRemoveProductListener() {
        this.removeProductPrompt.addEventListener('click', () => {
            this.productContainer.querySelector('.order--dialog').classList.add('active')
        });
        this.removeProduct.addEventListener('click', async () => {
           let res = await this.productRemove();
        });
    }

    async productRemove() {
        this.removeProduct.parentElement.parentElement.classList.remove('active');
        this.productContainer.classList.add('loading');
        this.startLoader(this.productContainer.parentElement,true);
        let response = await fetch(this.ajaxUrl,{
            method: 'POST',
            body: this.getFormatDataForDeletion()
        })
        let res = await response.json();
        let responseParsed = this.parseJsonResponse(res);
        if(responseParsed.success) {
            this.removeLoader();
            this.productContainer.remove();
            this.orderContainer.dispatchEvent(this.deleteEvent);
            return responseParsed;
        }
        return false;
    }

    async changeProductQuantity() {
        if(this.isEveryAttributeSelected()) {
            this.removeMessages(this.successContainer, this.errorContainer);
            this.productContainer.classList.add('loading');
            this.startLoader(this.productContainer.parentElement, true);
            let response = await fetch(this.ajaxUrl, {
                method: 'POST',
                body: this.getFormatDataForQuantityChange()
            })
            let res = await response.json();
            let responseParsed = this.parseJsonResponse(res);
            this.productContainer.classList.remove('loading');
            this.removeLoader();
            if (responseParsed.success) {
                this.orderContainer.dispatchEvent(this.quantityChangeEvent);
                this.quantityInputValue = responseParsed.quantity;
                return responseParsed;
            } else {
                this.quantityInput.value = this.quantityInputValue;
                if (responseParsed.message) {
                    this.printMessage(this.successContainer, this.errorContainer, 'error', responseParsed.message);
                }
            }
            return false;
        }
    }

    addChangeQuantityListener() {
        let timeout;
        this.quantityInput.addEventListener('input', () => {
            clearTimeout(timeout);
            timeout = setTimeout(async () =>{
                let quantity = parseInt(this.quantityInput.value,10);
                if(quantity <= 0) {
                    let res = await this.productRemove();
                    return;
                }
                let res = await this.changeProductQuantity();
            },650)

        });
    }

    getFormatDataForQuantityChange() {
        const data = new FormData();
        data.append('action',this.changeQuantityAction);
        data.append('variationId',this.variationId);
        data.append('quantity',this.quantityInput.value);
        return data;
    }

    getFormatDataForDeletion() {
        const data = new FormData();
        data.append('action',this.deleteAction);
        data.append('variationId',this.variationId);
        return data;
    }

}