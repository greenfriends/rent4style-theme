import Helper from "./Helper.js";
import CheckoutProduct from "./CheckoutProduct.js";
import CalendarJs from "./Calendar.js";

export default class Purchase extends Helper {
    constructor() {
        super();
        this.checkVariationAction = 'checkVariationAvailability';
        this.calendarInitAction = 'getProductFullyBookedDates';
        this.page = this.getPage();
        this.bookedDates = false;
        this.calendars = [];
        if(this.page === 'single') {
            this.setSinglePageEntities();
        }
        if(this.page === 'checkout') {
            this.setCheckoutEntities();
        }
    }
    getPage() {
        if(document.querySelector('.details--add-to-bag')) {
            return 'single';
        }
        if(document.getElementById('placeOrder')) {
            return 'checkout';
        }
    }
    setSinglePageEntities() {
        this.addToCartAction = 'gfAddToCart';
        this.addToCart = document.querySelector('.details--add-to-bag') ?? null;
        this.variationIdInputSingle = document.getElementById('variationId');
        this.calendarDateInputSingle = document.querySelector('.details--option-calendar');
        this.messageContainerSingle =  document.querySelector('.details--option-shiping') ?? null;
        this.successContainerSingle =  document.querySelector('.noticeSuccess') ?? null;
        this.errorContainerSingle =  document.querySelector('.noticeError') ??  null;
        this.cartIcon = document.querySelector('.header--shop-bag') ??  null;
        this.cartLink = document.getElementById('bagLink') ?? null;
        this.attributeSelectsSingle = document.querySelectorAll('.attributeSelect');
    }

    setCheckoutEntities() {
        this.placeOrder = document.getElementById('placeOrder') ?? null;
        this.productsCheckout = [];
        this.regenerateCartAction = 'regenerateCart';
        this.updateDatesInSessionAction = 'updateDatesInSession';
        this.checkoutInfo = document.getElementById('checkoutInfo');
        this.subtotal = document.getElementById('subtotal');
        this.total = document.getElementById('total');
        this.orderContainer = document.querySelector('.order');
        this.checkoutProducts = document.querySelectorAll('.order--info');
        this.removeDateFromSessionAction = 'removeDateFromSession';
        this.dateInputCheckout = document.querySelector('.details--option-calendar');
        this.currencyConversion = document.getElementById('currencyConversion');
        this.shopUrl = '/shop';
    }

    init() {
        if(this.page === 'single') {
            this.addAddToCartListener();
            this.addVariationCheckForSingleListeners();
            this.enableButtonOnCalendarPickListener();
            this.calendarInitListener(this.calendarDateInputSingle);
        }
        if(this.page === 'checkout') {
            this.initCheckoutProducts();
            this.initCalendarChangeListener();
            this.catchDeleteProductEvents();
            this.calendarInitListener(this.dateInputCheckout);
        }
    }

    calendarInitListener(calendarInput) {
        calendarInput.addEventListener('click', async () => {
            this.startLoader(document.querySelector('.js-calendar'),true)
            if(!this.bookedDates) {
                let response = await fetch(this.ajaxUrl, {
                    method: 'POST',
                    body: this.getFormDataCalendarInit()
                })
                this.bookedDates = await response.json();
            }
            this.removeLoader();
            const today = new Date();
            let selectedMonth = today.getUTCMonth();
            let selectedYear = today.getUTCFullYear();
            let calendarContainerAll = document.querySelectorAll('.js-calendar');
            calendarContainerAll.forEach(calendarContainer => {
                let calendar = new CalendarJs(selectedMonth, selectedYear, calendarContainer, JSON.parse(this.bookedDates));
                this.calendars.push(calendar);
                calendar.init();
            })
        })
    }

    removeCalendarsHtml() {
        this.calendars.forEach((calendar) => {
            calendar.removeCalendarHtml();
        })
    }

    addAddToCartListener() {
        this.addToCart.addEventListener('click', async () => {
           this.startLoader(this.messageContainerSingle);
            let response = await fetch(this.ajaxUrl,{
                method: 'POST',
                body: this.getFormDataAddToCart()
            })
            let res = await response.json();
            let responseParsed = this.parseJsonResponse(res);
            if(responseParsed.success) {
                if(responseParsed.message) {
                    this.printMessage(this.successContainerSingle, this.errorContainerSingle, 'success', responseParsed.message);
                    this.incrementCartContents(this.cartIcon,this.cartLink);
                    this.calendarDateInputSingle.classList.add('disable-popup');
                }
            } else {
                if(responseParsed.message) {
                    this.printMessage(this.successContainerSingle, this.errorContainerSingle, 'error', responseParsed.message);
                }
            }
            this.removeLoader();
        });
    }

    getFormDataAddToCart() {
        const data = new FormData();
        data.append('action',this.addToCartAction);
        data.append('postId',this.addToCart.getAttribute('data-id'));
        data.append('variationId',this.variationIdInputSingle.value);
        data.append('dates',JSON.stringify(this.calendarDateInputSingle.dates))
        return data;
    }

    getFormDataCalendarInit() {
        const data = new FormData();
        data.append('action', this.calendarInitAction);
        if(this.page === 'single') {
            data.append('postIds', JSON.stringify([{postId: this.variationIdInputSingle.value, qty: 1}]));
        }
        if(this.page === 'checkout') {
            data.append('postIds', JSON.stringify(this.getCheckoutVariationIds()));
            data.append('checkout', 'checkout');
        }
        return data;
    }

    getCheckoutVariationIds() {
        let data = [];
        this.productsCheckout.forEach((product) => {
           data.push({postId: product.variationId, qty: product.quantityInputValue});
        });
        return data;
    }

    addVariationCheckForSingleListeners() {
        this.attributeSelectsSingle.forEach((attributeSelect) => {
            attributeSelect.addEventListener('change', async () => {
                this.calendarDateInputSingle.classList.add('disableEvents');
                if(this.isEveryAttributeSelectedSingle(this.attributeSelectsSingle)) {
                    this.removeCalendarsHtml();
                    this.bookedDates = false;
                    this.startLoader(this.messageContainerSingle);
                    this.disableButton(this.addToCart);
                    let response = await fetch(this.ajaxUrl,{
                        method: 'POST',
                        body: this.getFormDataVariationCheckSingle()
                    })
                    let res = await response.json();
                    let responseParsed = this.parseJsonResponse(res);
                    this.removeLoader();
                    if(responseParsed.success) {
                        let variationId = responseParsed.variationId;
                        if(variationId) {
                            this.variationIdInputSingle.value = variationId;
                            this.removeMessages(this.successContainerSingle,this.errorContainerSingle);
                            this.calendarDateInputSingle.classList.remove('disableEvents');
                            this.printMessage(this.successContainerSingle, this.errorContainerSingle, 'success', this.myObject.selectADateText)
                            if(this.calendarDateInputSingle.value !== '') {
                                this.enableButton(this.addToCart);
                            }
                        }
                    } else {
                        this.disableButton(this.addToCart);
                        this.calendarDateInputSingle.classList.add('disableEvents');
                        this.variationIdInputSingle.value = '';
                        if(responseParsed.message) {
                            this.printMessage(this.successContainerSingle,this.errorContainerSingle,'error',responseParsed.message);
                        }
                    }

                } else {
                    this.disableButton(this.addToCart);
                }
            });
        });
    }

    isEveryAttributeSelectedSingle(selects) {
        let isValid = true;
        selects.forEach((select) => {
            if(select.value === '-1') {
                isValid = false;
            }
        })
        return isValid;
    }

    isEveryAttributeSelectedCheckout() {
        let isEverySelected = true;
        this.productsCheckout.forEach((product) => {
            if(!product.isEveryAttributeSelected()) {
                isEverySelected = false;
            }
        })
        return isEverySelected;
    }

    getFormDataVariationCheckSingle() {
        const data = new FormData();
        data.append('action',this.checkVariationAction);
        data.append('postId',this.addToCart.getAttribute('data-id'));
        data.append('data',this.getVariationSelectedSingle(this.attributeSelectsSingle));
        return data;
    }

    getVariationSelectedSingle(selects) {
        let obj = [];
        selects.forEach((select) => {
            obj.push({
                attribute: select.getAttribute('data-attribute'),
                value: select.value
            });
        });
        return JSON.stringify(obj);
    }


    initCheckoutProducts() {
        this.checkoutProducts.forEach((checkoutProduct) => {
            let product = new CheckoutProduct(this.orderContainer,checkoutProduct);
            this.productsCheckout.push(product);
            product.init();
        });
    }

    catchDeleteProductEvents() {
        this.orderContainer.addEventListener('productDeleted', (e) => {
           this.deleteProductObjectFromArray(e.detail);
           this.regenerateCheckoutPriceInfo().then(() => {
               this.removeLoader();
           });
        });
        this.orderContainer.addEventListener('quantityChanged', () => {
           this.regenerateCheckoutPriceInfo().then(() => {
               this.removeLoader();
           })
        });
        this.orderContainer.addEventListener('variationChanged',(e) => {
            if (e.detail) {
                // reset booked dates so we load new data when the calendar opens
                this.bookedDates = false;
                this.enableButton(this.placeOrder);
                this.regenerateCheckoutPriceInfo().then(() => {
                    this.removeLoader();
                })
            } else {
                this.disableButton(this.placeOrder);
            }
        })
    }

    deleteProductObjectFromArray(product) {
        this.productsCheckout = this.productsCheckout.filter((checkoutProduct) =>{
           return product !== checkoutProduct;
        });
        if(this.productsCheckout.length === 0) {
            this.orderContainer.remove();
            this.removeDateFromSession(true).then(() => {});
        }
    }

    async removeDateFromSession(redirect = false) {
        let data = new FormData();
        data.append('action',this.removeDateFromSessionAction)
        let response = await fetch(this.ajaxUrl,{
            method: 'POST',
            body: data
        })
        let res = await response.json();
        let responseParsed = this.parseJsonResponse(res);
        if(responseParsed.success && redirect) {
            this.redirectToShop();
        }

    }
    redirectToShop() {
        window.location.href = window.location.origin + this.shopUrl;
    }

    async regenerateCheckoutPriceInfo() {
        await this.removeDateFromSession(false);
        this.dateInputCheckout.value = '';
        this.bookedDates = false;
        this.removeCalendarsHtml();
        this.disableButton(this.placeOrder);
        this.checkoutInfo.classList.add('loading');
        this.startLoader(this.checkoutInfo.parentElement, true);

        let data = new FormData();
        data.append('action',this.regenerateCartAction);
        let response = await fetch(this.ajaxUrl,{
            method: 'POST',
            body: data
        })
        let res = await response.json();
        let responseParsed = JSON.parse(res);
        if(responseParsed.success) {
            this.subtotal.innerHTML = responseParsed.data.subtotal;
            this.total.innerHTML = responseParsed.data.total;
            this.checkoutInfo.classList.remove('loading');
            this.removeLoader();
            this.enableButton(this.placeOrder)
        }
        if(document.querySelectorAll('.noticeError.show').length > 0 || !this.isEveryAttributeSelectedCheckout()) {
            this.disableButton(this.placeOrder);
        }
    }


    enableButtonOnCalendarPickListener() {
        this.calendarDateInputSingle.addEventListener('dateInputChange', () => {
            this.successContainerSingle.classList.remove('show');
            this.enableButton(this.addToCart);
        });

        this.calendarDateInputSingle.addEventListener('dateInputUnselected', () => {
            this.successContainerSingle.classList.add('show');
            this.disableButton(this.addToCart);
        });
    }

    initCalendarChangeListener() {
        this.dateInputCheckout.addEventListener('dateInputChange',async () => {
            let data = new FormData();
            data.append('action',this.updateDatesInSessionAction);
            data.append('dates',JSON.stringify(this.dateInputCheckout.dates));
            let response = await fetch(this.ajaxUrl,{
                method: 'POST',
                body: data
            })
            let res = await response.json();
            let responseParsed = JSON.parse(res);
        })
    }
}