import Helper from "./Helper.js";

export default class CalendarJs extends Helper {
    months = [
        'Siječanj',
        'Veljača',
        'Ožujak',
        'Travanj',
        'Svibanj ',
        'Lipanj',
        'Srpanj',
        'Kolovoz',
        'Rujan',
        'Listopad',
        'Studeni',
        'Prosinac',
    ];
    days = [
        'Pon',
        'Uto',
        'Sri',
        'Čet',
        'Pet',
        'Sub',
        'Ned',
    ];
    constructor(selectedMonth, selectedYear, container, bookedDates) {
        super();
        this.selectedMonth = selectedMonth;
        this.selectedYear = selectedYear;
        this.container = container;
        this.dummyDate = new Date();
        this.today = new Date(this.dummyDate.toLocaleString('en-US',{timeZone:'Europe/Zagreb'}))
        this.currentYear = this.today.getFullYear();
        this.currentMonth = this.today.getMonth();
        this.currentDay = this.today.getDate();
        this.currentWeekDay = this.today.getDay();
        this.datesSelected = [];
        this.calendarInput = document.querySelector('.details--option-calendar');
        this.bookedDates = bookedDates;
    }
    init(){
        this.prevMonthClickEventListener();
        this.nextMonthEventListener();
        this.printCalendar();
    }

    removeCalendarHtml() {
        this.container.innerHTML = '';
    }

    addNotifyMeIfAvailable(elem) {
        // only on single
        if (document.body.classList.contains('single')) {
            elem.insertAdjacentHTML('beforeend', `<div class="calendar--info" title="Obavesti me">Obavijesti me ako se oslobodi</div>`);
            return elem.querySelector('.calendar--info');
        }
    }

    addNotifyMeListener(elem) {
        if (document.body.classList.contains('single')) {
            elem.addEventListener('click', () => {
                let dates = this.handleSlotPick(elem, false, true);
                this.addUserToNotifyList(dates);
            });
        }
    }

    async addUserToNotifyList(dates) {
        let container = document.getElementById('calendarNoticeContainer');
        let messageContainer = document.getElementById('messageElementNotice');
        if(messageContainer) {
            messageContainer.remove();
        }
        this.startLoader(container);
        const ajaxUrl = MYOBJECT.ajaxUrl;
        const myObject = MYOBJECT;
        let data = new FormData();
        let variationId = document.getElementById('variationId');
        if(!variationId) {
            this.removeLoader();
            return;
        }
        data.append('dates', JSON.stringify(dates));
        data.append('action','addUserToNotifyList');
        data.append('productId', variationId.value.toString());
        if(myObject && ajaxUrl) {
            let response = await fetch(ajaxUrl, {
                method: 'POST',
                body: data
            });
            let res = await response.json();
            let dataRes = JSON.parse(res);
            let success = dataRes.success;
            let message = dataRes.message;
            if(message && container) {
                if(success === true) {
                    this.printNoticeInContainer(container, message, 'success');
                } else {
                    this.printNoticeInContainer(container, message, 'error');
                }
            }
        }
        this.removeLoader();
    }

    printCalendar(){
        this.container.innerHTML = '';
        this.daysWrapper = document.createElement('div');
        this.daysWrapper.className = ('weekdays');
        this.container.appendChild(this.daysWrapper);
        this.days.forEach(day => {
            let dayElement = document.createElement('span');
            dayElement.className = ('calendar-day -weekday');
            dayElement.innerHTML = day;
            this.daysWrapper.appendChild(dayElement);
        });
        const calendar = new CalendarBase.Calendar({
            siblingMonths: true,
            weekStart: 1
        });
        let calendarMonth = document.querySelector('.js-calendar-month');
        calendarMonth.innerHTML = this.months[this.selectedMonth];
        let calendarYear = document.querySelector('.calendarYear');
        calendarYear.innerHTML = this.selectedYear;
        let i = 0; //counter for week separator
        let k = 0; // counter for first 3 days of week
        let separatorDiv = document.createElement('div');
        separatorDiv.className = 'weekSeparator';
        let calendarDates = calendar.getCalendar(this.selectedYear, this.selectedMonth);
        calendarDates.forEach((date,index) => {
            if (date) {
                let notify = false;
                this.container.appendChild(separatorDiv);
                let span = document.createElement('span');
                span.date = date;
                span.className = 'calendar-day enabled-entry';
                k++;

                let isCurrentMonth = date.month === this.currentMonth;
                let isPastMonth = (date.month < this.currentMonth) && (date.year <= this.currentYear);
                let isPastDay = (date.day < this.currentDay);
                let isCurrentYear = date.year === this.currentYear;
                let isCurrentDay = date.day === this.currentDay;
                let isNextDay = date.day  === this.currentDay + 1;
                let areFirstThreeDays = k <= 3;
                if (((isPastMonth || isPastDay) && isCurrentYear && (isCurrentMonth || isPastMonth)) || areFirstThreeDays || (isCurrentDay && isCurrentMonth && isCurrentYear)
                    || (this.currentWeekDay >= 4 && isCurrentYear && isCurrentMonth && (date.day === this.currentDay + 1 || date.day === this.currentDay + 2 || date.day === this.currentDay + 3))) {
                    span.className = ('calendar-day disabled-entry');
                }
                if(isCurrentMonth || date.month === this.currentMonth + 1) { // if current month or (next month, used for sibling days)
                    if(isCurrentDay && k === 4) {
                        calendarDates[index+2].isDisabled = true;
                        calendarDates[index+3].isDisabled = true;
                    }
                    if(isCurrentDay && k === 5) {
                        calendarDates[index+1].isDisabled = true;
                        calendarDates[index+2].isDisabled = true;
                    }
                }
                if(date.isDisabled) {
                    span.className = ('calendar-day disabled-entry');
                    delete date.isDisabled;
                }
                if(this.isBookedDate(date)) {
                    notify = true;
                    span.className = ('calendar-day disabled-entry booked');
                }
                if(k>3) {
                    span.classList.add('isSlotDate');
                }
                span.innerHTML = date.day.toString();
                if(notify && k === 4) {
                    let notifyMeElement = this.addNotifyMeIfAvailable(span);
                    this.addNotifyMeListener(notifyMeElement);
                }
                separatorDiv.appendChild(span);
                i++;
                if (i % 7 === 0 && i !== 0) {
                    separatorDiv = document.createElement('div');
                    separatorDiv.className = 'weekSeparator';
                    k = 0;
                }
                this.dayClickEventListener(date, span);
            }
        });
    }

    isBookedDate(date) {
        // calendar dates begin with 0, our data starts with 1
        let dummyDate = date;
        dummyDate.month += 1;
        if(dummyDate.siblingMonth) {
            delete dummyDate.siblingMonth;
        }
        let isBooked = false;
        this.bookedDates.forEach((bookedDate) => {
            if(JSON.stringify(bookedDate) === JSON.stringify(dummyDate)) {
                isBooked = true;
            }
        });
        return isBooked;
    }

    prevMonthClickEventListener() {
        document.getElementById('prevMonth').addEventListener('click', () => {
            if (this.selectedYear < this.currentYear || (this.selectedYear === this.currentYear) && (this.selectedMonth === this.currentMonth)) {
                return;
            }
            this.selectedMonth--;
            if (this.selectedMonth < 0) {
                this.selectedMonth = 11;
                this.selectedYear--;
            }
            this.printCalendar();
        });
    }

    nextMonthEventListener() {
        document.getElementById('nextMonth').addEventListener('click', () => {
            this.selectedMonth++;
            if (this.selectedMonth >= 12) {
                this.selectedMonth = 0;
                this.selectedYear++;
            }
            this.printCalendar();
        });
    }

    dayClickEventListener(date, element) {
        element.addEventListener('click', () => {
            let parentElementWeek = element.parentElement;
            // if there are booked dates in the same row (week) it means that that week slot is busy or if all are disabled return and dont regard the pick
            if(parentElementWeek.querySelectorAll('.booked').length > 0 || parentElementWeek.querySelectorAll('.disabled-entry').length >= 7) {
                return;
            }
            let slotDates = parentElementWeek.querySelectorAll('.isSlotDate');
            let toggleWeek = this.toggleWeekSelectedSlot(parentElementWeek); // returns true if a new week is selected, false if deselected
            slotDates.forEach((slotDate) => {
                if(toggleWeek) {
                    slotDate.style.background = '#78c775';
                } else {
                    slotDate.style = 'none';
                }
            });
            // This is used for handleDatePick(date) where date is the selected date
            // let selectedDay = parseInt(element.innerHTML);
            // let selectedMonth = date.month +1;
            // let selectedYear = date.year;
            // let selectedDate = {
            //     day: selectedDay,
            //     month: selectedMonth,
            //     year: selectedYear,
            //     dayElement: element
            // };
            // this.handleDatePick(selectedDate)
            this.handleSlotPick(toggleWeek,slotDates);

            const overlay = document.querySelector('.overlay');
            const popUp = document.querySelector('.popup')
            let notices = document.querySelector('.woocommerce-error');
            const popUpContent = document.querySelector('.popup-content')
            if(notices) {
                notices.remove();
            }
            html.classList.remove('no-scroll');
            setTimeout(() => {
                overlay.classList.remove('open');
            }, 200);
            popUp.classList.remove('open');
            popUpContent.innerHTML = '';
        });

        element.addEventListener('mouseover',() => {
            let parentElementWeek = element.parentElement;
            if(!parentElementWeek.classList.contains('selected')) {
                let slotDates = parentElementWeek.querySelectorAll('.enabled-entry');
                this.mouseEventToggleDate(slotDates, 'mouseover')
            }
        });
        element.addEventListener('mouseout',() => {
            let parentElementWeek = element.parentElement;
            if(!parentElementWeek.classList.contains('selected')) {
                let slotDates = parentElementWeek.querySelectorAll('.enabled-entry');
                this.mouseEventToggleDate(slotDates, 'mouseout')
            }
        })
    }

    mouseEventToggleDate(slots,event) {
        slots.forEach((slotDate) => {
            if(event === 'mouseover') {
                slotDate.style.background = '#e62b4226';
            }
            if(event === 'mouseout') {
                slotDate.style.background = '';
            }
        });
    }

    removeSelectedSlots() {
        document.querySelectorAll('.weekSeparator.selected').forEach((weekSelected) => {
            weekSelected.classList.remove('selected');
            weekSelected.querySelectorAll('.isSlotDate').forEach((entry) => {
                entry.style = '';
            });
        });
    }

    toggleWeekSelectedSlot(weekElement) {
        if(weekElement.classList.contains('selected')) {
            weekElement.classList.remove('selected');
            return false;
        }
        this.removeSelectedSlots();
        weekElement.classList.add('selected');
        return true;
    }

    handleSlotPick(newSelected, slotDates, notifyMe = false) {
        if(notifyMe) {
            let datesSelected = [];
            let week = newSelected.parentElement.parentElement;
            let bookedElements = week.querySelectorAll('.booked');
            bookedElements.forEach((bookedElement) => {
                datesSelected.push(bookedElement.date);
            });
            return datesSelected;
        }
        this.datesSelected = [];
        this.calendarInput.dates = [];
        if(newSelected) {
            this.setDateInputValue(slotDates);
            slotDates.forEach((slotDate) => {
                if(slotDate.date.siblingMonth) {
                    delete slotDate.date.siblingMonth;
                }
                this.calendarInput.dates.push(slotDate.date);
                this.datesSelected.push(slotDate.date);
            })
            this.calendarInput.dispatchEvent(new CustomEvent('dateInputChange'));
        } else {
            this.calendarInput.value = '';
            this.calendarInput.dispatchEvent(new CustomEvent('dateInputUnselected'));
        }
    }

    setDateInputValue(slotDates) {
        this.calendarInput.value =
            this.formatDay(slotDates[0].date.day) + '-' +
            this.formatMonth(slotDates[0].date.month) + '-' +
            slotDates[0].date.year + ' - ' +
            this.formatDay(slotDates[slotDates.length - 1].date.day) + '-' +
            this.formatMonth(slotDates[slotDates.length - 1].date.month) + '-' +
            slotDates[slotDates.length - 1].date.year;
    }

    handleDatePick(date) {
        if(date.dayElement.classList.contains('selectedDate')) {
            date.dayElement.classList.remove('selectedDate');

        } else {
            this.selectedDayStart = date.day;
            date.dayElement.classList.add('selectedDate');
            this.datesSelected.push(date);
        }

        this.sortDates();
        this.calendarInput.value =
            this.formatDay(this.datesSelected[0].day) + '-' +
            this.formatMonth(this.datesSelected[0].month) + '-' +
            this.datesSelected[0].year + ' - ' +
            this.formatDay(this.datesSelected[this.datesSelected.length - 1].day) + '-' +
            this.formatMonth(this.datesSelected[this.datesSelected.length - 1].month) + '-' +
            this.datesSelected[this.datesSelected.length - 1].year;
        this.calendarInput.dates = this.datesSelected;
    }


    formatMonth(month) {
        if (month < 10) {
            return '0' + month;
        }
        return month;
    }

    formatDay(day) {
        if (day < 10) {
            return '0' + day;
        }
        return day;
    }

    sortDates() {
        this.datesSelected.sort((a, b) => {
            let yearA = a.year;
            let yearB = b.year;
            let monthA = a.month;
            let monthB = b.month;
            let dayA = a.day;
            let dayB = b.day;
            if(yearA === yearB) {
                return monthA === monthB ? dayA - dayB : monthA - monthB;
            } else {
                return yearA - yearB;
            }
        });
    }
}