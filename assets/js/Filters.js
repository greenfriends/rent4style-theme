export default class Filters {

    checkboxFilters = [];
    clearAllButtons = document.querySelectorAll('.filters--clear');
    registerCheckboxFilters(filters) {
        filters.forEach((filter) => {
            let obj = {
                mainInput: document.querySelector(`[name='${filter.selector}']`),
                checkboxInputs: document.querySelectorAll(`.${filter.selector}`),
            };
            this.checkboxFilters.push(obj);
        });
    }

    addClearFiltersListener() {
        this.clearAllButtons.forEach((clearButton) => {
            clearButton.addEventListener('click', (e) => {
                this.uncheckAllCheckboxFilters();
                this.uncheckAllColorFilters();
            })
        })
    }

    uncheckAllCheckboxFilters() {
        this.checkboxFilters.forEach((filter) => {
            filter.checkboxInputs.forEach((input) => {
               input.removeAttribute('checked');
            });
            filter.mainInput.value = '';
        });
    }

    addCheckboxListeners() {
        this.checkboxFilters.forEach((filter) => {
            filter.checkboxInputs.forEach((input) => {
                input.addEventListener('change', () => {
                   this.appendCheckboxValuesToMainInput(filter.mainInput,filter.checkboxInputs);
                });
            })
        })
    }

    appendCheckboxValuesToMainInput(mainInput, checkboxInputs) {
        let finalValue = '';
        checkboxInputs.forEach((input) => {
            if(input.checked) {
                finalValue += input.value + ',';
            }
        })
        mainInput.value = finalValue.replace(/,\s*$/, ""); // remove the last comma
    }

    initColorFilters() {
        this.colorFilterContainer = document.querySelector('.filter--colors') ?? null;
        this.mainColorInput = document.querySelector(`[name='pa_color']`) ?? null;
        this.colorInputs = document.querySelectorAll('.colorInput');
        if(this.colorFilterContainer) {
            this.colorFilters = this.colorFilterContainer.querySelectorAll('li');
            this.colorFilters.forEach((filter) => {
                this.addFilterColorListener(filter);
            });
        }
    }

    addFilterColorListener(filter) {
        filter.addEventListener('click', () => {
            let input = filter.querySelector('input');
            if(filter.classList.contains('active')) {
                input.checked = false;
                filter.classList.remove('active');
            } else {
                input.checked = true;
                filter.classList.add('active');
            }
        this.appendColorValueToMainInput();
        });
    }

    appendColorValueToMainInput() {
        let finalValue = '';
        this.colorInputs.forEach((colorInput) => {
            if(colorInput.checked) {
                finalValue += colorInput.value + ',';
            }
        })
        this.mainColorInput.value = finalValue.replace(/,\s*$/, ""); // remove the last comma
    }

    uncheckAllColorFilters() {
        this.colorFilters.forEach((filter) => {
            filter.querySelector('input').checked = false;
            filter.classList.remove('active');
        });
        this.mainColorInput.value = '';
    }


}