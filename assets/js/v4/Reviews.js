export default class Reviews {
    ajaxUrl = MYOBJECT.ajaxUrl;
    reviewButtons = document.querySelectorAll('.addReview');
    addReviewAction = 'addReview';
    allstars =  document.querySelectorAll('.details--ratings .icon')
    solidStar = document.getElementById('hiddenIconStarSolid');
    hollowStar = document.getElementById('hiddenIconStarHollow');
    isOrderPage = document.querySelector('.order--list') ?? null;
    orderRemove = document.querySelectorAll('.order--remove.myAccount');
    init() {
        if(this.isOrderPage) {
            this.addReviewListeners();
            this.addStarListeners();
            this.addRemoveOrderListener();
        }
    }

    addRemoveOrderListener() {
        this.orderRemove.forEach((remove) => {
            remove.addEventListener('click', () => {
               let parent = remove.parentElement.parentElement;
               let notFitModal = parent.querySelector('.doesntFit');
               let closeModal = parent.querySelector('.closeFit');
               let accept = parent.querySelector('.actions .accept');
               let cancel = parent.querySelector('.actions .cancel');
               let orderNote = parent.querySelector('.orderNote');
               notFitModal.classList.add('show');
               closeModal.addEventListener('click', () => {
                   notFitModal.classList.remove('show');
               })
               cancel.addEventListener('click', () => {
                   notFitModal.classList.remove('show');
               })
               accept.addEventListener('click', async () => {
                   let response = await fetch(this.ajaxUrl, {
                       method: 'POST',
                       body: this.getCancelFormData(
                           {
                               orderId: parent.getAttribute('data-order-id'),
                               orderNote: orderNote.value
                           }
                       )
                   })
                   let res = await response.json();
                   if (res.success === true) {
                       notFitModal.classList.remove('show');
                       location.reload();
                   } else {

                   }
               });
        });
        })
    }

    getCancelFormData(formData) {
        const data = new FormData();
        data.append('action','itemFit');
        data.append('orderId',formData.orderId);
        data.append('orderNote',formData.orderNote);
        return data;
    }


    addStarListeners() {
        this.allstars.forEach(star => {
            star.onclick = () => {
                let starLevel = star.getAttribute('data-num')
                this.allstars.forEach(el => { //loop through stars again to compare the clicked star to all other stars
                    if(starLevel < el.getAttribute('data-num')) {
                        // change class
                        el.classList.remove('star-solid')
                        el.classList.add('star')
                        // change icon
                        el.querySelector('use').remove()
                        if(this.hollowStar) {
                            el.innerHTML += this.hollowStar.innerHTML;
                        }

                    } else {
                        // change class
                        el.classList.remove('star')
                        el.classList.add('star-solid')
                        // change icon
                        el.querySelector('use').remove()
                        if(this.solidStar) {
                            el.innerHTML += this.solidStar.innerHTML;
                        }
                    }
                })
            }
        })
    }
    addReviewListeners() {
        this.reviewButtons.forEach((reviewButton) => {
            this.addReviewImageListener(reviewButton);

            reviewButton.addEventListener('click',() => {
                if(!this.isReviewFormOpen(reviewButton)) {
                    this.openReviewForm(reviewButton);
                } else {
                    this.removeMessageContainer();
                    this.startLoader(reviewButton);
                    //@todo validate
                    this.submitReview(reviewButton).then((res) => {
                        if(res) {
                            this.printMessage(res,reviewButton);
                            if(!res.success) {
                                this.removeLoader();
                            }
                        }

                    });
                }
            });
        });
    }

    addReviewImageListener(reviewButton) {
        let addImageInput = reviewButton.parentElement.querySelector('.add-image--input');
        addImageInput.addEventListener('change', () => {
            let files = addImageInput.files;
            //@todo validate;
        });
    }

    printMessage(res, reviewButton) {
        if(res && res.message) {
            let messageContainer;
            if(res.success) {
                messageContainer = this.generateMessageContainer(true);
            } else {
                messageContainer = this.generateMessageContainer(false);
            }
            if(messageContainer) {
                messageContainer.innerText = res.message;
                if(res.success) {
                    reviewButton.innerHTML = '';
                    reviewButton.parentElement.querySelector('.reviews--item-right').remove();
                }
                reviewButton.appendChild(messageContainer);
            }
        }
    }

    removeMessageContainer() {
        let messageContainer = document.getElementById('messageContainer');
        if(messageContainer) {
            messageContainer.remove();
        }
    }
    generateMessageContainer(success) {
        let container = document.createElement('p');
        container.id = 'messageContainer';
        if(success) {
            container.classList.add('successMessage');
        } else {
            container.classList.add('errorMessage');
        }
        return container;
    }

    isReviewFormOpen(reviewButton) {
        return reviewButton.classList.contains('reviewSend');
    }
    openReviewForm(reviewButton) {
        let reviewForm = reviewButton.parentElement.querySelector('.reviewHidden');
        reviewForm.classList.remove('reviewHidden');
        reviewButton.querySelector('button').innerText = reviewButton.getAttribute('data-send-text');
        reviewButton.classList.add('reviewSend');
    }

    getFormData(reviewButton) {
        const data = new FormData();
        data.append('action',this.addReviewAction);
        data.append('productId',reviewButton.getAttribute('data-product-id'));
        data.append('rating',this.getRatingForReview(reviewButton));
        data.append('description', this.getDescriptionForReview(reviewButton));
        data.append('imageData', this.getImageFileForReview(reviewButton));
        data.append('age', this.getAgeForReview(reviewButton));
        data.append('title', this.getTitleForReview(reviewButton));
        data.append('wornTo', this.getWornToForReview(reviewButton));
        data.append('bodyType', this.getBodyTypeForReview(reviewButton));
        data.append('userId', reviewButton.getAttribute('data-user-id'));
        return data;
    }
    getImageFileForReview(reviewButton) {
        let fileInput = reviewButton.parentElement.querySelector('.add-image--input');
        if(fileInput) {
            return fileInput.files[0];
        }
    }

    getRatingForReview(reviewButton) {
        let reviewContainer = reviewButton.parentElement.querySelector('.details--ratings');
        return reviewContainer.querySelectorAll('.star-solid').length.toString();
    }

    getDescriptionForReview(reviewButton) {
        let descriptionContainer = reviewButton.parentElement.querySelector('.reviews--item-text');
        return descriptionContainer.querySelector('textarea').value;
    }

    getTitleForReview(reviewButton) {
        let descriptionContainer = reviewButton.parentElement.querySelector('.reviews--item-text');
        return descriptionContainer.querySelector('.reviewTitle').value;
    }
    getBodyTypeForReview(reviewButton) {
        return '';
        let descriptionContainer = reviewButton.parentElement.querySelector('.reviews--item-text');
        return descriptionContainer.querySelector('.bodyType').value;
    }
    getWornToForReview(reviewButton) {
        return '';
        let descriptionContainer = reviewButton.parentElement.querySelector('.reviews--item-text');
        return descriptionContainer.querySelector('.wornTo').value;
    }
    getAgeForReview(reviewButton) {
        return '';
        let descriptionContainer = reviewButton.parentElement.querySelector('.reviews--item-text');
        return descriptionContainer.querySelector('.age').value;
    }

    async submitReview(reviewButton) {
        let response = await fetch(this.ajaxUrl,{
            method: 'POST',
            body: this.getFormData(reviewButton)
        })
        let res = await response.json();
        return JSON.parse(res);
    }

    startLoader(target, center = false) {
        if(!document.getElementById('loader')) {
            let loader = document.createElement('span');
            loader.id = 'loader';
            loader.classList.add('gfLoader');
            if(center) {
                loader.classList.add('checkoutLoader');
            } else {
                loader.classList.add('singleLoader');
            }
            if (target) {
                target.appendChild(loader);
            }
        }
    }
    removeLoader() {
        if(document.getElementById('loader')) {
            document.getElementById('loader').remove();
        }
    }
}