// Open fixed menu
const hamburger = document.querySelector('.hamburger'),
      fixedMenu = document.querySelector('.fixed-menu'),
      closeFixedMenu = document.querySelector('.fixed-menu--close'),
      overlay = document.querySelector('.overlay'),
      html = document.querySelector("html");

document.addEventListener('click', function(event) {
    const isClickInsideSideMenu = fixedMenu.contains(event.target);
    const isClickHamburger = hamburger.contains(event.target);

    if(isClickHamburger) {
        openSideMenu();
    } else if (!isClickInsideSideMenu && fixedMenu.classList.contains("open")) {
        closeSideMenu();
    }
});

closeFixedMenu.addEventListener('click', () => closeSideMenu());

const openSideMenu = () => {
    html.classList.add('no-scroll');
    overlay.classList.add('open');
    setTimeout(() => {
        fixedMenu.classList.add('open');
    }, 100);
}
const closeSideMenu = () => {
    html.classList.remove('no-scroll');
    setTimeout(() => {
        overlay.classList.remove('open');
    }, 200);
    fixedMenu.classList.remove('open');
}




// Open search
const searchOpen = document.querySelector('.header--search-open'),
      search = document.querySelector('.header--search'),
      searchClear = document.querySelector('.header--search-clear'),
      searchInput = document.querySelector('.header--search input');

searchOpen.addEventListener('click', () => {
    search.classList.toggle('open')
    searchInput.focus()
})

// On key press show X button. On ENTER submit form
searchInput.addEventListener('keyup', function(event) {
    searchClear.classList.add('visible')
    if (event.code === 'Enter') {
        event.preventDefault();
        document.querySelector('.header--search form').submit();
    }
})

// Clear text in search field
searchClear.addEventListener('click', () => {
    searchInput.value = ""
    searchClear.classList.remove('visible')
    searchInput.focus()
})



// close info line
const infoLine = document.querySelector('.info-line'),
      closeInfoLine = document.querySelector('.close--info-line');
closeInfoLine.addEventListener('click', () => {
    infoLine.style.height = '0'
    infoLine.style.overflow = 'hidden'
    headerHeight = document.querySelector('.header').clientHeight;
})



// like product
const productLike = document.querySelectorAll('.product--like')
productLike.forEach(item => {
    item.addEventListener('click', () => {
        item.classList.toggle('liked')
    })
})



// to top
document.addEventListener('DOMContentLoaded', function() {
    const toTop = document.querySelector('.to-top')
    window.addEventListener('scroll', (e) => {
        let last_known_scroll_position = window.scrollY;
        if(toTop) {
            last_known_scroll_position > 600 ? toTop.style.opacity = "1" : toTop.style.opacity = "0"
        }
    });
    if(toTop) {
        toTop.addEventListener('click', () => {
            window.scroll({top: 0, left: 0, behavior: 'smooth'});
        })
    }
});


// filters toggle (open and close)
const filterToggle = document.querySelectorAll('.filter--toggle')
filterToggle.forEach(item => {
    const filterContent = item.closest('.filter--top').nextElementSibling.querySelector('.filter--content');
    const itemHeight = filterContent.clientHeight + 'px';
    filterContent.style.height = itemHeight;
    item.addEventListener('click', () => {
        if(filterContent.classList.contains('active')) {
            item.classList.remove('open')
            filterContent.style.height = "0px"
            filterContent.addEventListener('transitionend', function () {
                filterContent.classList.remove('active');
            }, {
                once: true
            });
        } else {
            item.classList.add('open')
            filterContent.classList.add('active')
            filterContent.style.height = 'auto'
            const height = filterContent.clientHeight + 'px';
            filterContent.style.height = '0px';
            setTimeout(function () {
                filterContent.style.height = height;
            }, 0);
        }
    })
})


// open left column (filters)
const openFilters = document.querySelector('.open--filters'),
      closeFilters = document.querySelector('.close--filters svg'),
      leftColumn = document.querySelector('.column--left')

if(openFilters) {
    openFilters.addEventListener('click', () => {
        overlay.classList.add('open');
        html.classList.add('no-scroll');
        leftColumn.classList.add('active')
    })
}
if(closeFilters) {
    closeFilters.addEventListener('click', () => {
        overlay.classList.remove('open');
        html.classList.remove('no-scroll');
        leftColumn.classList.remove('active')
    })
}

// sorting (select print in label)
const sortLabel = document.querySelector('.sort label span')

const getSorting = sel => {
    sortLabel.innerHTML = sel.options[sel.selectedIndex].text
}
document.addEventListener('DOMContentLoaded', function() {
    //sorting event listener
    const sortButton = document.getElementById('sorting')
    if (sortButton) {
        sortButton.addEventListener('change', () => {
            const filtersForm = document.getElementById('filtersForm')
            filtersForm.submit()
        })
    }
});

// fn for slide toggle
const slideToggle = (element) => {
    if(element.classList.contains('active')) {
        element.classList.remove('active')
        element.style.height = "0px"
        element.addEventListener('transitionend', function () {
            element.classList.remove('active');
        }, {
            once: true
        });
    } else {
        element.classList.add('active')
        element.style.height = 'auto'
        const height = element.clientHeight + 'px';
        element.style.height = '0px';
        setTimeout(function () {
            element.style.height = height;
        }, 0);
    }
}

// details page carousel
const detailsThumbs = new Swiper(".details--thumbs-carousel", {
    direction: 'vertical',
    slidesPerView: 5,
    freeMode: true,
    watchSlidesProgress: true,
});
const mySwiper = new Swiper(".details--carousel", {
    // If swiper loop is true set photoswipe counterEl: false (line 175 her)
    loop: true,
    /* slidesPerView || auto - if you want to set width by css like flickity.js layout - in this case width:80% by CSS */
    slidesPerView: "auto",
    centeredSlides: true,
    slideToClickedSlide: false,
    autoplay: {
        delay: 30000,
        pauseOnMouseEnter: true,
        disableOnInteraction: false
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
    },
    thumbs: {
        swiper: detailsThumbs,
    },
    keyboard: {
        enabled: true,
    }
});
var initPhotoSwipeFromDOM = function(gallerySelector) {
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for (var i = 0; i < numNodes; i++) {
            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes
            if (figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element

            size = linkEl.getAttribute("data-size").split("x");

            // create slide object
            item = {
                src: linkEl.getAttribute("href"),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };

            if (figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML;
            }

            if (linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute("src");
            }

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && (fn(el) ? el : closest(el.parentNode, fn));
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return el.tagName && el.tagName.toUpperCase() === "LI";
        });

        if (!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if (childNodes[i].nodeType !== 1) {
                continue;
            }

            if (childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }

        if (index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe(index, clickedGallery);
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
            params = {};

        if (hash.length < 5) {
            return params;
        }

        var vars = hash.split("&");
        for (var i = 0; i < vars.length; i++) {
            if (!vars[i]) {
                continue;
            }
            var pair = vars[i].split("=");
            if (pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if (params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(
        index,
        galleryElement,
        disableAnimation,
        fromURL
    ) {
        var pswpElement = document.querySelectorAll(".pswp")[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // #################### 3/4 define photoswipe options (if needed) ####################
        // https://photoswipe.com/documentation/options.html //
        options = {
            /* "showHideOpacity" uncomment this If dimensions of your small thumbnail don't match dimensions of large image */
            //showHideOpacity:true,

            // Buttons/elements
            closeEl: true,
            captionEl: true,
            fullscreenEl: true,
            zoomEl: true,
            shareEl: false,
            counterEl: false,
            arrowEl: true,
            preloaderEl: true,
            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute("data-pswp-uid"),
            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName("img")[0], // find thumbnail
                    pageYScroll =
                        window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();

                return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
            }
        };

        // PhotoSwipe opened from URL
        if (fromURL) {
            if (options.galleryPIDs) {
                // parse real index when custom PIDs are used
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for (var j = 0; j < items.length; j++) {
                    if (items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if (isNaN(options.index)) {
            return;
        }

        if (disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();

        /* ########### PART 4 - EXTRA CODE  ########### */
        /* EXTRA CODE (NOT FROM photoswipe CORE) -
        1/2. UPDATE SWIPER POSITION TO THE "CURRENT" ZOOM_IN IMAGE (BETTER UI) */
        // photoswipe event: Gallery unbinds events
        // (triggers before closing animation)
        gallery.listen("unbindEvents", function() {
            // The index of the current photoswipe slide
            let getCurrentIndex = gallery.getCurrentIndex();
            // Update position of the slider
            mySwiper.slideTo(getCurrentIndex, 0, false);
            // 2/2. Start swiper autoplay (on close - if swiper autoplay is true)
            mySwiper.autoplay.start();
        });
        // 2/2. Extra Code (Not from photoswipe) - swiper autoplay stop when image in zoom mode (When lightbox is open) */
        gallery.listen('initialZoomIn', function() {
            if(mySwiper.autoplay.running){
                mySwiper.autoplay.stop();
            }
        });
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll(gallerySelector);

    for (var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute("data-pswp-uid", i + 1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if (hashData.pid && hashData.gid) {
        openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
    }
};

// execute above function
initPhotoSwipeFromDOM(".lightbox");

// details page customer photos carousel
const detailsCustomer = new Swiper(".details--customer-carousel", {
    slidesPerView: 1,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination-customer',
        type: 'bullets',
    },
    effect: 'fade',
    fadeEffect: {
        crossFade: true
    },
    init: false
});

const detailsCustomerPhotos = document.querySelector('.details--customer');
const detailsCustomerThumb = document.querySelector('.details--thumbs-customer');
const detailsCustomerClose = document.querySelector('.details--customer-close');
const reviewsItemImages = document.querySelectorAll('.reviews--item-image')

if(detailsCustomerPhotos) {
    // detailsCustomerThumb.addEventListener('click', () => {
        // openCustomerPhotos();
        // stopDetailsVideo();
    // })

    detailsCustomerClose.addEventListener('click', () => {
        // closeCustomerPhotos();
    })

    document.addEventListener('keyup', function(event) {
        // if (event.code === 'Escape') {
        //     event.preventDefault();
        //     closeCustomerPhotos();
        // }
    })

    // open specific slide when clicked on reviews image
    reviewsItemImages.forEach(item => {
        // const slideId = item.getAttribute('data-slide');
        item.addEventListener('click', () => {
            // openCustomerPhotos();
            // detailsCustomer.slideTo(Number(slideId));
        })
    })
}

const openCustomerPhotos = () => {
    html.classList.add('no-scroll');
    overlay.classList.add('open');
    setTimeout(() => {
        detailsCustomerPhotos.classList.add('open');
    }, 100);
    detailsCustomer.init()
    mySwiper.autoplay.stop();
}
const closeCustomerPhotos = () => {
    html.classList.remove('no-scroll');
    setTimeout(() => {
        overlay.classList.remove('open');
    }, 200);
    detailsCustomerPhotos.classList.remove('open');
    mySwiper.autoplay.start();
}

// details video
const detailsVideoOpen = document.querySelectorAll('.details--thumbs-video, .details--video-open');
const detailsVideoSection = document.querySelector('.details--video');
const detailsVideo = document.querySelector('.details--video video');
const triggerGallery = document.querySelector('.details--images-open');
// mq for fullscreen on mobile
const mediaQuery = window.matchMedia('(max-width: 767px)')
if(detailsVideoOpen) {
    detailsVideoOpen.forEach(item => {
        item.addEventListener('click', async () => {
            if(triggerGallery) {
                triggerGallery.style.fontWeight = 'normal';
            }
            detailsVideo.src = item.getAttribute('data-src');
            detailsVideoSection.classList.add('active')
            if (mediaQuery.matches) {
                if (detailsVideo.requestFullscreen)
                    await detailsVideo.requestFullscreen();
                else if (detailsVideo.webkitRequestFullscreen)
                    detailsVideo.webkitRequestFullscreen();
                else if (detailsVideo.msRequestFullScreen)
                    detailsVideo.msRequestFullScreen();
            }
        })
    })
    if(triggerGallery) {
        triggerGallery.addEventListener('click', () => {
            detailsVideoSection.classList.remove('active');
            triggerGallery.style.fontWeight = 'bold';
        });
    }
}

// fullscreen
// if(detailsVideo) {
//     detailsVideo.addEventListener('click', function(e) {
//         handleFullscreen();
//     });
// }
// const handleFullscreen = () => {
//     if (isFullScreen()) {
//         console.log('jeste full screen')
//     } else {
//         console.log('nije full screen')
//     }
// }
// loop video
let iterations = 1,
    maxIterations = 2;
if(detailsVideo) {
    detailsVideo.addEventListener('ended',  function()  {
        if (iterations < maxIterations) {
            this.currentTime = 0;
            this.play();
            iterations++;
        } else {
            stopDetailsVideo();
        }
    }, false);
}

// close video when clicked on gallery thumbs
const detailsThumbsItems = document.querySelectorAll('.details--thumbs-carousel .swiper-slide')

if(detailsThumbsItems) {
    detailsThumbsItems.forEach(item => {
        item.addEventListener('click', () => {
            stopDetailsVideo();
        })
    })
}

// close video fn
const stopDetailsVideo = () => {
    detailsVideoSection.classList.remove('active');
    detailsVideo.pause();
    detailsVideo.currentTime = 0;
}


// details ratings
const detailsRatings = document.querySelector('.details--info .details--ratings')
const reviews = document.querySelector('.reviews')

if(detailsRatings) {
    detailsRatings.addEventListener('click', () => {
        const reviewsDistance = window.pageYOffset + reviews.getBoundingClientRect().top - 50
        window.scroll({top: reviewsDistance, left: 0, behavior: 'smooth'});
    })
}

// open rent options
const rentPrice = document.querySelectorAll('.details--option-price');
const rentPriceCheck = document.querySelectorAll('.details--option-price input');
const rentShiping = document.querySelectorAll('.details--option-shiping');

if(rentPrice) {
    rentPrice.forEach(item => {
        item.addEventListener('click', () => {
            // remove all active classes
            rentShiping.forEach(item => {
                item.classList.remove('active')
            })
            // uncheck all inputs
            rentPriceCheck.forEach(item => {
                item.checked = false
            })
            // add active class for clicked sibling
            item.nextElementSibling.classList.add('active')
            // check input for clicked element
            item.querySelector('input').checked = true
        })
    })
}

// open popup
const popUpOpen = document.querySelectorAll('.popup-calendar--open, .popup-calendar-dates--open, .details--add-to-bag, .header--user, .popup-shipping--open, .popup-billing--open, .popup-shipping-new--add, .popup-billing-new--add, #registerAside, #loginAside')
const popUp = document.querySelector('.popup')
const popUpClose = document.querySelectorAll('.popup-close, .popup-button--secondary')

// types of popup content
const popUpContent = document.querySelector('.popup-content')
const popUpCalendar = document.querySelector('.popup-calendar')
const popUpLogIn = document.querySelector('.popup-login')
const popUpShipping = document.querySelector('.popup-shipping')
const popUpShippingNew = document.querySelector('.popup-shipping-new')
const popUpBilling = document.querySelector('.popup-billing')
const popUpBillingNew = document.querySelector('.popup-billing-new')

if(popUpOpen) {
    popUpOpen.forEach(item => {
        item.addEventListener('click', (e) => {
            fixedMenu.classList.remove('open');
            e.preventDefault();
            if(item.classList.contains('popup-calendar--open') && !item.classList.contains('disable-popup')) {
                openPopUp();
                if (popUpLogIn) {
                    popUpContent.append(popUpLogIn)
                } else {
                    popUpContent.append(popUpCalendar)
                    busyDatesInfo()
                }
            } else if(item.classList.contains('popup-calendar-dates--open')) {
                openPopUp();
                if ('content' in document.createElement('template')) {
                    popUpContent.innerHTML = '';
                    let template = document.querySelector('#contactFormTemplate');
                    let clone = template.content.cloneNode(true);
                    popUpContent.append(clone);
                }

            } else if (
                item.classList.contains('details--add-to-bag') ||
                item.classList.contains('header--user')) {
                if (popUpLogIn){
                    openPopUp();
                    popUpContent.append(popUpLogIn)
                } else {
                    let pickedDates = document.querySelector('.details--option-calendar').dates;
                }
            } else if (item.classList.contains('popup-shipping--open')) {
                openPopUp();
                popUpContent.append(popUpShipping)
            } else if (item.classList.contains('popup-shipping-new--add')) {
                openPopUp();
                popUpContent.innerHTML = '';
                popUpContent.append(popUpShippingNew)
            } else if (item.classList.contains('popup-billing--open')) {
                openPopUp();
                popUpContent.append(popUpBilling)
            } else if (item.classList.contains('popup-billing-new--add')) {
                openPopUp();
                popUpContent.innerHTML = '';
                popUpContent.append(popUpBillingNew)
            } else if (item.id === 'loginAside') {
                openPopUp();
                e.preventDefault();
                popUpContent.append(popUpLogIn);
                signUp.classList.add('hidden')
                logIn.classList.remove('hidden')
            } else if (item.id === 'registerAside') {
                openPopUp();
                e.preventDefault();
                popUpContent.append(popUpLogIn)
                signUp.classList.remove('hidden')
                logIn.classList.add('hidden')
            }
        })
    })

    popUpClose.forEach(item => {
        item.addEventListener('click', () => {
            closePopUp();
        })
    })

    document.addEventListener('keyup', function(event) {
        if (event.code === 'Escape') {
            event.preventDefault();
            closePopUp();
        }
    })
}

const openPopUp = () => {
    html.classList.add('no-scroll');
    overlay.classList.add('open');
    setTimeout(() => {
        popUp.classList.add('open');
    }, 100);
}
const closePopUp = () => {
    let notices = document.querySelector('.woocommerce-error');
    if(notices) {
        notices.remove();
    }
    html.classList.remove('no-scroll');
    setTimeout(() => {
        overlay.classList.remove('open');
    }, 200);
    popUp.classList.remove('open');
    popUpContent.innerHTML = '';
}

// Popup entered items and items to edit
    // when checked clear disabled
const popUpEnteredRadio = document.querySelectorAll('.popup-entered input[type=radio]')
const popUpEnteredInputs = document.querySelectorAll('.popup-entered input[type=text]')
if(popUpEnteredRadio) {
    popUpEnteredRadio.forEach(item => {
        item.addEventListener('click', () => {
            popUpEnteredInputs.forEach(item => {
                item.disabled = true
            })
            item.nextElementSibling.querySelectorAll('input').forEach(item => {
                item.disabled = false
            })
        })
    })
}

// Popup removing added items
const popUpRemoveItems = document.querySelectorAll('.popup-entered--remove')
const popUpRemoveItemsYes = document.querySelectorAll('.popup-entered--remove-dialog .popup-button--yes')
const popUpRemoveItemsNo = document.querySelectorAll('.popup-entered--remove-dialog .popup-button--no')
// popup-entered--item

if(popUpRemoveItems) {
    popUpRemoveItems.forEach(item => {
        item.addEventListener('click', () => {
            item.nextElementSibling.classList.add('active')
        })
    })

    popUpRemoveItemsYes.forEach(item => {
        item.addEventListener('click', () => {
            item.parentElement.parentElement.parentElement.remove()
        })
    })

    popUpRemoveItemsNo.forEach(item => {
        item.addEventListener('click', () => {
            item.parentElement.parentElement.classList.remove('active')
        })
    })
}

// login/signup toggle
const logIn = document.querySelector('.login')
const logSwitch = document.querySelector('.login-switch')
const signUp = document.querySelector('.signup')
const signUpSwitch = document.querySelector('.signup-switch')

if(logSwitch) {
    logSwitch.addEventListener('click', () => {
        logIn.classList.add('hidden')
        signUp.classList.remove('hidden')
    })
}

if(signUpSwitch) {
    signUpSwitch.addEventListener('click', () => {
        signUp.classList.add('hidden')
        logIn.classList.remove('hidden')
    })
}

// view more carousel
const viewMore = new Swiper(".view--more-carousel", {
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        320: {
            slidesPerView: 2,
            spaceBetween: 18
        },
        480: {
            slidesPerView: 3,
            spaceBetween: 18
        },
        640: {
            slidesPerView: 4,
            spaceBetween: 28
        }
    }
});

// orders dialog remove (close, no and yes)
const orderDialogClose = document.querySelectorAll('.order--dialog-close')
if(orderDialogClose) {
    orderDialogClose.forEach(item => {
        item.addEventListener('click', () => {
            item.parentElement.classList.remove('active')
        })
    })
}
const orderDialogButtonClose = document.querySelectorAll('.order--dialog-button-no')
if(orderDialogButtonClose) {
    orderDialogButtonClose.forEach(item => {
        item.addEventListener('click', () => {
            item.parentElement.parentElement.classList.remove('active')
        })
    })
}
// orders edit
const orderEdit = document.querySelectorAll('.order--edit')
if(orderEdit) {
    orderEdit.forEach(item => {
        item.addEventListener('click', () => {
            // change edit to save
            item.textContent = 'Save'
            item.classList.remove('order--edit')
            item.classList.add('order--save')
            // un disable all inputs
            const inputs = item.closest('.order').querySelectorAll('input, select')
            inputs.forEach(item => {
                item.disabled = false
            })
        })
    })
}
// add image in review
const addImages = document.querySelectorAll('.add-image')
if(addImages) {
    addImages.forEach(item => {
        item.addEventListener('click', () => {
            item.nextElementSibling.click()
        })
    })
}

// opening add reviews form
// const orderButton = document.querySelectorAll('.order .order--button')
// if(orderButton) {
//     orderButton.forEach(item => {
//         item.addEventListener('click', () => {
//             item.nextElementSibling.classList.add('active')
//             item.remove()
//         })
//     })
// }

// favorites
const favoritesHearts = document.querySelector('.favorites--hearts')
function randomNumber(min, max) {
    return Math.floor(Math.random() * max) + min;
}

function createHearts(type, quantity) {
    for(var i = 0; i<quantity; i++) {
        var cloud = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        cloud.classList.add('icon', 'type-'+type);
        let heart = document.querySelector('.heart-full');
        if(heart) {
            cloud.innerHTML += heart.innerHTML;
        }
        cloud.style.left = randomNumber(1,99) + '%';
        cloud.style.top = randomNumber(1,99) + '%';
        cloud.style.animationDuration = randomNumber(20,200) + 's';
        document.querySelector(".favorites--hearts").appendChild(cloud);
    }
}
if(favoritesHearts) {
    createHearts(1, 12);
    createHearts(2, 16);
    createHearts(3, 6);
}

// banners - brandings
const bannerBrandingLeft = document.querySelector('.banner-branding--left')
const bannerBrandingRight = document.querySelector('.banner-branding--right')
let headerHeight;
if(bannerBrandingLeft) {
    headerHeight = document.querySelector('.header').clientHeight + document.querySelector('.info-line').clientHeight
    window.addEventListener('scroll', () => {
        let distanceFromTop = window.pageYOffset
        let bannerTop = headerHeight - distanceFromTop
        if(distanceFromTop > headerHeight) {
            bannerBrandingLeft.style.top = "0px"
            bannerBrandingRight.style.top = "0px"
        } else {
            bannerBrandingLeft.style.top = `${bannerTop}px`
            bannerBrandingRight.style.top = `${bannerTop}px`
        }
    })
}

// faq toggle (open and close)
const faqToggle = document.querySelectorAll('.faq--toggle')
faqToggle.forEach(item => {
    const faqContent = item.closest('.faq--top').nextElementSibling.querySelector('.faq--content');
    const faqHeight = faqContent.clientHeight + 'px';
    faqContent.style.height = faqHeight;
    item.addEventListener('click', () => {
        if(faqContent.classList.contains('active')) {
            item.classList.remove('open')
            faqContent.style.height = "0px"
            faqContent.addEventListener('transitionend', function () {
                faqContent.classList.remove('active');
            }, {
                once: true
            });
        } else {
            item.classList.add('open')
            faqContent.classList.add('active')
            faqContent.style.height = 'auto'
            const height = faqContent.clientHeight + 'px';
            faqContent.style.height = '0px';
            setTimeout(function () {
                faqContent.style.height = height;
            }, 0);
        }
    })
})

// add info button on calendar
const busyDatesInfo = () => {
    let calendarWeeks = document.querySelectorAll('.weekSeparator')
    calendarWeeks.forEach(item => {
        // let calendarBusyDate = item.querySelector('.enabled-entry')
        // if(calendarBusyDate) {
        //     calendarBusyDate.innerHTML = calendarBusyDate.innerHTML + '<div class="calendar--info">Obavesti me ako se oslobodi</div>'
        // }
    })
}


// Open login modal if there are errors
if(popUpLogIn && popUpLogIn.classList.contains('errorLogin')) {
    openPopUp();
    popUpContent.append(popUpLogIn);
    signUp.classList.add('hidden');
    logIn.classList.remove('hidden');
}
if(popUpLogIn && popUpLogIn.classList.contains('errorRegister')) {
    openPopUp();
    popUpContent.append(popUpLogIn);
    signUp.classList.remove('hidden')
    logIn.classList.add('hidden')
}

document.addEventListener('DOMContentLoaded', () => {
   const disabledPaginationButtons = document.querySelectorAll('.paginationDisabled')
    disabledPaginationButtons.forEach(button => {
        button.addEventListener('click', (e) => {
            e.preventDefault();
        })
    })
});

// remove billing like this for now
let noticesWc = document.querySelector('.woocommerce-error');
if(noticesWc) {
    let elements = noticesWc.querySelectorAll('li strong');
    if(elements.length > 0) {
        elements.forEach((elem) => {
            elem.innerText = elem.innerText.replace('Billing ', '');
        })
    }
}

let tagsWorksList = document.querySelectorAll('.works--list.points ul li p a');
if(tagsWorksList.length > 0) {
    tagsWorksList.forEach((tag) => {
        tag.href = 'https://www.instagram.com/rent4style/';
        tag.target = '_blank';
        tag.style.cursor = 'pointer';
        tag.title = 'rent4style';
        tag.style.fontWeight = 'bold';
    })
}

