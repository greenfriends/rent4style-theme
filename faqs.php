<?php
/**
 * Template Name: FAQS
 */
get_header();
?>
<article class="about">
    <h1><?=__('NAJČEŠĆA PITANJA', 'gf-theme')?></h1>
    <div class="faq">
        <div class="faq--top">
            <h2>Jesam li uzela dobru veličinu?</h2>
            <span class="faq--toggle">
                        <svg class="icon minus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"></use>
                        </svg>
                        <svg class="icon plus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"></use>
                        </svg>
                    </span>
        </div>
        <div class="faq--space">
            <div class="faq--content">
                <p>Uz svaki proizvod dostupan je i opis s mjerama grudi, struka i bokova. Osim toga navedeni su i podaci modela kao i veličina proizvoda koji model nosi. Odgovaramo na sve upite vezane uz mjere. Po potrebi moguća je i proba uz unaprijed dogovoren termin u Ivanić-Gradu.</p>
            </div>
        </div>
    </div>
    <div class="faq">
        <div class="faq--top">
            <h2>Kada moram naručiti haljinu da mi stigne taj vikend?</h2>
            <span class="faq--toggle">
                        <svg class="icon minus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"></use>
                        </svg>
                        <svg class="icon plus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"></use>
                        </svg>
                    </span>
        </div>
        <div class="faq--space">
            <div class="faq--content">
                <p>Haljinu možete naručiti u utorak do 17h ako je dostupna kako bi u četvrtak bila kod vas. Ako je rok prošao, a proizvod vam je hitan i želite ga, javite nam se da vidimo što možemo, a često možemo puno toga. &#128522;</p>
            </div>
        </div>
    </div>
    <div class="faq">
        <div class="faq--top">
            <h2>  Plaćam li pouzećem?</h2>
            <span class="faq--toggle">
                        <svg class="icon minus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"></use>
                        </svg>
                        <svg class="icon plus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"></use>
                        </svg>
                    </span>
        </div>
        <div class="faq--space">
            <div class="faq--content">
                <p>Nemamo opciju plaćanja pouzećem. Sva naplata bit će izvršena preko PayWay-a na našem webu te dostavljaču ništa ne plaćate.</p>
            </div>
        </div>
    </div>
    <div class="faq">
        <div class="faq--top">
            <h2> Plaćam li dostavu i prikup?</h2>
            <span class="faq--toggle">
                        <svg class="icon minus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"></use>
                        </svg>
                        <svg class="icon plus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"></use>
                        </svg>
                    </span>
        </div>
        <div class="faq--space">
            <div class="faq--content">
                <p>Veći dio troškova dostave i prikupa snosi tvrtka CARPE VIAM D.O.O. dok manji dio, u iznosu od 40 kn pokriva naručitelj.</p>
            </div>
        </div>
    </div>
    <div class="faq">
        <div class="faq--top">
            <h2> Skidaju li se sredstva odmah s računa?</h2>
            <span class="faq--toggle">
                        <svg class="icon minus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"></use>
                        </svg>
                        <svg class="icon plus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"></use>
                        </svg>
                    </span>
        </div>
        <div class="faq--space">
            <div class="faq--content">
                <p>Sredstva se skidaju s računa odmah po rezervaciji proizvoda.</p>
            </div>
        </div>
    </div>
    <div class="faq">
        <div class="faq--top">
            <h2>Što ako haljina ne odgovara i nije nošena?</h2>
            <span class="faq--toggle">
                        <svg class="icon minus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"></use>
                        </svg>
                        <svg class="icon plus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"></use>
                        </svg>
                    </span>
        </div>
        <div class="faq--space">
            <div class="faq--content">
                <p>Osoba može odustati od najma Proizvoda uz vraćanje punog iznosa naknade za najam 10 dana prije zakazanog datuma najma. Ako osoba odustane od najma 7 dana prije termina, sredstva će biti vraćena na Korisnički račun u obliku bona kojeg Stranka u punom iznosu može koristiti za idući najam. Ako osoba odustane od najma haljine 2 dana prije dana najma nije moguć povrat sredstava i Stranka je dužna bez odgode o tome obavijestiti Rent4Style. Povrat sredstava nije moguć ni kada je proizvod dostavljen, a nije nošen.</p>
                <p>Stigla ti je haljina i ne odgovara ti? Odmah nas kontaktiraj kako bismo poslali dostavnu službu po nju.
                    U našem <a href="<?=get_permalink(wc_terms_and_conditions_page_id())?>" title="pravilnik">pravilniku</a>  pročitaj koji su uvjeti u slučaju da haljina nije nošena.</p>
            </div>
        </div>
    </div>
    <div class="faq">
        <div class="faq--top">
            <h2>  Mogu li zamijeniti haljinu ako mi ne odgovara?</h2>
            <span class="faq--toggle">
                        <svg class="icon minus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"></use>
                        </svg>
                        <svg class="icon plus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"></use>
                        </svg>
                    </span>
        </div>
        <div class="faq--space">
            <div class="faq--content">
                <p>Zamjena proizvoda je moguća, ako je drugi Proizvod dostupan i ako postoji dovoljno dugačak rok da Proizvod bude dostavljen na vrijeme.</p>
            </div>
        </div>
    </div>
    <div class="faq">
        <div class="faq--top">
            <h2>Što ako uništim haljinu?</h2>
            <span class="faq--toggle">
                        <svg class="icon minus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"></use>
                        </svg>
                        <svg class="icon plus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"></use>
                        </svg>
                    </span>
        </div>
        <div class="faq--space">
            <div class="faq--content">
                <p>Ako haljina nakon vašeg nošenja nije ispravna tj. uništena je plaćate cijelu haljinu u punoj cijeni. Sitna oštećenja kao pad gumba, puknuti cif, rasporeni dio, osigurana su s naše strane.</p>
            </div>
        </div>
    </div>
    <div class="faq">
        <div class="faq--top">
            <h2> Trebam li prati haljinu nakon nošenje?</h2>
            <span class="faq--toggle">
                        <svg class="icon minus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"></use>
                        </svg>
                        <svg class="icon plus">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"></use>
                        </svg>
                    </span>
        </div>
        <div class="faq--space">
            <div class="faq--content">
                <p>Haljina stiže čista i vraća se nošena. Bitno je zapakirati je u ambalažu u kojoj je i stigla.</p>
            </div>
        </div>
    </div>
</article>
    <div class="about--menu">
        <ul>
            <li><a href="/about" title="about"><?=__('O nama','r4s')?></a></li>
            <li class="active"><a href="/faqs" title="faq"><?=__('FAQ','r4s')?></a></li>
            <li><a href="/kontakt" title="Kontakt"><?=__('Kontakt','r4s')?></a></li>
        </ul>
    </div>
<?php
get_footer();
