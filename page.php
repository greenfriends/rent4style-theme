<?php
get_header();?>

<?php
if(is_product_category() || is_product_tag()){
    echo '<main>
    <div class="container container--main container--space">';
    $category = $category = get_queried_object();
    $cols = get_option('woocommerce_catalog_columns', 4);
    $rows = get_option('woocommerce_catalog_rows', 4);
    $perPage = $cols * $rows;
    $currentPage = get_query_var('paged') ?: 1;
    $categories = get_terms(['taxonomy' => 'product_cat', 'hide_empty' => true]);
    $tags = get_terms(['taxonomy' => 'product_tag', 'hide_empty' => true]);
    global $gfContainer;
    /** @var \PluginContainer\Core\WpDbUtils\WpDbUtils $wpDbUtils */
    $wpDbUtils = $gfContainer->get(\PluginContainer\Core\WpDbUtils\WpDbUtils::class);
    $attrTaxonomies = $wpDbUtils->getAvailableAttributes();
//    var_dump($attrTaxonomies);
    $attributes = [];
    foreach ($attrTaxonomies as $key => $value) {
        $valuesForCategory = $wpDbUtils->getAttributeValuesForCategory($key, get_queried_object_id());
        if (!$valuesForCategory) {
            continue;
        }
        $attributes[$key] = $valuesForCategory;
    }
    $query = $wpDbUtils->getProducts($perPage, $currentPage, get_queried_object_id());
    $products = $query['products'];
    $maxNumPages = $query['maxNumPages'];
    include THEME_DIR . '/templates/shop/shopPageContent.php';
    echo '    </div>
</main>';
} else {
    /* Start the Loop */
    while ( have_posts() ) :
        the_post();
        the_content();

        // If comments are open or there is at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) {
            comments_template();
        }
    endwhile;// End of the loop.
}
?>
<?php
get_footer();