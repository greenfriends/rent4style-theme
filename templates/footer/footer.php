<?php dynamic_sidebar('aboveFooter'); ?>
<div class="to-top">
    <span><?=__('Top', 'gf-theme')?></span>
    <svg class="icon">
        <use href="<?=THEME_URI . '/assets/images'?>/sprite.svg#angle-up" />
    </svg>
</div>
</main>
<footer class="footer">
    <div class="container container--main">
        <div class="footer--list">
            <h6>© <?=date('Y') . ' RENT4STYLE'?></h6>
            <?php $footerMenuOne = wp_get_nav_menu_items('Footer One'); ?>
            <ul>
                <?php
                foreach ($footerMenuOne as $footerMenuOneItem): ?>
                    <?php
                    $class = '';
                    $actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    if($actualLink === $footerMenuOneItem->url) {
                        $class = 'active';
                    }
                    ?>
            <li>
                <a class="<?=$class?>" href="<?=$footerMenuOneItem->url?>"><?=$footerMenuOneItem->title?></a>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
        <div class="footer--list">
            <?php $footerMenuTwo = wp_get_nav_menu_items('Footer Two'); ?>
            <?php if(count($footerMenuTwo)):?>
                <h6>
                    <?=$footerMenuTwo[0]->title?>
                    <?php unset($footerMenuTwo[0])?>
                </h6>
                <ul>
                    <?php foreach($footerMenuTwo as $footerMenuTwoItem):?>
                        <?php
                        $class = '';
                        $actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        if($actualLink === $footerMenuTwoItem->url) {
                            $class = 'active';
                        }
                        ?>
                        <li>
                            <a class="<?=$class?>" href="<?=$footerMenuTwoItem->url?>"><?=$footerMenuTwoItem->title?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif;?>
        </div>
        <div class="footer--list">
            <?php $footerMenuFour = wp_get_nav_menu_items('Footer Four'); ?>
            <?php if(count($footerMenuFour)):?>
                <h6>
                    <?=$footerMenuFour[0]->title?>
                    <?php unset($footerMenuFour[0])?>
                </h6>
                <ul>
                    <?php foreach($footerMenuFour as $footerMenuFourItem):?>
                        <?php
                        $class = '';
                        $actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        if($actualLink === $footerMenuFourItem->url) {
                            $class = 'active';
                        }
                        ?>
                        <li>
                            <a class="<?=$class?>" href="<?=$footerMenuFourItem->url?>"><?=$footerMenuFourItem->title?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif;?>
        </div>
        <div class="footer--list">
            <h6>PRATI rent4style</h6>
            <ul class="footer--social">
                <?= do_shortcode('[follow_icons]')?>
            </ul>
        </div>
    </div>
    <?php
    global $gfContainer;
    /**
     * @var \PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion $currencyConversion
     */
    $currencyConversion = $gfContainer->get(\PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion::class);
    $rate = $currencyConversion->getExchangeRate();
    ?>
    <?php if($rate):?>
    <span style="padding:1rem 0; font-weight:bold; display:block;text-align:center;"><?='Fiksni tečaj konverzije: 1 &euro; = ' . number_format($rate, 4,',','.') . ' kn'?></span>
    <?php endif;?>
</footer>
