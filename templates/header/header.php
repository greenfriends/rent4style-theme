<?php

use PluginContainer\Packages\Wishlist\Service\WishlistPage;

?>
<?php
global $gfContainer;
$infoLine = $gfContainer->get(\PluginContainer\Packages\InfoLine\Controller\InfoLine::class);
$infoLineContent = null;
try {
    $infoLineContent = $infoLine->getInfoLineContent();
} catch (ReflectionException $e) {
    $logger = $gfContainer->get(\PluginContainer\Core\Logger\Logger::class);
    $logger->error('Error in getting info line content', ['error' => $e->getMessage(),'trace' => $e->getTraceAsString()]);
}
?>

<?php if($infoLineContent):?>
    <div class="info-line">
        <?=stripslashes($infoLineContent)?>
        <span class="close--info-line">
            <svg class="icon">
                <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#xmark"></use>
            </svg>
        </span>
    </div>
<?php endif;?>

<header class="header">
    <div class="header--main">
        <div class="hamburger--search">
            <div class="hamburger">
                <div class="hamburger--icon"></div>
            </div>
            <span class="header--search-open">
                    <svg class="icon">
                        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#magnifying-glass" />
                    </svg>
                </span>
        </div>
        <div class="header--logo">
            <?php the_custom_logo()?>
        </div>
        <?php $wishlistPage = new WishlistPage();?>
        <div class="header--controls">
                <span class="header--saved">
                    <a href="<?=$wishlistPage->getPageUrl()?>" title="wishlist page">
                        <svg class="icon">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#heart" />
                        </svg>
                    </a>
                </span>
            <?php if(is_user_logged_in()):?>
            <span>
                 <a href="<?=get_permalink(wc_get_page_id('myaccount'))?>">
                    <svg class="icon">
                        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#user" />
                    </svg>
                </a>
            </span>
            <?php else:?>
            <span class="header--user">
                <svg class="icon">
                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#user" />
                </svg>
            </span>
            <?php endif; ?>
            <?php
            $cartContentCount = WC()->cart->get_cart_contents_count();
            ?>
            <span class="header--shop">
                    <a id="bagLink" href="<?=$cartContentCount > 0 ? wc_get_checkout_url() : get_permalink(wc_get_page_id('shop' ))?>">
                        <span class="header--shop-text"><?=__('Košarica','gf-theme')?></span>
                        <svg class="icon--medium">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#bag-shopping" />
                        </svg>
                        <!-- if empty/(0) remove class filled -->
                        <?php if(WC()->cart) :?>

                            <?php if ($cartContentCount): ?>
                                <span class="header--shop-bag filled"><?=$cartContentCount?></span>
                            <?php else: ?>
                                <span class="header--shop-bag"></span>
                            <?php endif;?>
                        <?php endif;?>
                    </a>
                </span>
        </div>
    </div>
    <nav class="header--menu">
        <ul>
            <?php $primaryMenu = wp_get_nav_menu_items('Primary Menu'); ?>
            <?php foreach ($primaryMenu as $primaryMenuItem):?>
            <?php
            $class = '';
                $actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                if($actualLink === $primaryMenuItem->url) {
                    $class = 'active';
                }
            ?>
            <li>
                <a class="<?=$class?>" href="<?=$primaryMenuItem->url?>"><?=$primaryMenuItem->title?></a>
            </li>
            <?php endforeach;?>
            <li class="highlighted"><a target="_blank" href="https://www.instagram.com/rent4style/" title="Instagram Shop">Instagram shop</a></li>
        </ul>
    </nav>
</header>
<!-- fixed aside menu -->
<section class="fixed-menu">
    <div class="fixed-menu--content">
        <div class="fixed-menu--close">
            <span></span>
        </div>
        <div class="fixed-menu--user">
            <?php if(is_user_logged_in()):?>
                <a href="<?=esc_url(wp_logout_url('/'))?>" title="<?=__('Odjavi se','gf-theme')?>"><?=__('Odjavi se','gf-theme')?></a>
            <?php else:?>
                <a href="" id="loginAside"><?=__('Prijavi se','gf-theme')?></a>
                <?=__('ili','gf-theme')?>
                <a href="" id="registerAside"><?=__('REGISTRIRAJ SE','gf-theme')?></a>

            <?php endif;?>
        </div>
        <nav>
            <ul>
                <?php foreach ($primaryMenu as $primaryMenuItem):?>
                    <li>
                        <a href="<?=$primaryMenuItem->url?>"><?=$primaryMenuItem->title?></a>
                    </li>
                <?php endforeach;?>
                <li><a target="_blank" href="https://www.instagram.com/rent4style/" title="Instagram Shop">INSTAGRAM SHOP</a></li>
                <li class="highlighted"><a href=""><?=__('Košarica', 'gf-theme')?></a></li>
            </ul>
        </nav>
        <div class="share">
            <div class="a2a_kit a2a_default_style">
                <a class="a2a_button_facebook facebook">
                    <svg class="icon">
                        <use href="<?=THEME_URI . '/assets/images/sprite.svg#facebook-f'?>"></use>
                    </svg>
                </a>
                <a class="a2a_button_whatsapp whatsapp">
                    <svg class="icon">
                        <use href="<?=THEME_URI . '/assets/images/sprite.svg#whatsapp'?>"></use>
                    </svg>
                </a>
                <a class="a2a_button_email email">
                    <svg class="icon">
                        <use href="<?=THEME_URI . '/assets/images/sprite.svg#envelope'?>"></use>
                    </svg>
                </a>
            </div>
        </div>
        <div class="fixed-menu--info">
            <?php $asideMenu = wp_get_nav_menu_items('Aside Menu'); ?>
                    <?php foreach($asideMenu as $asideMenuItem):?>
                        <a href="<?=$asideMenuItem->url?>"><?=$asideMenuItem->title?></a>
                    <?php endforeach; ?>
        </div>
    </div>
</section>
<div class="overlay"></div>
<!-- // fixed aside menu -->

<!-- search block -->
<div class="header--search">
    <form autocomplete="off" id="search-form" class="search-form" role="search" method="get"
          action="<?= esc_url(home_url('/')) ?>">
        <input type="search"
               id="search-input"
               class="search-input"
               value="<?php echo get_search_query(); ?>"
               name="s"
               placeholder="<?php _e('Pronađi nešto za sebe', 'gf-theme') ?>"
               aria-label="search"/>
        <input type="hidden" name="post_type" value="product"/>
        <span class="header--search-clear">
            <svg class="icon--medium">
                <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#xmark" />
            </svg>
        </span>
    </form>
</div>
<!-- popups-->
<div class="popup">
    <div class="popup-close">
        <svg class="icon">
            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#xmark" />
        </svg>
    </div>
    <div class="popup-content"></div>
</div>
<?php
if(!is_user_logged_in()) {
    include(THEME_DIR . '/templates/login/popupLogin.php');
}
?>
<!-- // search block -->