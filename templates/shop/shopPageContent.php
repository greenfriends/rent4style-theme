<?php
/**
 * @var $products WC_Product[]
 * @var $currentPage int
 * @var $maxNumPages int
 * @var $categories WP_Term[]
 * @var $tags WP_Term[]
 * @var $attributes WP_Term[]
 */

$filters = [];
if(isset($_GET)) {
    foreach($_GET as $key => $value) {
        $filters[$key] = explode(',',$value);
    }
}

function isInGet($attrName,$attrValue,$filters) {
    return array_key_exists($attrName,$filters) && in_array($attrValue, $filters[$attrName], true);
}

global $gfContainer;
$tagPickerController = $gfContainer->get(\PluginContainer\Packages\TagPicker\Controller\TagPicker::class);
$chosenTagsToDisplay = $tagPickerController->getChosenTags();
?>
<div class="container container--main container--space">
    <div class="columns">
        <aside class="column--left">
            <div class="close--filters">
                <svg class="icon">
                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#xmark"/>
                </svg>
                <span class="close--filters--button"><?=__('Filteri', 'gf-theme')?></span>
                <span class="flex-space"></span>
            </div>
            <form id="filtersForm" method="GET" action="<?=get_permalink(wc_get_page_id('shop'))?>">
            <div class="filters--scroll">
                <div class="links">
                    <h6><?=__('Kategorije', 'gf-theme')?></h6>
                    <ul>
                        <?php foreach ($categories as $category): ?>
                            <?php
                            $catLink = get_term_link($category);
                            $class = '';
                            $actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            if($actualLink === $catLink) {
                                $class = 'active';
                            }
                            ?>
                            <?php if($category->name === 'Uncategorized') {continue;} ?>
                            <li><a class="<?=$class?>" href="<?=$catLink?>"><?=$category->name?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="filters">
                    <span class="filters--name"><?=sprintf(__('Filteri (%d)', 'gf-theme'), count($attributes))?></span>
                    <button type="reset" class="filters--clear"><?=__('Očisti sve', 'gf-theme')?></button>
                </div>
                <?php foreach ($attributes as $attributeName => $attributesData): ?>
                    <input type="hidden" name="<?=$attributeName?>" value="<?=$_GET[$attributeName] ?? ''?>">
                    <div class="filter">
                        <div class="filter--top">
                            <?php
                                $fixedAtName = ucfirst(wc_attribute_taxonomy_slug($attributeName));
                                if($fixedAtName === 'Size') {
                                    $fixedAtName = 'Veličina'; // @todo fix for all products with size, hardcoded for now
                                }
                                if($fixedAtName === 'Color') {
                                    $fixedAtName = 'Boja';
                                }
                            ?>
                            <h6><?=$fixedAtName.' ('.count($attributesData).' )'?></h6>
                            <span class="filter--toggle open">
                                    <svg class="icon minus">
                                        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#minus"/>
                                    </svg>
                                    <svg class="icon plus">
                                        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#plus"/>
                                    </svg>
                                </span>
                        </div>
                        <div class="filter--space">
                            <div class="filter--content active">
                                <?php if ($attributeName === 'pa_color'):?>
                                <ul class="filter--colors">
                                    <?php foreach ($attributesData as $attribute): ?>
                                        <?php
                                        $showIcon = false;
                                        $color = $attribute->slug;
                                        if(isInGet('pa_color',$attribute->slug,$filters)) {
                                            $showIcon = true;
                                        }
                                        $color = getHexForColorName($attribute->slug);
                                        ?>
                                        <li <?= $showIcon ? 'class="active"' : ''?> style="background:<?=$color?>; <?=$attribute->slug === 'white' ? 'border:2px solid black;' : ''?>">
                                        <input class="colorInput" aria-label="<?=$attribute->slug?>" style="display:none;" type="checkbox" value="<?=$attribute->slug?>" <?=$showIcon ? 'checked': ''?>>
                                            <svg class="icon" <?=$attribute->slug === 'white' ? 'style="fill:black;"' : ''?>>
                                                <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#check"/>
                                            </svg>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <?php else: ?>
                                <?php foreach ($attributesData as $attribute): ?>
                                    <div class="input-wrapper">
                                        <input aria-label="<?=$attributeName?>" class="<?=$attributeName?>" type="checkbox" value="<?=$attribute->slug?>"
                                        <?= isInGet($attributeName, $attribute->slug,$filters) ? 'checked' : '' ?>>
                                        <label for="<?=$attributeName?>"><?=ucfirst($attribute->slug)?></label>
                                    </div>
                                <?php endforeach; ?>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="apply--filters">
                <button class="filters--clear"><?=__('Clear all','gf-theme')?></button>
                <button class="submit" type="submit"><?=__('Prikaži sve','gf-theme')?></button>
            </div>
            </form>
        </aside>
        <div class="column--right">
            <div class="filters-sort">
                <div class="open--filters">
                    <svg class="icon">
                        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#sliders-up"/>
                    </svg>
                    <?=sprintf(__('Filteri (%d)', 'gf-theme'), count($attributes))?>
                </div>
                <div class="sort">
                    <label for="sorting">
                        <svg class="icon">
                            <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#arrow-up-arrow-down"/>
                        </svg>
                        <span><?=__('Poredaj', 'gf-theme')?></span>
                    </label>
                    <select form="filtersForm" id="sorting" name="orderBy">
                        <option <?= isset($_GET['orderBy']) && $_GET['orderBy'] === 'date|desc' ? 'selected' : '' ?> value="date|desc"><?=__('Novo', 'gf-theme')?></option>
                        <option <?= isset($_GET['orderBy']) && $_GET['orderBy'] === 'price|desc' ? 'selected' : '' ?> value="price|desc"><?=__('Najskuplje','gf-theme')?></option>
                        <option <?= isset($_GET['orderBy']) && $_GET['orderBy'] === 'price|asc' ? 'selected' : '' ?> value="price|asc"><?=__('Najjeftinije','gf-theme')?></option>
                    </select>
                </div>
            </div>
            <?php
            global $gfContainer;
            $wishlist = $gfContainer->get(\PluginContainer\Packages\Wishlist\Controller\Wishlist::class);
            $wishlistForUser = $wishlist->getWishlistProducts();
            ?>
            <div class="products">
                <?php foreach ($products as $product): ?>
                    <div class="product">
                        <?php
                        $isProductInWishlist = $wishlist->isProductInWishlistForUser(get_current_user_id(), $product->get_id(), $wishlistForUser);
                        ?>
                        <?=\PluginContainer\Packages\Wishlist\Controller\Wishlist::getWihslistIconHtml($product, $isProductInWishlist)?>
                        <figure class="product--image">
                            <a href="<?=$product->get_permalink()?>">
                                <?=$product->get_image('large')?>
                            </a>
                        </figure>
                        <div class="product--info">
                            <h2 class="product--made"><a target="_blank"
                                        href="<?=$product->get_meta('productMadeUrl')?>"><?=$product->get_meta('productMadeTitle')?></a>
                            </h2>
                            <span class="product--name"><?=$product->get_title()?></span>
                            <?php
                            global $gfContainer;
                            /**
                             * @var \PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion $currencyConversion
                             */
                            $currencyConversion = $gfContainer->get(\PluginContainer\Packages\CurrencyConversion\Controller\CurrencyConversion::class);
                            $rate = $currencyConversion->getExchangeRate();
                            ?>
                            <span class="product--price"><?=$product->get_price_html()?> <?=$rate && get_woocommerce_currency() === 'HRK' ? '<span class="ratePrice">/ &euro; ' . number_format($product->get_price() / $rate,2) . '</span>' : ''?></span>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="pagination">
                 <a href="<?=get_pagenum_link($currentPage - 1)?>"
                    class="pagination--arrow <?=$currentPage <= 1 ? 'paginationDisabled' : ''?>">
                    <svg class="icon">
                        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#angle-left"/>
                    </svg>
                </a>
                <span class="pagination--page"><?=$currentPage . ' / ' . $maxNumPages?></span>
                <a href="<?=get_pagenum_link($currentPage + 1)?>"
                   class="pagination--arrow <?=$currentPage >= $maxNumPages ? 'paginationDisabled' : ''?>">
                    <svg class="icon">
                        <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#angle-right"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</div>
