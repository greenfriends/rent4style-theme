<?php
/**
 * @var $productId
 * @see 'single.php'
 */
$reviews = getProductReviewInfo($productId) ?? [];
?>
<?php if(count($reviews) > 0):?>
<div class="reviews--more">
    <div class="reviews">
        <div class="container container--main">
            <div class="reviews--top">
                <?php
                    /**
                     * @var $reviewCount
                     * @see 'single.php'
                     */
                ?>
                <h6><?='(' . $reviewCount . ') ' . __('Recenzija','gf-theme')?>
                </h6>
                <div class="details--ratings">
                    <?php
                    /**
                     * @var $rating
                     * @see 'single.php'
                     */
                    ?>
                    <?=getRatingHtml($rating)?>
                </div>
            </div>
            <div class="reviews--sorting">
                <div class="reviews--sorting-sort">
                    <div class="details--select">
                        <div>
                            <label><?=__('Sortiraj','gf-theme')?></label>
                            <select name="sortReviews" id="sortReviews">
                                <option value="newest"><?=__('Najnovija','gf-theme')?></option>
                                <option value="popular"><?=__('Ocjena','gf-theme')?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="reviews--sorting-size">
                    <div class="details--select">
                        <div>
                            <label class="findYourSize"><?=__('Pronađi svoju veličinu','gf-theme')?></label>
                            <select name="reviewsSize" id="reviewsSize">
                                <option selected disabled value="-1"><?=__('Veličina','gf-theme')?></option>
                                <option value="XS">XS</option>
                                <option value="S">S</option>
                                <option value="M">M</option>
                                <option value="L">L</option>
                                <option value="XL">XL</option>
                            </select>
                        </div>
                        <div>
                            <select name="reviewsHeight" id="reviewsHeight">
                                <option selected disabled value="-1"><?=__('Visina','gf-theme')?></option>
                                <option value="150"><?=__('Do 150 cm','gf-theme')?></option>
                                <option value="160"><?=__('Do 160 cm','gf-theme')?></option>
                                <option value="170"><?=__('Do 170 cm','gf-theme')?></option>
                                <option value="180"><?=__('Do 180 cm','gf-theme')?></option>
                                <option value="190"><?=__('Do 190 cm','gf-theme')?></option>
                            </select>
                        </div>
                        <div>
                            <select name="reviewsWeight" id="reviewsWeight">
                                <option selected disabled value="-1"><?=__('Težina','gf-theme')?></option>
                                <option value="60"><?=__('Do 60 kg','gf-theme')?></option>
                                <option value="70"><?=__('Do 70 kg','gf-theme')?></option>
                                <option value="90"><?=__('Do 90 kg','gf-theme')?></option>
                                <option value="100"><?=__('Do 100 kg','gf-theme')?></option>
                                <option value="110"><?=__('Do 110 kg','gf-theme')?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <button id="submitFilters"><?=__('Filtriraj', 'gf-theme')?></button>
            </div>
        </div>
        <div class="reviews--list">
            <div class="container container--main">
                    <?php foreach($reviews as $review):?>
                    <div class="reviews--item">
                        <div class="reviews--item-left">
                            <div class="reviews--item-name"><?=$review->author?></div>
                            <ul class="reviews--item-sizes">
                                <li>
                                    <span><?=__('Veličina','gf-theme')?></span>
                                    <span class="size"><?=$review->authorSize?></span>
                                </li>
                                <li>
                                    <span><?=__('Visina','gf-theme')?></span>
                                    <span class="height"><?=$review->authorHeight . ' cm'?></span>
                                </li>
                                <li>
                                    <span><?=__('Kilogrami','gf-theme')?></span>
                                    <span class="weight"><?=$review->authorWeight . ' kg'?></span>
                                </li>
                            </ul>
                            <div class="reviews--item-text"><?=$review->content?></div>
                        </div>
                        <div class="reviews--item-right">
                            <div class="reviews--item-desc">
                                <div class="reviews--item-name reviews--item-name-2"><?=$review->author?></div>
                                <div class="reviews--item-headline"><?=$review->title?></div>
                                <div class="reviews--item-rate-date">
                                    <div class="details--ratings">
                                        <?=$review->ratingHtml?>
                                    </div>
                                    <div class="reviews--item-date"><?=$review->date?></div>
                                </div>
                                <div class="reviews--item-text reviews--item-text-2"><?=$review->content?></div>
                            </div>
                            <div class="reviews--item-image"
                                 data-image-full="<?=$review->image?>"
                                 data-content="<?=$review->content?>"
                                 data-author="<?= $review->author ?>"
                                 data-height="<?= $review->authorHeight ?>"
                                 data-size="<?= $review->authorSize ?>"
                                 data-weight="<?= $review->authorWeight ?>"
                                data-age="<?=$review->age?>"
                                data-worn-to="<?=$review->wornTo?>"
                                data-body-type="<?=$review->bodyType?>"
                                 data-title="<?=$review->title?>">
                                <?php if($review->thumbnail):?>
                                    <img src="<?=$review->image?>" alt="<?=__('Image review','gf-theme')?>" />
                                    <span><?=__('View photo','gf-theme')?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="reviews--button-more">
                <button id="loadMoreReviews" data-product-id="<?=$productId?>" data-offset="1"><?=__('Učitaj više','gf-theme')?></button>
            </div>
        </div>
    </div>
    <template id="reviewTemplate">
        <div class="reviews--item">
            <div class="reviews--item-left">
                <div class="reviews--item-name"></div>
                <ul class="reviews--item-sizes">
                    <li>
                        <span><?=__('Veličina','gf-theme')?></span>
                        <span class="size"></span>
                    </li>
                    <li>
                        <span><?=__('Visina','gf-theme')?></span>
                        <span class="height"></span>
                    </li>
                    <li>
                        <span><?=__('Kilogrami','gf-theme')?></span>
                        <span class="weight"></span>
                    </li>
                </ul>
                <div class="reviews--item-text"></div>
            </div>
            <div class="reviews--item-right">
                <div class="reviews--item-desc">
                    <div class="reviews--item-name reviews--item-name-2"></div>
                    <div class="reviews--item-headline"></div>
                    <div class="reviews--item-rate-date">
                        <div class="details--ratings">

                        </div>
                        <div class="reviews--item-date"></div>
                    </div>
                    <div class="reviews--item-text reviews--item-text-2"></div>
                </div>
                <div class="reviews--item-image">
                    <img src="" alt="<?=__('Image review','gf-theme')?>" />
                    <span><?=__('View photo','gf-theme')?></span>
                </div>
            </div>
        </div>
    </template>
</div>
    <div class="details--customer">
        <div class="details--customer-carousel">
            <div class="swiper-slide customerImagesContainer">
                <div class="details--customer-image">
                    <img src="" alt="<?=__('Customer Image','gf-theme')?>"/>
                </div>
                <div class="details--customer-info">
                    <div class="details--customer-ratings">

                    </div>
                    <h3 class="title"></h3>
                    <div class="details--customer-body">
                        <ul>
                            <li>
                                <span><?=__('Visina','gf-theme')?></span>
                                <span class="height"></span>
                            </li>
                            <li>
                                <span><?=__('Težina','gf-theme')?></span>
                                <span class="weight"></span>
                            </li>
                            <li>
                                <span><?=__('Veličina','gf-theme')?></span>
                                <span class="size"></span>
                            </li>
                        </ul>
                    </div>
                    <p class="content"></p>
                </div>
            </div>
            <div class="details--customer-close">
                <svg class="icon">
                    <use href="<?=THEME_URI . '/assets/images/'?>sprite.svg#xmark"/>
                </svg>
            </div>
        </div>
        <div style="color:white;" class="swiper-button-next nextReview"></div>
        <div style="color:white" class="swiper-button-prev previousReview"></div>
    </div>

<?php endif; ?>