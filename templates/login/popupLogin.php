<?php
$error = '';
if (isset($_POST['woocommerce-login-nonce']) && count(wc_get_notices('error')) > 0){
    $error = 'Login';
}
if(isset($_POST['woocommerce-register-nonce']) && count(wc_get_notices('error')) > 0) {
    $error = 'Register';
}
?>
<div class="popup-login popup-hidden error<?=$error?>">
    <?php
    if (function_exists('wc_print_notices')){
        wc_print_notices();
    }
    ?>
    <form method="post" <?php do_action( 'woocommerce_register_form_tag' ); ?>>
        <?php do_action( 'woocommerce_register_form_start' );?>
        <div class="signup">
            <p>REGISTRACIJA</p>
            <div class="details--input"> <!-- add "error" when empty -->
                <span><?=__('Mail adresa','gf-theme')?></span>
                <input aria-label="<?=__('Email','gf-theme')?>" name="email" id="reg_email" type="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" />
            </div>
            <div class="details--input"> <!-- add "error" when empty -->
                <span><?=__('Lozinka','gf-theme')?></span>
                <input aria-label="<?=__('Password','gf-theme')?>" type="password" name="password" id="reg_password" />
            </div>
            <span class="signup-switch">Ako već imaš svoj nalog, prijavi se.</span>
            <br>
            <small>Registracijom prihvaćaš <a href="<?=get_permalink(wc_terms_and_conditions_page_id())?>">Opće uvjete poslovanja</a> i <a href="<?=get_privacy_policy_url()?>">Izjavu o privatnosti</a>.</small>

            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
            <button type="submit" name="register" value="<?php esc_attr_e( 'Sign Up', 'woocommerce' ); ?>"><?php esc_html_e( 'Registriraj se', 'woocommerce' ); ?></button>
            <div class="line"><span>ili</span></div>
            <div class="centerContainer">
                <?= do_shortcode('[nextend_social_login provider="facebook"]') ?>
                <?= do_shortcode('[nextend_social_login provider="google"]') ?>
            </div>
        </div>
        <?php do_action( 'woocommerce_register_form_end' ); ?>
    </form>

    <?php  do_action( 'woocommerce_before_customer_login_form' );?>
    <form method="post">
        <?php do_action( 'woocommerce_login_form_start' ); ?>
        <div class="login hidden">
            <p>Prijavi se i unajmi haljinu</p>
            <div class="details--input"> <!-- add "error" when empty -->
                <span>Korisničko ime</span>
                <input aria-label="<?=__('Username or email','gf-theme')?>" name="username" id="username" type="text" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" />
            </div>
            <div class="details--input"> <!-- add "error" when empty -->
                <span>Lozinka</span>
                <input aria-label="<?=__('Password','gf-theme')?>" type="password" name="password" id="password" />
            </div>
            <?php do_action( 'woocommerce_login_form' ); ?>
            <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
            <small><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>">Zaboravili ste lozinku?</a></small>
            <button type="submit"  name="login" value="<?php esc_attr_e( 'Sign in', 'woocommerce' ); ?>"><?php esc_html_e( 'Prijavi se', 'woocommerce' ); ?></button>
            <div class="line"><span>ili</span></div>
            <div class="centerContainer">
                <?= do_shortcode('[nextend_social_login provider="facebook"]') ?>
                <?= do_shortcode('[nextend_social_login provider="google"]') ?>
            </div>
            <span class="login-switch">Nemaš nalog? Registriraj se.</span>
        </div>
        <?php do_action( 'woocommerce_login_form_end' ); ?>
    </form>
</div>